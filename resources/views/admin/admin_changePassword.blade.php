@extends('admin.admin_master')
@section('admin')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<div class="container-full">
    <!-- Main content -->
    <section class="content">

        <!-- Basic Forms -->
         <div class="box">
           <div class="box-header with-border">
             <h4 class="box-title">Đổi mật khẩu</h4>

           </div>
           <!-- /.box-header -->
           <div class="box-body">
             <div class="row">
               <div class="col">
                   <form method="POST" action="{{route('adminProfile.updatePassword')}}">
                    @csrf
                    <div class="row">
                       <div class="col-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <h5>Mật khẩu hiện tại <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="password" name="current_password" id="current_password" class="form-control">
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <h5>Mật khẩu mới <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="password" name="password" id="password" class="form-control">
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <h5>Nhập lại mật khẩu <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-rounded btn-info" value="Update">
                                </div>
                            </div>
                       </div>





                   </form>

               </div>
               <!-- /.col -->
             </div>
             <!-- /.row -->
           </div>
           <!-- /.box-body -->
         </div>
         <!-- /.box -->

       </section>
    <!-- /.content -->
  </div>
@endsection

