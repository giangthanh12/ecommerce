@extends('admin.admin_master')
@section('admin')

@php
// doanh thu theo ngày
    $date = \Carbon\Carbon::now()->format('d F Y');
    $date_before = \Carbon\Carbon::yesterday()->format('d F Y');
    $amount_date_before = App\Models\Order::where('order_date', $date_before)->sum('amount');
    $amount_date = App\Models\Order::where('order_date', $date)->sum('amount');
    if(!$amount_date) {
        $amount_date = 0;
    }
    if ($amount_date_before) {
        $percent_date_change = round(($amount_date - $amount_date_before) / $amount_date_before * 100,1);
    }
// doanh thu theo tháng
    $month = \Carbon\Carbon::now()->format('F');
    $year = \Carbon\Carbon::now()->format('Y');
    $month_before = \Carbon\Carbon::now()->subMonth()->format('F');
    $year_before = \Carbon\Carbon::now()->subMonth()->format('Y');
    $amount_month_before = App\Models\Order::where('order_month', $month_before)->where('order_year', $year_before)->sum('amount');
    $amount_month = App\Models\Order::where('order_month', $month)->where('order_year', $year_before)->sum('amount');
    if(!$amount_month) {
        $amount_month = 0;
    }
    if ($amount_month_before) {
       $percent_month_change = round(($amount_month - $amount_month_before) / $amount_month_before * 100,1);
    }

    // doanh thu theo năm

    $year = \Carbon\Carbon::now()->format('Y');
    $year_before = \Carbon\Carbon::now()->subYear()->format('Y');
    $amount_year_before = App\Models\Order::where('order_year', $year_before)->sum('amount');
    $amount_year = App\Models\Order::where('order_year', $year)->sum('amount');
    if(!$amount_year) {
        $amount_year = 0;
    }
    if($amount_year_before) {
        $percent_year_change = round(($amount_year - $amount_year_before) / $amount_year_before * 100,1);
    }
    // Đơn hàng mới
    $order_amount = count(App\Models\Order::where('status', 'Pending')->get());
    $orders_pending = App\Models\Order::where('status', 'Pending')->get();
@endphp
<div class="container-full">
    <!-- Main content -->

    <section class="content">
        <div class="row">
            <div class="col-xl-3 col-6">
                <div class="box overflow-hidden pull-up">
                    <div class="box-body">
                        <div class="icon bg-primary-light rounded w-60 h-60">
                            <i class="text-primary mr-0 font-size-24 mdi mdi-account-multiple"></i>
                        </div>
                        <div>
                            <p class="text-mute mt-20 mb-0 font-size-16">Doanh thu theo ngày</p>
                            <h3 class="text-white mb-0 font-weight-500">{{number_format($amount_date,0,'','.')}}₫
                                @if ($amount_date_before)
                                <small class="text-{{$amount_date > $amount_date_before ? 'success' : 'danger'}}"><i class="fa fa-caret-{{$amount_date > $amount_date_before  ? 'up' : 'down'}}"></i> {{$amount_date > $amount_date_before  ? '+' : ''}}{{$percent_date_change}} %</small>
                                @endif
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-6">
                <div class="box overflow-hidden pull-up">
                    <div class="box-body">
                        <div class="icon bg-warning-light rounded w-60 h-60">
                            <i class="text-warning mr-0 font-size-24 mdi mdi-car"></i>
                        </div>
                        <div>
                            <p class="text-mute mt-20 mb-0 font-size-16">Doanh thu theo tháng</p>
                            <h3 class="text-white mb-0 font-weight-500">{{number_format($amount_month,0,'','.')}}₫
                                @if ($amount_month_before)
                                <small class="text-{{$percent_month_change > 0 ? 'success' : 'danger'}}"><i class="fa fa-caret-{{$percent_month_change > 0 ? 'up' : 'down'}}"></i> {{$percent_month_change > 0 ? '+' : ''}}{{$percent_month_change}} %</small>
                                @endif
                                </h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-6">
                <div class="box overflow-hidden pull-up">
                    <div class="box-body">
                        <div class="icon bg-info-light rounded w-60 h-60">
                            <i class="text-info mr-0 font-size-24 mdi mdi-sale"></i>
                        </div>
                        <div>
                            <p class="text-mute mt-20 mb-0 font-size-16">Doanh thu theo năm</p>

                            <h3 class="text-white mb-0 font-weight-500">{{number_format($amount_year,0,'','.')}}₫
                            @if ($amount_year_before)
                                <small class="text-{{$percent_year_change > 0 ? 'success' : 'danger'}}"><i class="fa fa-caret-{{$percent_year_change > 0 ? 'up' : 'down'}}"></i> {{$percent_year_change > 0 ? '+' : ''}}{{$percent_year_change}} %%</small>
                            @endif

                            </h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-6">
                <div class="box overflow-hidden pull-up">
                    <div class="box-body">
                        <div class="icon bg-danger-light rounded w-60 h-60">
                            <i class="text-danger mr-0 font-size-24 mdi mdi-phone-incoming"></i>
                        </div>
                        <div>
                            <p class="text-mute mt-20 mb-0 font-size-16">Đơn hàng chờ</p>
                            <h3 class="text-white mb-0 font-weight-500">{{$order_amount}}</h3>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-12">
                <div class="box">
                    <div class="box-header">
                        <h4 class="box-title align-items-start flex-column">
                            Đơn hàng đang chờ
                        </h4>
                    </div>

                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-border">
                                <thead>

                                    <tr class="text-uppercase bg-lightest">
                                        <th style="min-width: 250px"><span class="text-white">Ngày</span></th>
                                        <th style="min-width: 100px"><span class="text-fade">Hóa đơn</span></th>
                                        <th style="min-width: 100px"><span class="text-fade">Số tiền</span></th>
                                        <th style="min-width: 150px"><span class="text-fade">Phương thức thanh toán</span></th>
                                        <th style="min-width: 130px"><span class="text-fade">Trạng thái</span></th>
                                        <th style="min-width: 120px">Thao tác</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($orders_pending as $order)
                                    <tr>
                                        <td class="pl-0 py-8">
                                            {{Carbon\Carbon::parse($order->order_date)->format('D, d/m/Y')}}
                                        </td>
                                        <td>
                                            {{$order->invoice_no}}
                                        </td>
                                        <td>
                                            {{number_format($order->amount,0,'','.')}} đ
                                        </td>
                                        <td>
                                            {{$order->payment_type}}
                                        </td>
                                        <td>
                                            <span class="badge badge-warning">{{$order->status}}</span>
                                        </td>
                                        <td class="text-right">

                                            <a href="{{route('Order.detail',$order->id)}}" class="waves-effect waves-light btn btn-info btn-circle mx-5"><span class="mdi mdi-arrow-right"></span></a>
                                        </td>
                                    </tr>
                                    @endforeach


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
@endsection
