@php
 $route = Route::current()->getName();
    $prefix = request()->route()->getPrefix();
    $user = Auth::guard('admin')->user();

@endphp

<aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar">

        <div class="user-profile">
			<div class="ulogo">
				 <a href="{{url('admin/dashboard')}}">
				  <!-- logo for regular state and mobile devices -->
					 <div class="d-flex align-items-center justify-content-center">
						  <img src="{{asset('backend/images/logo-dark.png')}}" alt="">
						  <h3><b>Tyuq</b> Shop</h3>
					 </div>
				</a>
			</div>
        </div>
      <!-- sidebar menu-->
      <ul class="sidebar-menu" data-widget="tree">

		<li class="{{$route === 'dashboard' ? 'active' : ''}}">
          <a href="{{url('admin/dashboard')}}">
            <i data-feather="pie-chart"></i>
			<span>Dashboard</span>
          </a>
        </li>
        @can('read-brand')
        <li class="treeview {{$prefix === '/brand' ? 'active' : ''}}" >
            <a href="#">
              <i data-feather="message-circle"></i>
              <span>Brand</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class="{{$route == 'Brand.view' ? 'active' : ''}}"><a href="{{route('Brand.view')}}" ><i class="ti-more"></i>All brand</a></li>
            </ul>
          </li>
        @endcan



    @if ($user->category == 1)
        <li class="treeview {{$prefix === '/category' ? 'active' : ''}}" >
            <a href="#">
              <i data-feather="message-circle"></i>
              <span>Category</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class="{{$route == 'Category.view' ? 'active' : ''}}"><a href="{{route('Category.view')}}" ><i class="ti-more"></i>All category</a></li>
              <li class="{{$route == 'Subcategory.view' ? 'active' : ''}}"><a href="{{route('SubCategory.view')}}" ><i class="ti-more"></i>All Subcategory</a></li>
              <li class="{{$route == 'SubsubCategory.view' ? 'active' : ''}}"><a href="{{route('SubsubCategory.view')}}" ><i class="ti-more"></i>All Sub-Subcategory</a></li>
            </ul>
          </li>

        @endif
          @if ($user->product == 1)
          <li class="treeview {{$prefix === '/product' ? 'active' : ''}}" >
            <a href="#">
              <i data-feather="message-circle"></i>
              <span>Product</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class="{{$route == 'Product.add' ? 'active' : ''}}"><a href="{{route('Product.add')}}" ><i class="ti-more"></i>Add product</a></li>
              <li class="{{$route == 'Product.manage' ? 'active' : ''}}"><a href="{{route('Product.manage')}}" ><i class="ti-more"></i>Manage product</a></li>
            </ul>
          </li>
          @endif
          @if ($user->slider == 1)
          <li class="treeview {{$prefix === '/slider' ? 'active' : ''}}" >
            <a href="#">
              <i data-feather="message-circle"></i>
              <span>Slider</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class="{{$route == 'Slider.manage' ? 'active' : ''}}"><a href="{{route('Slider.manage')}}" ><i class="ti-more"></i>Manage Slider</a></li>
            </ul>
          </li>
          @endif

          @if ($user->coupon == 1)
          <li class="treeview {{$prefix === '/coupon' ? 'active' : ''}}" >
            <a href="#">
              <i data-feather="message-circle"></i>
              <span>Coupon</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class="{{$route == 'Coupon.manage' ? 'active' : ''}}"><a href="{{route('Coupon.manage')}}" ><i class="ti-more"></i>Manage Coupon</a></li>
            </ul>
          </li>
          @endif
          @if ($user->postCategory == 1)
          <li class="treeview {{$prefix === '/postCategory' ? 'active' : ''}}" >
            <a href="#">
              <i data-feather="message-circle"></i>
              <span>Post Category</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class="{{$route == 'PostCategory.manage' ? 'active' : ''}}"><a href="{{route('PostCategory.manage')}}" ><i class="ti-more"></i>Manage Post Category</a></li>
            </ul>
          </li>
          @endif

          @if ($user->post == 1)
             <li class="treeview {{$prefix === '/post' ? 'active' : ''}}" >
            <a href="#">
              <i data-feather="message-circle"></i>
              <span>Posts </span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class="{{$route == 'Post.manage' ? 'active' : ''}}"><a href="{{route('Post.manage')}}" ><i class="ti-more"></i>manage</a></li>
            </ul>
          </li>
          @endif

          @if ($user->setting == 1)
<li class="treeview {{$prefix === '/setting' ? 'active' : ''}}" >
            <a href="#">
              <i data-feather="message-circle"></i>
              <span>Setting </span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class="{{$route == 'Site.manage' ? 'active' : ''}}"><a href="{{route('Site.manage')}}" ><i class="ti-more"></i>Manage Site</a></li>
            </ul>
          </li>
          @endif

        @if ($user->adminRole == 1)
        <li class="treeview {{$prefix === '/adminRole' ? 'active' : ''}}">
            <a href="#">
              <i data-feather="file"></i>
              <span>Admin role</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class="{{$route == 'adminRole.manage' ? 'active' : ''}}"><a href="{{route('adminRole.manage')}}"><i class="ti-more"></i>Manage admin</a></li>

            </ul>
        </li>
        @endif

        <li class="treeview {{$prefix === '/module' ? 'active' : ''}}">
            <a href="#">
              <i data-feather="file"></i>
              <span>Module</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class="{{$route == 'Module.manage' ? 'active' : ''}}"><a href="{{route('Module.manage')}}"><i class="ti-more"></i>Danh sách module</a></li>

            </ul>
        </li>
        <!-- <li class="treeview {{$prefix === '/admin' ? 'active' : ''}}">
            <a href="#">
              <i data-feather="file"></i>
              <span>Admin</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class="{{$route == 'Admin.manage' ? 'active' : ''}}"><a href="{{route('Admin.manage')}}"><i class="ti-more"></i>Danh sách admin</a></li>

            </ul>
        </li> -->


        <li class="header nav-small-cap">User Interface</li>
        @if ($user->order == 1)
        <li class="treeview {{$prefix === '/order' ? 'active' : ''}}" >
            <a href="#">
              <i data-feather="message-circle"></i>
              <span>Orders</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class="{{$route == 'Order.managePendingOrder' ? 'active' : ''}}"><a href="{{route('Order.managePendingOrder')}}" ><i class="ti-more"></i>Manage Pending Order</a></li>
              <li class="{{$route == 'Order.manageConfirmedOrder' ? 'active' : ''}}"><a href="{{route('Order.manageConfirmedOrder')}}" ><i class="ti-more"></i>Manage Confirmed Order</a></li>
              <li class="{{$route == 'Order.manageProcessingOrder' ? 'active' : ''}}"><a href="{{route('Order.manageProcessingOrder')}}" ><i class="ti-more"></i>Manage Processing Order</a></li>
              <li class="{{$route == 'Order.managePickedOrder' ? 'active' : ''}}"><a href="{{route('Order.managePickedOrder')}}" ><i class="ti-more"></i>Manage Picked Order</a></li>
              <li class="{{$route == 'Order.manageShippedOrder' ? 'active' : ''}}"><a href="{{route('Order.manageShippedOrder')}}" ><i class="ti-more"></i>Manage Shipped Order</a></li>
              <li class="{{$route == 'Order.manageDeliveredOrder' ? 'active' : ''}}"><a href="{{route('Order.manageDeliveredOrder')}}" ><i class="ti-more"></i>Manage Delivered Order</a></li>
              <li class="{{$route == 'Order.manageCancelOrder' ? 'active' : ''}}"><a href="{{route('Order.manageCancelOrder')}}" ><i class="ti-more"></i>Manage Cancel Order</a></li>
              <li class="{{$route == 'Order.manageReturnOrder' ? 'active' : ''}}"><a href="{{route('Order.manageReturnOrder')}}" ><i class="ti-more"></i>Manage Return Order</a></li>
            </ul>
        </li>
        @endif









        @if ($user->Alluser == 1)
                <li class="treeview {{$prefix === '/Alluser' ? 'active' : ''}}">
                    <a href="#">
                        <i data-feather="file"></i>
                        <span>Users</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{$route == 'Alluser.view' ? 'active' : ''}}"><a href="{{route('Alluser.view')}}"><i class="ti-more"></i>All users</a></li>
                    </ul>
                </li>
        @endif
        @if ($user->review == 1)
            <li class="treeview {{$prefix === '/admin/review' ? 'active' : ''}}">
            <a href="#">
                <i data-feather="file"></i>
                <span>Reviews</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
              <li class="{{$route == 'Review.manage' ? 'active' : ''}}"><a href="{{route('Review.manage')}}"><i class="ti-more"></i>Manage reviews</a></li>
            </ul>
        </li>
        @endif



        @if ($user->AllOrder ==1)
            <li class="treeview {{$prefix === '/AllOrder' ? 'active' : ''}}">
            <a href="#">
              <i data-feather="file"></i>
              <span>Search Orders</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class="{{$route == 'Allorder.search' ? 'active' : ''}}"><a href="{{route('Allorder.search')}}"><i class="ti-more"></i>Search orders</a></li>
            </ul>
        </li>
        @endif

      </ul>
    </section>
  </aside>
