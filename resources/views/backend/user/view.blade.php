@extends('admin.admin_master')
@section('admin')
<div class="container-full">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">Danh sách user</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="table-responsive">
                     <table id="example1" class="table table-bordered table-striped">
                       <thead>
                           <tr>
                               <th>Hình ảnh</th>
                               <th>Tên</th>
                               <th>Email</th>
                               <th>Số điện thoại</th>
                               <th>Status</th>
                               <th>Actions</th>
                           </tr>
                       </thead>
                       <tbody>
                           @foreach ($users as $user)
                        <tr>
                            <td> <img width="60" height="60" src="{{!empty($user->profile_photo_path) ? asset('upload/user_profile/'.$user->profile_photo_path) : asset('upload/user3-128x128.jpg')}}" alt=""></td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->phone}}</td>
                            <th>
                                @if ($user->checkOnline($user->id))
                                    <span class="badge badge-success">Online</span>
                                @else
                                    <span class="badge badge-danger"> {{Carbon\Carbon::parse($user->last_seen)->diffForHumans()}} </span>
                                @endif



                            </th>
                            <td>
                                <a href="{{route('Brand.edit',$user->id)}}" title="Edit" class="btn btn-info mb-5"><i class="fa fa-pencil"></i></a>
                                <a href="{{route('Brand.delete',$user->id)}}" title="Delete"   class="btn btn-danger mb-5 delete"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                           @endforeach

                       </tbody>
                     </table>
                   </div>
               </div>
               <!-- /.box-body -->
             </div>
             <!-- /.box -->
           </div>
        <!-- /.col -->
      </div>
        {{-- Add brand --}}

      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>
  @endsection

