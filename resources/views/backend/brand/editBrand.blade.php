@extends('admin.admin_master')
@section('admin')
<div class="container-full">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- /.col -->
        <div class="col-8">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">Add brand</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="brand-form">
                        <form action="{{route('Brand.update', $brand->id)}}" enctype="multipart/form-data" method="POST">
                            @csrf
                            <div class="form-group">
                                <h5>Brand name vn <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="brand_name_vn" value="{{$brand->brand_name_vn}}" id="brand_name_vn" class="form-control">
                                    @error('brand_name_vn')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Brand name en <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="brand_name_en" value="{{$brand->brand_name_en}}" id="brand_name_en" class="form-control">
                                    @error('brand_name_en')
                                        <span class="text-danger">{{$message}}</span>
                                     @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Brand image <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="file" name="brand_image" id="brand_image" class="form-control">
                                    <input type="hidden"  name="brand_image_old"  value="{{$brand->brand_image}}" class="form-control">
                                    @error('brand_image')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div>
                                    <img id="showImage" src="{{asset($brand->brand_image)}}" width="100" height="100" alt="">
                                </div>
                            </div>
                            <input type="submit" class="btn btn-primary mb-5" value="Add Brand">
                        </form>
                   </div>
               </div>
               <!-- /.box-body -->
            </div>
             <!-- /.box -->
        </div>
      </div>
        {{-- Add brand --}}

      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>
  <script>
     var  elementImage = document.querySelector('#brand_image');
     element = document.querySelector('#showImage');
       elementImage.onchange = function(e){
           var reader = new FileReader();
           reader.onload = function(e) {
            element.setAttribute('src',reader.result);
           }
           reader.readAsDataURL(elementImage.files[0]);
       };
  </script>
  @endsection

