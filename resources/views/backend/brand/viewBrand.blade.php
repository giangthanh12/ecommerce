@extends('admin.admin_master')
@section('admin')
<div class="container-full">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-8">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">List Brands</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="table-responsive">
                     <table id="example1" class="table table-bordered table-striped">
                       <thead>
                           <tr>
                               <th>Brand name vn</th>
                               <th>Brand name en</th>
                               <th>Brand image</th>
                               <th>Actions</th>
                           </tr>
                       </thead>
                       <tbody>
                           @foreach ($brands as $brand)
                        <tr>
                            <td>{{$brand->brand_name_vn}}</td>
                            <td>{{$brand->brand_name_en}}</td>
                            <td><img src="{{asset($brand->brand_image)}}" width="100" height="100" alt=""></td>
                            <td>
                                <a href="{{route('Brand.edit',$brand->id)}}" title="Edit" class="btn btn-info mb-5"><i class="fa fa-pencil"></i></a>
                                <a href="{{route('Brand.delete',$brand->id)}}" title="Delete"   class="btn btn-danger mb-5 delete"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                           @endforeach

                       </tbody>
                     </table>
                   </div>
               </div>
               <!-- /.box-body -->
             </div>
             <!-- /.box -->
           </div>
        <!-- /.col -->
        <div class="col-4">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">Add brand</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="brand-form">
                        <form action="{{route('Brand.store')}}" enctype="multipart/form-data" method="POST">
                            @csrf
                            <div class="form-group">
                                <h5>Brand name vn <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="brand_name_vn" id="brand_name_vn" class="form-control">
                                    @error('brand_name_vn')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Brand name en <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="brand_name_en" id="brand_name_en" class="form-control">
                                    @error('brand_name_en')
                                        <span class="text-danger">{{$message}}</span>
                                     @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Brand image <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="file" name="brand_image" id="brand_image" class="form-control">
                                    @error('brand_image')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <input type="submit" class="btn btn-primary mb-5" value="Add Brand">
                        </form>
                   </div>
               </div>
               <!-- /.box-body -->
            </div>
             <!-- /.box -->
        </div>
      </div>
        {{-- Add brand --}}

      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>
  @endsection

