@extends('admin.admin_master')
@section('admin')
<div class="container-full">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- /.col -->
        <div class="col-8">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">Edit Slider</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="brand-form">
                        <form action="{{route('Slider.update', $slider->id)}}" enctype="multipart/form-data" method="POST">
                            @csrf
                            <div class="form-group">
                                <h5>Slider title <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="slider_title" id="slider_title" value="{{$slider->slider_title}}" class="form-control">
                                    @error('slider_title')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Slider description<span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="slider_description" id="slider_description" value="{{$slider->slider_description}}" class="form-control">
                                    @error('slider_description')
                                        <span class="text-danger">{{$message}}</span>
                                     @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Slider image <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="file" name="slider_img" id="slider_img" class="form-control">
                                    @error('slider_img')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                    <img width="300"  height="200" src="{{asset($slider->slider_img)}}" alt="">
                                </div>
                            </div>
                            <input type="submit" class="btn btn-primary mb-5" value="Add Slider">
                        </form>
                   </div>
               </div>
               <!-- /.box-body -->
            </div>
             <!-- /.box -->
        </div>
      </div>
        {{-- Add brand --}}

      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>
  <script>
     var  elementImage = document.querySelector('#brand_image');
     element = document.querySelector('#showImage');
       elementImage.onchange = function(e){
           var reader = new FileReader();
           reader.onload = function(e) {
            element.setAttribute('src',reader.result);
           }
           reader.readAsDataURL(elementImage.files[0]);
       };
  </script>
  @endsection

