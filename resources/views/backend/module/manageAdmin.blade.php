@extends('admin.admin_master')
@section('admin')
<div class="container-full">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">Danh sách admin</h3>
                 <a class="btn btn-primary float-right" href="{{route('adminRole.add')}}">Thêm thành viên</a>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="table-responsive">
                     <table id="example1" class="table table-bordered table-striped">
                       <thead>
                           <tr>
                               <th>Ảnh</th>
                               <th>Tên</th>
                               <th>Địa chỉ email</th>
                               <th>Điện thoại</th>

                               <th width="20%">Thao tác</th>
                           </tr>
                       </thead>
                       <tbody>
                           @foreach ($admins as $admin)
                        <tr>
                            <td><img src="{{asset($admin->profile_photo_path)}}" width="50" height="50" alt=""></td>
                            <td>{{$admin->name}}</td>
                            <td>{{$admin->email}}</td>
                            <th>+{{$admin->phone}}</th>




                            <td>
                                <a href="{{route('Admin.edit',$admin->id)}}" title="Edit" class="btn btn-info mb-5"><i class="fa fa-pencil"></i></a>
                                <a href="{{route('Admin.delete',$admin->id)}}" title="Delete"   class="btn btn-danger mb-5 delete"><i class="fa fa-trash"></i></a>

                            </td>
                        </tr>
                           @endforeach

                       </tbody>
                     </table>
                   </div>
               </div>
               <!-- /.box-body -->
             </div>
             <!-- /.box -->
           </div>
        <!-- /.col -->
      </div>
        {{-- Add brand --}}

      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>
  @endsection

