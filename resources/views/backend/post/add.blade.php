@extends('admin.admin_master')
@section('admin')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<div class="container-full">


    <!-- Main content -->
    <section class="content">

     <!-- Basic Forms -->
      <div class="box">
        <div class="box-header with-border">
          <h4 class="box-title">Thêm bài viết</h4>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col">
                <form method="POST"  action="{{route('Post.save')}}" enctype="multipart/form-data">
                    @csrf
                  <div class="row">
                    <div class="col-12">
                        <div class="row">{{-- start row  --}}
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h5>Danh mục bài viết <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                      <select name="post_category_id" id="post_category_id" class="form-control" required="">
                                        <option  selected="" disabled>Chọn danh mục</option>
                                        @foreach ($categories as $category)
                                          <option value="{{$category->id}}">{{$category->post_category_name_vn}}</option>
                                        @endforeach
                                      </select>
                                      @error('post_category_id')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                  </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h5>Post title vn <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="post_title_vn" id="post_title_vn" class="form-control" required="">
                                      @error('post_title_vn')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                  </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h5>Post tag <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="post_tag" id="post_tag" class="form-control" required="">
                                      @error('post_tag')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                  </div>
                            </div>
                        </div> {{-- end row  --}}

                        <div class="row">{{-- start row  --}}
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <h5>Post title en <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="post_title_en" id="post_title_en" class="form-control" required="">
                                      @error('post_title_vn')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                  </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h5>Post image<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="file" name="post_image" onchange="upload(this)" required=""  id="post_image" class="form-control" />
                                      @error('post_image')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                    <div><img src="" id="showImage" alt=""></div>
                                  </div>
                            </div>


                        </div> {{-- end row  --}}



{{-- start row  --}}
<div class="row">{{-- start row  --}}
    <div class="col-sm-6">
        <div class="form-group">
            <h5>Post detail vn<span class="text-danger">*</span></h5>
            <div class="controls">
                <textarea id="post_detail_vn" name="post_detail_vn" class="form-control" required="">
                    This is my textarea to be replaced with CKEditor.
                </textarea>
              @error('short_descp_vn')
              <span class="text-danger">{{$message}}</span>
              @enderror
            </div>
          </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <h5>Post detail en<span class="text-danger">*</span></h5>
            <div class="controls">
                <textarea id="post_detail_en" name="post_detail_en" class="form-control" required="">
                    This is my textarea to be replaced with CKEditor.
                </textarea>
              @error('post_detail_en')
              <span class="text-danger">{{$message}}</span>
              @enderror
            </div>
          </div>
    </div>

</div> {{-- end row  --}}

<div class="text-xs-right">
    <input type="submit" class="btn btn-rounded btn-primary mb-5" value="Thêm bài viết">
</div>

                  </div>

                </form>

            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>

<script>

</script>
<script type="text/javascript">
  function upload(input) {
   if(input.files && input.files[0]) {
       var file = input.files[0];
       var reader = new FileReader();
       reader.onload = function(e) {
            $('#showImage').attr('src', e.target.result).width(80).height(80);
       }
       reader.readAsDataURL(file);
   }
  }


</script>
  @endsection

