@extends('admin.admin_master')
@section('admin')
<div class="container-full">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">List Brands</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="table-responsive">
                     <table id="example1" class="table table-bordered table-striped">
                       <thead>
                           <tr>
                               <th>Username</th>
                               <th>Product name</th>
                               <th>Comment</th>
                               <th>Status</th>
                               <th>Actions</th>
                           </tr>
                       </thead>
                       <tbody>
                           @foreach ($reviews as $review)
                        <tr>
                            <td>{{$review->user->name}}</td>
                            <td width="15%">{{$review->product->product_name_vn}}</td>
                            <td width="30%">{{$review->comment}}</td>
                            <td>
                                @if ($review->status === 0)
                                <span class="badge badge-warning">Chờ duyệt</span>
                                @else
                                <span class="badge badge-success">Đã duyệt</span>
                                @endif
                            </td>
                            <td>
                            @if ($review->status == 1)
                                <a href="{{route('Review.inactive', $review->id)}}" class="btn btn-danger mb-5" title="inactive"><i class="fa fa-arrow-down"></i></a>
                            @else
                                <a href="{{route('Review.active', $review->id)}}" class="btn btn-success mb-5" title="active"><i class="fa fa-arrow-up"></i></a>
                            @endif
                                <a href="{{route('Review.delete',$review->id)}}" title="Delete"   class="btn btn-danger mb-5 delete"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                           @endforeach

                       </tbody>
                     </table>
                   </div>
               </div>
               <!-- /.box-body -->
             </div>
             <!-- /.box -->
           </div>
        <!-- /.col -->

      </div>
        {{-- Add brand --}}

      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>
  @endsection

