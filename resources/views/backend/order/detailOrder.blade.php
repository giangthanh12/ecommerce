@extends('admin.admin_master')
@section('admin')
<div class="container-full">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-6">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">Chi tiết người nhận</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="table-responsive">
                     <table  class="table table-bordered table-striped">

                            <tr>
                                <td>Tên người nhận</td>
                                <td>{{$order->name}}</td>
                            </tr>
                            <tr>
                                <td>Số điện thoại</td>
                                <td>{{$order->phone}}</td>
                            </tr>
                            <tr>
                                <td>Email người nhận</td>
                                <td>{{$order->email}}</td>
                            </tr>
                            <tr>
                                <td>Địa chỉ người nhận</td>
                                <td>{{$order->notes}}</td>
                            </tr>
                            <tr>
                                <td>Mã bưu điện</td>
                                <td>{{$order->post_code}}</td>
                            </tr>
                            <tr>
                                <td>Ngày đặt hàng</td>
                                <td>{{$order->order_date}}</td>
                            </tr>
                     </table>
                   </div>
               </div>
               <!-- /.box-body -->
             </div>
             <!-- /.box -->
           </div>
        <!-- /.col -->
        <div class="col-6">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">Chi tiết đơn hàng</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="table-responsive">
                     <table  class="table table-bordered table-striped">
                        <tr>
                            <td>Người đặt hàng</td>
                            <td>{{$order->user->name}}</td>
                        </tr>
                        <tr>
                            <td>Số điện thoại người đặt</td>
                            <td>{{$order->user->phone}}</td>
                        </tr>
                        <tr>
                            <td>Hình thức đặt hàng</td>
                            <td>{{$order->payment_type}}</td>
                        </tr>
                        <tr>
                            <td>Mã giao dịch</td>
                            <td>{{$order->transaction_id}}</td>
                        </tr>
                        <tr>
                            <td>Mã đơn hàng</td>
                            <td>{{$order->invoice_no}}</td>
                        </tr>
                        <tr>
                            <td>Tổng đơn hàng</td>
                            <td>{{number_format($order->amount,0,'','.')}}₫</td>
                        </tr>
                        <tr>
                            <td>Trạng thái đơn hàng</td>
                            <td><span class="badge badge-warning" style="background:#FFB800;">{{$order->status}}</span></td>
                        </tr>
                        <tr>
                            <td>Cập nhật trạng thái</td>
                            @if ($order->status === 'Pending')
                            <td><a href="{{url('order/pending/confirmed/'.$order->id)}}" id="confirmed" class="btn btn-success mb-5" >Update Confirm Order</a></td>
                            @elseif ($order->status === 'confirmed')
                            <td><a href="{{url('order/confirmed/processing/'.$order->id)}}" id="confirmed" class="btn btn-success mb-5" >Update Processing Order</a></td>
                            @elseif ($order->status === 'processing')
                            <td><a href="{{url('order/processing/picked/'.$order->id)}}" id="confirmed" class="btn btn-success mb-5" >Update Picked Order</a></td>
                            @elseif ($order->status === 'picked')
                            <td><a href="{{url('order/picked/shipped/'.$order->id)}}" id="confirmed" class="btn btn-success mb-5" >Update Shipped Order</a></td>
                            @elseif ($order->status === 'shipped')
                            <td><a href="{{url('order/shipped/delivered/'.$order->id)}}" id="confirmed" class="btn btn-success mb-5" >Update Delivered Order</a></td>
                            @elseif ($order->status === 'delivered')
                            <td><a href="{{url('order/delivered/cancel/'.$order->id)}}" id="confirmed" class="btn btn-success mb-5" >Update Cancel Order</a></td>
                            @endif
                        </tr>
                     </table>
                   </div>
               </div>
               <!-- /.box-body -->
             </div>
             <!-- /.box -->
           </div>
        <!-- /.col -->

      </div>


      <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header with-border">
                      <h3 class="box-title">Chi tiết sản phẩm</h3>
                    </div>
                <table class="table">
                    <div class="lds-ripple1"><div></div><div></div></div>
                    <thead>
                        <tr>
                            <th class="cart-description item">Ảnh</th>
                            <th class="cart-product-name item" >Tên sản phẩm</th>
                            <th class="cart-product-color item" width="20%">Mã code sản phẩm</th>
                            <th class="cart-product-size item">Màu</th>
                            <th class="cart-edit item" width="10%">Kích cỡ</th>
                            <th class="cart-price item">Giá</th>
                            <th class="cart-qty item" width="10%">Số lượng</th>
                            <th class="cart-sub-total item">Tổng</th>

                        </tr>
                    </thead><!-- /thead -->

                    <tbody>
                        @foreach ($order_products as $order_product)
                            <tr>
                                <td><img src="{{asset($order_product->product->product_thumbnail)}}" width="60" height="60" alt=""></td>
                                <td>{{$order_product->product->product_name_vn}}</td>
                                <td>{{$order_product->product->product_code}}</td>
                                <td>{{$order_product->color}}</td>
                                <td>{{$order_product->size}}</td>
                                <td>{{number_format($order_product->price,0,'','.')}}₫</td>
                                <td>{{$order_product->qty}}</td>
                                <td>{{number_format($order_product->price * $order_product->qty,0,'','.')}}₫</td>
                            </tr>
                        @endforeach
                    </tbody><!-- /tbody -->
                </table><!-- /table -->
                </div>
            </div>
      </div>


      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>
  @endsection

