@extends('admin.admin_master')
@section('admin')
<div class="container-full">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- /.col -->
        <div class="col-4">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">Tìm kiếm đơn hàng theo ngày</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="brand-form">
                        <form action="{{route('Allorder.searchDaySave')}}"  method="GET">
                            @csrf
                            <div class="form-group">
                                <h5>Ngày đặt hàng <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="date" name="order_date"  id="order_date" class="form-control">
                                    @error('brand_name_vn')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <input type="submit" class="btn btn-primary mb-5" value="Tìm kiếm">
                        </form>
                   </div>
               </div>
               <!-- /.box-body -->
            </div>
             <!-- /.box -->
        </div>
        <div class="col-4">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">Tìm kiến đơn hàng theo tháng</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="brand-form">
                        <form action="{{route('Allorder.searchMonthSave')}}"  method="GET">
                            @csrf
                            <div class="form-group">
                                <h5>Chọn tháng <span class="text-danger">*</span></h5>
                                <div class="controls">
                                  <select name="order_month" id="order_month" class="form-control" >
                                    <option  selected="" disabled>Lựa chọn tháng</option>

                                      <option value="January">January</option>
                                      <option value="February">February</option>
                                      <option value="March">March</option>
                                      <option value="April">April</option>
                                      <option value="May">May</option>

                                      <option value="June">June</option>
                                      <option value="July">July</option>
                                      <option value="August">August</option>
                                      <option value="September">September</option>
                                      <option value="October">October</option>
                                      <option value="November">November</option>
                                      <option value="December">December</option>
                                  </select>
                                  @error('category_id')
                                  <span class="text-danger">{{$message}}</span>
                                  @enderror
                                </div>
                              </div>
                              <div class="form-group">
                                <h5>Chọn năm<span class="text-danger">*</span></h5>
                                <div class="controls">
                                  <select name="order_year" id="order_year" class="form-control" >
                                    <option  selected="" disabled>Lựa chọn năm</option>
                                      <option value="2018">2017</option>
                                      <option value="2018">2018</option>
                                      <option value="2019">2019</option>
                                      <option value="2020">2020</option>
                                      <option value="2021">2021</option>
                                  </select>
                                  @error('category_id')
                                  <span class="text-danger">{{$message}}</span>
                                  @enderror
                                </div>
                              </div>
                            <input type="submit" class="btn btn-primary mb-5" value="Tìm kiếm">
                        </form>
                   </div>
               </div>
               <!-- /.box-body -->
            </div>
             <!-- /.box -->
        </div>
        <div class="col-4">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">Tìm kiếm đơn hàng theo năm</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="brand-form">
                        <form action="{{route('Allorder.searchYearSave')}}"  method="GET">
                            @csrf
                            <div class="form-group">
                                <h5>Chọn năm<span class="text-danger">*</span></h5>
                                <div class="controls">
                                  <select name="order_year" id="order_year" class="form-control" >
                                    <option  selected="" disabled>Lựa chọn năm</option>
                                      <option value="2018">2017</option>
                                      <option value="2018">2018</option>
                                      <option value="2019">2019</option>
                                      <option value="2020">2020</option>
                                      <option value="2021">2021</option>
                                  </select>
                                  @error('category_id')
                                  <span class="text-danger">{{$message}}</span>
                                  @enderror
                                </div>
                              </div>
                            <input type="submit" class="btn btn-primary mb-5" value="Tìm kiếm">
                        </form>
                   </div>
               </div>
               <!-- /.box-body -->
            </div>
             <!-- /.box -->
        </div>
      </div>
        {{-- Add brand --}}

      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>

  @endsection

