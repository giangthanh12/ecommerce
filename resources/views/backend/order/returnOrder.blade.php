@extends('admin.admin_master')
@section('admin')
<div class="container-full">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">Danh sách đơn hàng tìm kiếm</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="table-responsive">
                     <table id="example1" class="table table-bordered table-striped">
                       <thead>
                           <tr>
                               <th>Ngày</th>
                               <th>Hóa đơn</th>
                               <th>Tiền</th>
                               <th>Lí do trả hàng</th>
                               <th>Trạng thái</th>
                               <th width="20%">Actions</th>
                           </tr>
                       </thead>
                       <tbody>
                           @foreach ($orders as $order)
                        <tr>
                            <td>{{Carbon\Carbon::parse($order->delivered_date)->format('D, d/m/Y')}}</td>
                            <td>{{$order->invoice_no}}</td>
                            <td>{{number_format($order->amount,0,'','.')}}</td>
                            <td>{{$order->return_reason}}</td>
                            <td>
                                @if ($order->return_order === 1)
                                    <span class="badge badge-warning">Yêu cầu hoàn trả lại hàng ?</span>
                                @elseif ($order->return_order === 2)
                                <span class="badge badge-success">Đã xác nhận hàng</span>
                                @endif
                            </td>

                            <td>
                                @if ($order->return_order === 1)
                                <a href="{{route('Order.returnSuccess',$order->id)}}" title="Edit" class="btn btn-info mb-5">Xác nhận nhận hàng</a>
                                @endif
                            </tr>
                           @endforeach

                       </tbody>
                     </table>
                   </div>
               </div>
               <!-- /.box-body -->
             </div>
             <!-- /.box -->
           </div>
        <!-- /.col -->

      </div>
        {{-- Add brand --}}

      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>
  @endsection

