@extends('admin.admin_master')
@section('admin')
<div class="container-full">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">List Pending Orders</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="table-responsive">
                     <table id="example1" class="table table-bordered table-striped">
                       <thead>
                           <tr>
                               <th>Date</th>
                               <th>Invoice</th>
                               <th>Amount</th>
                               <th>Payment</th>
                               <th>Status</th>
                               <th width="20%">Actions</th>
                           </tr>
                       </thead>
                       <tbody>
                           @foreach ($orders as $order)
                        <tr>
                            <td>{{Carbon\Carbon::parse($order->confirmed_date)->format('D, d/m/Y')}}</td>
                            <td>{{$order->invoice_no}}</td>
                            <td>{{number_format($order->amount,0,'','.')}}</td>
                            <td>{{$order->payment_type}}</td>
                            <td>
                                <span class="badge badge-warning">{{$order->status}}</span>

                            </td>

                            <td>
                                <a href="{{route('Order.detail',$order->id)}}" title="Edit" class="btn btn-info mb-5"><i class="fa fa-eye"></i></a>
                                <a href="{{route('Order.invoice',$order->id)}}" title="Edit" class="btn btn-danger mb-5"><i class="fa fa-download"></i></a>
                            </tr>
                           @endforeach

                       </tbody>
                     </table>
                   </div>
               </div>
               <!-- /.box-body -->
             </div>
             <!-- /.box -->
           </div>
        <!-- /.col -->

      </div>
        {{-- Add brand --}}

      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>
  @endsection

