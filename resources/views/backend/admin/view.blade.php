@extends('admin.admin_master')
@section('admin')
<div class="container-full">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">List Admins</h3>
                 <a class="btn btn-primary float-right" href="{{route('adminRole.add')}}">Thêm thành viên</a>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="table-responsive">
                     <table id="example1" class="table table-bordered table-striped">
                       <thead>
                           <tr>
                               <th>Image</th>
                               <th>Name</th>
                               <th>Email</th>
                               <th>Phone</th>
                               <th>Access Role</th>
                               <th width="20%">Actions</th>
                           </tr>
                       </thead>
                       <tbody>
                           @foreach ($admins as $admin)
                        <tr>
                            <td><img src="{{asset($admin->profile_photo_path)}}" width="50" height="50" alt=""></td>
                            <td>{{$admin->name}}</td>
                            <td>{{$admin->email}}</td>
                            <th>+{{$admin->phone}}</th>
                            <th>
                                @if ($admin->brand == 1)
                                <span class="badge badge-primary">brand</span>
                                @endif
                                @if ($admin->category == 1)
                                <span class="badge badge-secondary">category</span>
                                @endif
                                @if ($admin->product == 1)
                                <span class="badge badge-success">product</span>
                                @endif
                                @if ($admin->slider == 1)
                                <span class="badge badge-danger">slider</span>
                                @endif
                                @if ($admin->coupon == 1)
                                <span class="badge badge-warning">coupon</span>
                                @endif
                                @if ($admin->postCategory == 1)
                                <span class="badge badge-info">postCategory</span>
                                @endif
                                @if ($admin->post == 1)
                                <span class="badge badge-dark">post</span>
                                @endif
                                @if ($admin->setting == 1)
                                <span class="badge badge-primary">setting</span>
                                @endif
                                @if ($admin->order == 1)
                                <span class="badge badge-secondary">order</span>
                                @endif
                                @if ($admin->order == 1)
                                <span class="badge badge-warning">Allorder</span>
                                @endif
                                @if ($admin->Alluser == 1)
                                <span class="badge badge-success">Alluser</span>
                                @endif
                                @if ($admin->review == 1)
                                <span class="badge badge-danger">review</span>
                                @endif
                                @if ($admin->adminRole == 1)
                                <span class="badge badge-warning">adminRole</span>
                                @endif

                            </th>



                            <td>
                                <a href="{{route('adminRole.edit',$admin->id)}}" title="Edit" class="btn btn-info mb-5"><i class="fa fa-pencil"></i></a>
                                <a href="{{route('adminRole.delete',$admin->id)}}" title="Delete"   class="btn btn-danger mb-5 delete"><i class="fa fa-trash"></i></a>

                            </td>
                        </tr>
                           @endforeach

                       </tbody>
                     </table>
                   </div>
               </div>
               <!-- /.box-body -->
             </div>
             <!-- /.box -->
           </div>
        <!-- /.col -->
      </div>
        {{-- Add brand --}}

      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>
  @endsection

