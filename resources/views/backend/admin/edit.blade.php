@extends('admin.admin_master')
@section('admin')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<div class="container-full">
    <!-- Main content -->
    <section class="content">

        <!-- Basic Forms -->
         <div class="box">
           <div class="box-header with-border">
             <h4 class="box-title">Thêm thành viên</h4>

           </div>
           <!-- /.box-header -->
           <div class="box-body">
             <div class="row">
               <div class="col">
                   <form method="POST" action="{{route('adminRole.update',$admin->id)}}" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                       <div class="col-12">
                           <div class="row">
                               <div class="col-md-6">
                                   <div class="form-group">
                                        <h5>Name<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="name" value="{{$admin->name}}"  class="form-control" > </div>
                                    </div>
                               </div>
                               <div class="col-md-6">
                                   <div class="form-group">
                                        <h5>Email <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="email" name="email" class="form-control" value="{{$admin->email}}" required="" >
                                        </div>
                                    </div>
                               </div>
                           </div>
                           <div class="row">
                            {{-- <div class="col-md-6">
                                <div class="form-group">
                                     <h5>Password<span class="text-danger">*</span></h5>
                                     <div class="controls">
                                         <input type="password" name="password" value="{{$admin->password}}" class="form-control" >
                                     </div>
                                 </div>
                            </div> --}}
                            <div class="col-md-6">
                                <div class="form-group">
                                     <h5>Phone <span class="text-danger">*</span></h5>
                                     <div class="controls">
                                         <input type="number" name="phone" value="{{$admin->phone}}" class="form-control" required="" >
                                        </div>
                                 </div>
                            </div>
                        </div>
                        <div class="row">
                               <div class="col-md-6">
                                    <div class="form-group">
                                        <h5>Image <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="file" id="image" name="profile_photo_path" class="form-control" >
                                        </div>
                                    </div>
                               </div>
                               <div class="col-md-6">
                                    <img width="100" height="100" id="showImage" src="{{asset($admin->profile_photo_path)}}" alt="">
                               </div>
                           </div>

                           <hr>
<div class="row">
    <div class="col-sm-12">
        <div class="box box-outline-primary">
          <div class="box-header with-border">
            <h4 class="box-title"><strong>Phân quyền thành viên</strong></h4>
          </div>

          <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="controls">
                            <fieldset>
                                <input type="checkbox" name="brand" value="1" id="checkbox_4" {{$admin->brand == 1 ? 'checked' : ''}}>
                                <label for="checkbox_4">Brand</label>
                            </fieldset>
                            <fieldset>
                                <input type="checkbox" value="1" name="category" id="checkbox_5" {{$admin->category == 1 ? 'checked' : ''}}>
                                <label for="checkbox_5">Category</label>
                            </fieldset>
                            <fieldset>
                                <input type="checkbox" name="product" id="checkbox_6" value="1" {{$admin->product == 1 ? 'checked' : ''}}>
                                <label for="checkbox_6">Product</label>
                            </fieldset>
                            <fieldset>
                                <input type="checkbox" name="Alluser" id="checkbox_13" value="1" {{$admin->Alluser == 1 ? 'checked' : ''}}>
                                <label for="checkbox_13">User</label>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="controls">
                            <fieldset>
                                <input type="checkbox" name="slider" id="checkbox_7" value="1"  {{$admin->slider == 1 ? 'checked' : ''}}>
                                <label for="checkbox_7">Slider</label>
                            </fieldset>
                            <fieldset>
                                <input type="checkbox" name="coupon" id="checkbox_8" value="1"  {{$admin->coupon == 1 ? 'checked' : ''}}>
                                <label for="checkbox_8">Coupon</label>
                            </fieldset>
                            <fieldset>
                                <input type="checkbox" name="postCategory" id="checkbox_9" value="1" {{$admin->postCategory == 1 ? 'checked' : ''}}>
                                <label for="checkbox_9">Post Category</label>
                            </fieldset>
                            <fieldset>
                                <input type="checkbox" name="AllOrder" id="checkbox_14" value="1" {{$admin->AllOrder == 1 ? 'checked' : ''}}>
                                <label for="checkbox_14">AllOrder</label>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="controls">
                            <fieldset>
                                <input type="checkbox" name="post" id="checkbox_10" value="1" {{$admin->post == 1 ? 'checked' : ''}}>
                                <label for="checkbox_10">Post</label>
                            </fieldset>
                            <fieldset>
                                <input type="checkbox" name="setting" id="checkbox_11" value="1" {{$admin->setting == 1 ? 'checked' : ''}}>
                                <label for="checkbox_11">Setting</label>
                            </fieldset>
                            <fieldset>
                                <input type="checkbox" name="order" id="checkbox_12" value="1" {{$admin->order == 1 ? 'checked' : ''}}>
                                <label for="checkbox_12">Order</label>
                            </fieldset>
                            <fieldset>
                                <input type="checkbox" name="review" id="checkbox_15" value="1" {{$admin->review == 1 ? 'checked' : ''}}>
                                <label for="checkbox_15">Review</label>
                            </fieldset>
                        </div>
                    </div>
                </div>

            </div>
          </div>
        </div>
      </div>
</div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-rounded btn-info" value="Thêm thành viên">
                        </div>
                       </div>





                   </form>

               </div>
               <!-- /.col -->
             </div>
             <!-- /.row -->
           </div>
           <!-- /.box-body -->
         </div>
         <!-- /.box -->

       </section>
    <!-- /.content -->
  </div>
  <script type="text/javascript">
    $(document).ready(function() {
        $('#image').change(function(e) {
           var reader = new FileReader(); // tạo một file đọc
           reader.onload = function(e) {
            // console.log(e.target.result);
            $('#showImage').attr('src', e.target.result);
           } // quá trình đọc kết thúc sẽ được gán giá trị kết quả cho src
           if (e.target.files[0]) {
                reader.readAsDataURL(e.target.files[0]);// chỉ ra đó là file đọc data url
            }
        })
    });
  </script>
@endsection

