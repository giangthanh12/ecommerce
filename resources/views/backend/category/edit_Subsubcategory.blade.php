@extends('admin.admin_master')
@section('admin')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<div class="container-full">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-8">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">Edit category</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="brand-form">
                        <form action="{{route('Subsubcategory.update', $Subsubcategory->id)}}" method="POST">
                            @csrf
                            <div class="form-group">
                              <h5>Select category <span class="text-danger">*</span></h5>
                              <div class="controls">
                                <select name="category_id" id="category_id" class="form-control" >
                                  <option  selected="" disabled>Select category</option>
                                  @foreach ($categories as $category)
                                    <option {{$category->id === $Subsubcategory->category_id ? 'selected' : ''}} value="{{$category->id}}">{{$category->category_name_vn}}</option>
                                  @endforeach
                                </select>
                                @error('category_id')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                              </div>
                            </div>
                            <div class="form-group">
                                <h5>Select subcategory <span class="text-danger">*</span></h5>
                                <div class="controls">
                                  <select name="subCategory_id" id="subCategory_id" class="form-control" >
                                      @foreach ($Subcategories as $Subcategory)
                                      <option {{$Subcategory->id === $Subsubcategory->subCategory_id ? 'selected' : ''}} value="{{$Subcategory->id}}" >{{$Subcategory->subCategory_name_vn}}</option>
                                      @endforeach

                                  </select>
                                  @error('subCategory_id')
                                  <span class="text-danger">{{$message}}</span>
                                  @enderror
                                </div>
                              </div>
                            <div class="form-group">
                                <h5>Subcategory name vn<span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="Subsubcategory_name_vn" value="{{$Subsubcategory->Subsubcategory_name_vn}}" id="Subsubcategory_name_vn" class="form-control">
                                    @error('Subsubcategory_name_vn')
                                        <span class="text-danger">{{$message}}</span>
                                     @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Subcategory name en<span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="Subsubcategory_name_en" value="{{$Subsubcategory->Subsubcategory_name_en}}" id="Subsubcategory_name_en" class="form-control">
                                    @error('Subsubcategory_name_en')
                                        <span class="text-danger">{{$message}}</span>
                                     @enderror
                                </div>
                            </div>
                            <input type="submit" class="btn btn-primary mb-5" value="Update Sub-subcategory">
                        </form>
                   </div>
               </div>
               <!-- /.box-body -->
            </div>
             <!-- /.box -->
        </div>
      </div>
        {{-- Add brand --}}

      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>
  <script type="text/javascript">
      $(document).ready(function() {
    $('#category_id').change(function() {
        document.getElementById("loader").style.display = "block";
      var category_id = $(this).val();
      var _token = $('input[name="_token"]').val();
      if(category_id) {
        $.ajax({
                    url: "{{ route('ajax.request.getSubCategories') }}",
                    type:'POST',
                    dataType: 'json', // dịnh dạng kiểu dữ liệu nhận về là json
                    data: {_token:_token, category_id:category_id},
                    success: function(data) {
                        // var data = JSON.parse(data) ;
                        // vì data nhận về là kiểu json trên có dataType = json nên jquery đã hiểu và phân tích dữ liệu để sử dụng
                        // như bình thường nếu không có dataType thì dữ liệu chỉ hiểu là chuỗi json mà jquery ko hiểu thực chất là một object
                        //nên ta phải dùng Json.parse để chuyển dữ liệu về type javascript
                       $('#subCategory_id').empty();
                       $.each(data, function (index, value) {
                        $('#subCategory_id').append(`<option value="${value.id}">${value.subCategory_name_vn}</option>`);
                        });
                        document.getElementById("loader").style.display = "none";
                    }
                });
      }
    })
      })
  </script>
  @endsection

