@extends('admin.admin_master')
@section('admin')
<div class="container-full">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-8">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">List Subcategory</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="table-responsive">
                     <table id="example1" class="table table-bordered table-striped">
                       <thead>
                           <tr>
                               <th>Category</th>
                               <th>Subcategory name vn</th>
                               <th>Subcategory name en</th>
                               <th>Actions</th>
                           </tr>
                       </thead>
                       <tbody>
                           @foreach ($subCategories as $subCategory)
                        <tr>
                            <td>{{$subCategory->category->category_name_vn}}</td>
                            <td>{{$subCategory->subCategory_name_vn}}</td>
                            <td>{{$subCategory->subCategory_name_en}}</td>
                            <td>
                                <a href="{{route('Subcategory.edit',$subCategory->id)}}" title="Edit" class="btn btn-info mb-5"><i class="fa fa-pencil"></i></a>
                                <a href="{{route('Subcategory.delete',$subCategory->id)}}" title="Delete"   class="btn btn-danger mb-5 delete"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                           @endforeach

                       </tbody>
                     </table>
                   </div>
               </div>
               <!-- /.box-body -->
             </div>
             <!-- /.box -->
           </div>
        <!-- /.col -->
        <div class="col-4">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">Add category</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="brand-form">
                        <form action="{{route('Subcategory.store')}}" method="POST">
                            @csrf
                            <div class="form-group">
                              <h5>Select category <span class="text-danger">*</span></h5>
                              <div class="controls">
                                <select name="category_id" id="category_id" class="form-control" >
                                  <option  selected="" disabled>Select category</option>
                                  @foreach ($categories as $category)
                                    <option value="{{$category->id}}">{{$category->category_name_vn}}</option>
                                  @endforeach
                                </select>
                                @error('category_id')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                              </div>
                            </div>
                            <div class="form-group">
                                <h5>Subcategory name vn<span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="subCategory_name_vn" id="subCategory_name_vn" class="form-control">
                                    @error('subCategory_name_vn')
                                        <span class="text-danger">{{$message}}</span>
                                     @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Subcategory name en<span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="subCategory_name_en" id="subCategory_name_en" class="form-control">
                                    @error('subCategory_name_en')
                                        <span class="text-danger">{{$message}}</span>
                                     @enderror
                                </div>
                            </div>
                            <input type="submit" class="btn btn-primary mb-5" value="Add Subcategory">
                        </form>
                   </div>
               </div>
               <!-- /.box-body -->
            </div>
             <!-- /.box -->
        </div>
      </div>
        {{-- Add brand --}}

      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>
  @endsection

