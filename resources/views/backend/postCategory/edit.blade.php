@extends('admin.admin_master')
@section('admin')
<div class="container-full">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- /.col -->
        <div class="col-8">
            <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">Add brand</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="brand-form">
                        <form action="{{route('PostCategory.update', $PostCategory->id)}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <h5>Category post name vn <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="post_category_name_vn" value="{{$PostCategory->post_category_name_vn}}" id="post_category_name_vn" class="form-control">
                                    @error('post_category_name_vn')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Category post name en <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="post_category_name_en" value="{{$PostCategory->post_category_name_en}}" id="post_category_name_en" class="form-control">
                                    @error('post_category_name_en')
                                        <span class="text-danger">{{$message}}</span>
                                     @enderror
                                </div>
                            </div>
                            <input type="submit" class="btn btn-primary mb-5" value="Update Category Post">
                        </form>
                   </div>
               </div>
               <!-- /.box-body -->
            </div>
             <!-- /.box -->
        </div>
      </div>
        {{-- Add brand --}}

      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>

  @endsection

