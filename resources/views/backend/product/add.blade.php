@extends('admin.admin_master')
@section('admin')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<div class="container-full">


    <!-- Main content -->
    <section class="content">

     <!-- Basic Forms -->
      <div class="box">
        <div class="box-header with-border">
          <h4 class="box-title">Add product</h4>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col">
                <form method="POST"  action="{{route('product.save')}}" enctype="multipart/form-data">
                    @csrf
                  <div class="row">
                    <div class="col-12">
                        <div class="row">{{-- start row  --}}
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h5>Select Brand <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                      <select name="brand_id" id="brand_id" class="form-control" required="">
                                        <option  selected="" disabled>Select Brand</option>
                                        @foreach ($brands as $brand)
                                          <option value="{{$brand->id}}">{{$brand->brand_name_vn}}</option>
                                        @endforeach
                                      </select>
                                      @error('brand_id')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                  </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h5>Select category <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                      <select name="category_id" id="category_id" class="form-control choose" required="">
                                        <option  selected="" disabled>Select category</option>
                                        @foreach ($categories as $category)
                                          <option value="{{$category->id}}">{{$category->category_name_vn}}</option>
                                        @endforeach
                                      </select>
                                      @error('category_id')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                  </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h5>Select Subcategory <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                      <select name="subCategory_id" id="subCategory_id" class="form-control choose" required="">
                                        <option  selected="" disabled>Select Subcategory</option>

                                      </select>
                                      @error('subCategory_id')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                  </div>
                            </div>
                        </div> {{-- end row  --}}

                        <div class="row">{{-- start row  --}}
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h5>Select Sub-subcategory <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                      <select name="subsubCategory_id" id="subsubCategory_id" class="form-control" required="">
                                        <option  selected="" disabled>Select Sub-subcategory</option>
                                      </select>
                                      @error('subsubCategory_id')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                  </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h5>Product name vn <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="product_name_vn" id="product_name_vn" class="form-control" required="">
                                      @error('product_name_vn')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                  </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h5>Product name en <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="product_name_en" id="product_name_en" class="form-control" required="">
                                      @error('product_name_en')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                  </div>
                            </div>
                        </div> {{-- end row  --}}
                        <div class="row">{{-- start row  --}}
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h5>Product code <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="product_code" required="" id="product_code" class="form-control" >
                                      @error('product_code')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                  </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h5>Product quantity <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="product_qty" required="" id="product_qty" class="form-control" >
                                      @error('product_qty')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                  </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h5>Product tags vn<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                      <input type="text" name="product_tags_vn" value="Lorem,Ipsum,Amet" data-role="tagsinput" required="" placeholder="add tags" style="display: none;">
                                      @error('product_tags_vn')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                  </div>
                            </div>
                        </div> {{-- end row  --}}
                        <div class="row">{{-- start row  --}}
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h5>Product tags en <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="product_tags_en" name="product_tags_en"  value="Lorem,Ipsum,Amet" required="" id="product_tags_en" class="form-control" data-role="tagsinput" />
                                      @error('product_tags_en')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                  </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                           <h5>Product size vn <span class="text-danger">*</span></h5>
                           <div class="controls">
                    <input type="text" name="product_size_vn" class="form-control" required="" value="M,L,XL,XXL" data-role="tagsinput">
                    @error('product_size_vn')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                             </div>
                       </div>

                           </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h5>Product size en<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" value="M,L,XL,XXL"  data-role="tagsinput" required="" name="product_size_en" id="product_size_en" class="form-control" >
                                      @error('product_size_en')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                  </div>
                            </div>
                        </div> {{-- end row  --}}
                        <div class="row">{{-- start row  --}}
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h5>Product color vn <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="product_color_vn" value="xanh, đỏ, tím" required="" id="product_color_vn" class="form-control" data-role="tagsinput" />
                                      @error('product_tags_en')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                  </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h5>Product color en <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" value="Green,Red,Purple"  data-role="tagsinput" required=""  name="product_color_en" id="product_color_en" class="form-control" >
                                      @error('product_color_en')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                  </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h5>Selling price<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text"  name="selling_price" id="selling_price" required="" class="form-control" >
                                      @error('selling_price')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                  </div>
                            </div>
                        </div> {{-- end row  --}}
                        <div class="row">{{-- start row  --}}
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h5>Discount price<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="discount_price"  id="discount_price" class="form-control" />
                                      @error('discount_price')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                  </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h5>Product thumbnail<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="file" name="product_thumbnail" onchange="upload(this)" required=""  id="product_thumbnail" class="form-control" />
                                      @error('product_thumbnail')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                    <div><img src="" id="showImage" alt=""></div>
                                  </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h5>Multi images<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="file"   name="multi_image[]" multiple  onchange="uploadMulti(this)" required=""  class="form-control" >
                                    </div>
                                    <div class="showMultiImage">

                                    </div>
                                  </div>
                            </div>
                        </div> {{-- end row  --}}
{{-- start row  --}}
<div class="row">{{-- start row  --}}
    <div class="col-sm-6">
        <div class="form-group">
            <h5>Short description vn<span class="text-danger">*</span></h5>
            <div class="controls">
                <textarea id="short_descp_vn" name="short_descp_vn" class="form-control" required="">
                    This is my textarea to be replaced with CKEditor.
                </textarea>
              @error('short_descp_vn')
              <span class="text-danger">{{$message}}</span>
              @enderror
            </div>
          </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <h5>Short description en<span class="text-danger">*</span></h5>
            <div class="controls">
                <textarea id="short_descp_en" name="short_descp_en" class="form-control" required="">
                    This is my textarea to be replaced with CKEditor.
                </textarea>
              @error('short_descp_en')
              <span class="text-danger">{{$message}}</span>
              @enderror
            </div>
          </div>
    </div>

</div> {{-- end row  --}}
                        <div class="row">{{-- start row  --}}
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <h5>Long description vn<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <textarea id="editor1" name="long_descp_vn" class="form-control" required="">
                                            This is my textarea to be replaced with CKEditor.
                                        </textarea>
                                      @error('long_descp_vn')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                  </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <h5>Long description en<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <textarea id="long_descp_en" name="long_descp_en" class="form-control" required="">
                                            This is my textarea to be replaced with CKEditor.
                                        </textarea>
                                      @error('long_descp_en')
                                      <span class="text-danger">{{$message}}</span>
                                      @enderror
                                    </div>
                                  </div>
                            </div>

                        </div> {{-- end row  --}}

                        <div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<div class="controls">
										<fieldset>
											<input type="checkbox" name="hot_deals" value="1" id="checkbox_4" >
											<label for="checkbox_4">Hot deals</label>
										</fieldset>
										<fieldset>
											<input type="checkbox" value="1" name="featured" id="checkbox_5">
											<label for="checkbox_5">Feature</label>
										</fieldset>
							        </div>
                                </div>
                            </div>
							<div class="col-md-6">
								<div class="form-group">
									<div class="controls">
										<fieldset>
											<input type="checkbox" name="special_offer" id="checkbox_7" value="1" >
											<label for="checkbox_7">Special offer</label>
										</fieldset>
										<fieldset>
											<input type="checkbox" name="special_deals" id="checkbox_8" value="2">
											<label for="checkbox_8">Special deals</label>
										</fieldset>
                                    </div>
							    </div>
						    </div>
                        </div>
                  </div>
                    <div class="text-xs-right">
                        <input type="submit" class="btn btn-rounded btn-primary mb-5" value="Add Product">
                    </div>
                </form>

            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
<script>
    $(document).ready(function() {
        $(document).on('change', '.choose', function() {
            var _token = $('input[name="_token"]').val();
          var elementId = $(this).attr('id');
          var value = $(this).val();
          if(elementId === 'category_id') {
            $.ajax({
                    url: "{{ route('ajax.request.getSubCategories') }}",
                    type:'POST',
                    dataType: 'json', // dịnh dạng kiểu dữ liệu nhận về là json
                    data: {_token:_token, category_id:value},
                    success: function(data) {
                        // var data = JSON.parse(data) ;
                        // vì data nhận về là kiểu json trên có dataType = json nên jquery đã hiểu và phân tích dữ liệu để sử dụng
                        // như bình thường nếu không có dataType thì dữ liệu chỉ hiểu là chuỗi json mà jquery ko hiểu thực chất là một object
                        //nên ta phải dùng Json.parse để chuyển dữ liệu về type javascript
                       $('#subCategory_id').empty();
                       $('#subCategory_id').append(`<option  selected='' disabled>Select Subcategory</option>`);
                       $.each(data, function (index, value) {
                        $('#subCategory_id').append(`<option value="${value.id}">${value.subCategory_name_vn}</option>`);
                        });
                    }
                });
          }
          else if(elementId === 'subCategory_id') {
            $.ajax({
                    url: "{{ route('ajax.request.getSubSubCategories') }}",
                    type:'POST',
                    dataType: 'json', // dịnh dạng kiểu dữ liệu nhận về là json
                    data: {_token:_token, subCategory_id:value},
                    success: function(data) {
                        // var data = JSON.parse(data) ;
                        // vì data nhận về là kiểu json trên có dataType = json nên jquery đã hiểu và phân tích dữ liệu để sử dụng
                        // như bình thường nếu không có dataType thì dữ liệu chỉ hiểu là chuỗi json mà jquery ko hiểu thực chất là một object
                        //nên ta phải dùng Json.parse để chuyển dữ liệu về type javascript
                       $('#subsubCategory_id').empty();
                       $('#subsubCategory_id').append(`<option  selected='' disabled>Select Sub-subcategory</option>`);
                       $.each(data, function (index, value) {
                        $('#subsubCategory_id').append(`<option value="${value.id}">${value.Subsubcategory_name_vn}</option>`);
                        });
                    }
                });
          }
        });

    });

</script>
<script>

</script>
<script type="text/javascript">
  function upload(input) {
   if(input.files && input.files[0]) {
       var file = input.files[0];
       var reader = new FileReader();
       reader.onload = function(e) {
            $('#showImage').attr('src', e.target.result).width(80).height(80);
       }
       reader.readAsDataURL(file);
   }
  }

  function uploadMulti(input) {

      if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
      {
        $('.showMultiImage').empty();
          var data = input.files; //this file data

          $.each(data, function(index, file){ //loop though each file
              if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){ //check supported file type
                  var fRead = new FileReader(); //new filereader
                    fRead.onload = function(e) {
                        var img = $('<img/>').addClass('thumb').attr('src', e.target.result) .width(80).height(80);//create image element
                        $('.showMultiImage').append(img); //append image to output element
                    }
                  fRead.readAsDataURL(file); //URL representing the file's data.
              }
          });

      }else{
          alert("Your browser doesn't support File API!"); //if File API is absent
      }
  }

</script>
  @endsection

