@extends('frontend.main_layout')

@section('content')
<hr>
<div class="body-content">
	<div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="panel-group">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="unicase-checkout-title">Chi tiết người nhận</h4>
                        </div>
                        <div class="">
                            <table class="table">
                                <tr>
                                    <td>Tên người nhận</td>
                                    <td>{{$order->name}}</td>
                                </tr>
                                <tr>
                                    <td>Số điện thoại</td>
                                    <td>{{$order->phone}}</td>
                                </tr>
                                <tr>
                                    <td>Email người nhận</td>
                                    <td>{{$order->email}}</td>
                                </tr>
                                <tr>
                                    <td>Địa chỉ người nhận</td>
                                    <td>{{$order->notes}}</td>
                                </tr>
                                <tr>
                                    <td>Mã bưu điện</td>
                                    <td>{{$order->post_code}}</td>
                                </tr>
                                <tr>
                                    <td>Ngày đặt hàng</td>
                                    <td>{{$order->order_date}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-6">
                <div class="panel-group">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="unicase-checkout-title">Chi tiết đơn hàng</h4>
                        </div>
                        <div class="">
                            <table class="table">
                                <tr>
                                    <td>Người đặt hàng</td>
                                    <td>{{$order->user->name}}</td>
                                </tr>
                                <tr>
                                    <td>Số điện thoại người đặt</td>
                                    <td>{{$order->user->phone}}</td>
                                </tr>
                                <tr>
                                    <td>Phương thức đặt hàng</td>
                                    <td>{{$order->payment_type}}</td>
                                </tr>
                                <tr>
                                    <td>Mã giao dịch</td>
                                    <td>{{$order->transaction_id}}</td>
                                </tr>
                                <tr>
                                    <td>Mã đơn hàng</td>
                                    <td>{{$order->invoice_no}}</td>
                                </tr>
                                <tr>
                                    <td>Tổng đơn hàng</td>
                                    <td>{{number_format($order->amount,0,'','.')}}₫</td>
                                </tr>
                                <tr>
                                    <td>Trạng thái đơn hàng</td>
                                    <td><span class="badge badge-warning" style="background:#FFB800;">{{$order->status}}</span></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="shopping-cart-table ">
                    <div class="table-responsive">
                        <table class="table">
                            <div class="lds-ripple1"><div></div><div></div></div>
                            <thead>
                                <tr>
                                    <th class="cart-description item">Ảnh</th>
                                    <th class="cart-product-name item" >Tên sản phẩm</th>
                                    <th class="cart-product-color item" width="20%">Mã code sản phẩm</th>
                                    <th class="cart-product-size item">Màu</th>
                                    <th class="cart-edit item" width="10%">Kích cỡ</th>
                                    <th class="cart-price item">Giá</th>
                                    <th class="cart-qty item" width="10%">Số lượng</th>
                                    <th class="cart-sub-total item">Tổng</th>

                                </tr>
                            </thead><!-- /thead -->

                            <tbody>
                                @foreach ($order_products as $order_product)
                                    <tr>
                                        <td><img src="{{asset($order_product->product->product_thumbnail)}}" width="60" height="60" alt=""></td>
                                        <td>{{$order_product->product->product_name_vn}}</td>
                                        <td>{{$order_product->product->product_code}}</td>
                                        <td>{{$order_product->color}}</td>
                                        <td>{{$order_product->size}}</td>
                                        <td>{{number_format($order_product->price,0,'','.')}}₫</td>
                                        <td>{{$order_product->qty}}</td>
                                        <td>{{number_format($order_product->price * $order_product->qty,0,'','.')}}₫</td>
                                    </tr>
                                @endforeach
                            </tbody><!-- /tbody -->
                        </table><!-- /table -->
                    </div>
                </div><!-- /.shopping-cart-table -->
            </div>
            <div class="col-12">
                @if ($order->status !== 'delivered')

                @else
                @if ($order->return_reason != Null)
                <div class="alert alert-warning" role="alert">
                    Đơn hàng của bạn đã được yêu cầu hoàn trả. Tiếp tục mua hàng ấn <a style="color:#8a6d3b; text-decoration:underline;" href="{{url('/')}}">vào đây.</a>
                  </div>
                @else
                <form action="{{url('my-order/reason/'.$order->id)}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="return_reason">Lí do trả hàng:</label>
                        <textarea name="return_reason" id="return_reason" class="form-control"  rows="5"></textarea>
                    </div>
                    <button type="submit"  class="btn-upper btn btn-primary">Gửi thông tin</button>
                </form>
                @endif

                @endif
            </div>
        </div>





		</div><!-- /.sigin-in-->
</div><!-- /.body-content -->
<br>
<br>

<style>
    .table>thead>tr>th {
    vertical-align: bottom;
    border-bottom: 2px solid #4d9ebe;
    padding-left: 30px;
}
</style>
@endsection
