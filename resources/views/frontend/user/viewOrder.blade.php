@extends('frontend.main_layout')

@section('content')
<div class="body-content">
	<div class="container">
        <div class="row">
            {{-- Load user.sidebar --}}
            @include('frontend.common.user_sidebar')
            {{-- end user.sidebar --}}

            <div class="col-md-10">
                <div class="table-responsive">
                    <table class="table">

                        <thead>
                            <tr>
                                <th scope="col">Ngày đặt hàng</th>
                                <th scope="col">Tổng đơn hàng</th>
                                <th scope="col">Phương thức </th>
                                <th scope="col" >Mã hóa đơn</th>
                                <th scope="col" >Trạng thái order</th>
                                <th scope="col">Thao tác</th>
                            </tr>
                        </thead><!-- /thead -->

                        <tbody >
                            @foreach ($orders as $order)
                            <tr>
                                <td>{{$order->order_date}}</td>
                                <td>{{number_format($order->amount,0,'','.')}}₫</td>
                                <td>{{$order->payment_method}}</td>
                                <td>{{$order->invoice_no}}</td>
                                <td>
                                    @if ($order->status === 'Pending')
                                    <span class="badge badge-warning" style="background:#CCCCFF;">Đang chờ</span>
                                    @elseif ($order->status === 'confirmed')
                                    <span class="badge badge-warning" style="background:#FFBF00;">Đã xác nhận</span>
                                    @elseif ($order->status === 'processing')
                                    <span class="badge badge-warning" style="background:#FF7F50;">Đang xử lý</span>
                                    @elseif ($order->status === 'picked')
                                    <span class="badge badge-warning" style="background:#6495ED;">Đã đóng gói</span>
                                    @elseif ($order->status === 'shipped')
                                    <span class="badge badge-warning" style="background:#9FE2BF;">Đã vận chuyển</span>
                                    @elseif ($order->status === 'delivered')
                                    <span class="badge badge-warning" style="background:#40E0D0;">Đã nhận hàng</span>
                                    @if ($order->return_order == 1)
                                    <span class="badge badge-warning" style="background:#52BE80;">Đang hoàn trả lại đơn hàng</span>
                                    @endif
                                    @else
                                    <span class="badge badge-warning" style="background:#DE3163;">Đơn bị hủy</span>
                                    @endif

                                </td>
                                <td>
                                    <a href="{{url('my-order/detail/'.$order->id)}}"><i class="fa fa-eye"></i></a>
                                    <a href="{{url('my-order/invoice/'.$order->id)}}"><i class="fa fa-download"></i></a>
                                </td>
                            </tr>
                            @endforeach

                        </tbody><!-- /tbody -->
                    </table><!-- /table -->
                </div>
            </div>

        </div>



		</div><!-- /.sigin-in-->
</div><!-- /.body-content -->
<br>
<br>
<script>
    var showImage = document.querySelector('#showImage');
   var image = document.querySelector('#profile_photo_path');

   image.addEventListener('change', function(e) {
    const file = image.files[0];
      var reader = new FileReader();
      reader.onload = function() {
          showImage.setAttribute("src",reader.result);
      }
      if (file) {
            reader.readAsDataURL(file);
        }
   })

</script>
<style>
    .table>thead>tr>th {
    vertical-align: bottom;
    border-bottom: 2px solid #4d9ebe;
    padding-left: 25px;
}
</style>
@endsection

