@extends('frontend.main_layout')
@section('title', 'Shop Tyuq')
@section('content')
<div class="body-content outer-top-xs" id="top-banner-and-menu">
    <div class="container">
      <div class="row">
        <!-- ============================================== SIDEBAR ============================================== -->
        <div class="col-xs-12 col-sm-12 col-md-3 sidebar">

          <!-- ================================== TOP NAVIGATION ================================== -->
            @include('frontend.common.vertical_menu')
          <!-- /.side-menu -->
          <!-- ================================== TOP NAVIGATION : END ================================== -->

          <!-- ============================================== HOT DEALS ============================================== -->
          <div class="sidebar-widget hot-deals wow fadeInUp outer-bottom-xs">
            <h3 class="section-title">Sản phẩm giảm mạnh</h3>
            <div class="owl-carousel sidebar-carousel custom-carousel owl-theme outer-top-ss">
                @foreach ($products_hotDeals as $product)
                <div class="item">
                    <div class="products">
                    <div class="hot-deal-wrapper">
                        <div class="image"><a href="{{url('product/detail/'.$product->id.'/'.$product->product_slug_vn)}}"><img src="{{asset($product->product_thumbnail)}}" alt=""></a>  </div>
                        <div class="sale-offer-tag">
                            @php
                            $discount = ($product->selling_price - $product->discount_price)/$product->selling_price*100
                        @endphp
                            <span>{{round($discount)}}%<br>
                        off </span>

                   </div>
                        <div class="timing-wrapper">
                        <div class="box-wrapper">
                            <div class="date box"> <span class="key">120</span> <span class="value">DAYS</span> </div>
                        </div>
                        <div class="box-wrapper">
                            <div class="hour box"> <span class="key">20</span> <span class="value">HRS</span> </div>
                        </div>
                        <div class="box-wrapper">
                            <div class="minutes box"> <span class="key">36</span> <span class="value">MINS</span> </div>
                        </div>
                        <div class="box-wrapper hidden-md">
                            <div class="seconds box"> <span class="key">60</span> <span class="value">SEC</span> </div>
                        </div>
                        </div>
                    </div>
                    <!-- /.hot-deal-wrapper -->

                    <div class="product-info text-left m-t-20">
                        <h3 class="name"><a href="detail.html">{{$product->product_name_vn}}</a></h3>
                        <div class="rating rateit-small"></div>
                        <div class="product-price">
                            @if ($product->discount_price != Null)
                            <span class="price"> {{number_format($product->discount_price,0,'','.')}} Đ</span>
                            <span class="price-before-discount">{{number_format($product->selling_price,0,'','.')}} Đ</span>
                        @else
                        <span class="price"> {{number_format($product->selling_price,0,'','.')}} Đ</span>
                        @endif

                        </div>
                        <!-- /.product-price -->

                    </div>
                    <!-- /.product-info -->


                    </div>
                </div>
                @endforeach


            </div>
            <!-- /.sidebar-widget -->
          </div>
          <!-- ============================================== HOT DEALS: END ============================================== -->

          <!-- ============================================== SPECIAL OFFER ============================================== -->

          <div class="sidebar-widget outer-bottom-small wow fadeInUp">
            <h3 class="section-title">Khuyến mãi đặc biệt</h3>
            <div class="sidebar-widget-body outer-top-xs">
              <div class="owl-carousel sidebar-carousel special-offer custom-carousel owl-theme outer-top-xs">
                  @foreach (array_chunk($product_special_offer->toArray(),3) as $groupProduct)
                  <div class="item">
                    <div class="products special-product">
                        @foreach ($groupProduct as $product)
                            <div class="product">
                                <div class="product-micro">
                                <div class="row product-micro-row">
                                    <div class="col col-xs-5">
                                    <div class="product-image">
                                        <div class="image"> <a href="{{url('product/detail/'.$product['id'].'/'.$product['product_slug_vn'])}}"> <img src="{{asset($product['product_thumbnail'])}}" alt=""> </a> </div>
                                        <!-- /.image -->
                                    </div>
                                    <!-- /.product-image -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col col-xs-7">
                                    <div class="product-info">
                                        <h3 class="name"><a href="#">{{$product['product_name_vn']}}</a></h3>
                                        <div class="rating rateit-small"></div>
                                        <div class="product-price">
                                             <span class="price"> {{number_format($product['selling_price'],0,'','.')}} Đ</span>
                                        </div>
                                        <!-- /.product-price -->
                                    </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.product-micro-row -->
                                </div>
                                <!-- /.product-micro -->

                            </div>
                        @endforeach


                    </div>
                  </div>
                  @endforeach


              </div>
            </div>
            <!-- /.sidebar-widget-body -->
          </div>
          <!-- /.sidebar-widget -->
          <!-- ============================================== SPECIAL OFFER : END ============================================== -->
          <!-- ============================================== PRODUCT TAGS ============================================== -->

          @include('frontend.common.product_tags')
          <!-- /.sidebar-widget -->
          <!-- ============================================== PRODUCT TAGS : END ============================================== -->
          <!-- ============================================== SPECIAL DEALS ============================================== -->

          <div class="sidebar-widget outer-bottom-small wow fadeInUp">
            <h3 class="section-title">Special Deals</h3>
            <div class="sidebar-widget-body outer-top-xs">
              <div class="owl-carousel sidebar-carousel special-offer custom-carousel owl-theme outer-top-xs">
                @foreach (array_chunk($product_special_deals->toArray(),3) as $groupProduct)
                <div class="item">
                  <div class="products special-product">
                    @foreach ($groupProduct as $product)
                    <div class="product">
                      <div class="product-micro">
                        <div class="row product-micro-row">
                          <div class="col col-xs-5">
                            <div class="product-image">
                              <div class="image"> <a href="#"> <img src="{{asset($product['product_thumbnail'])}}"  alt=""> </a> </div>
                              <!-- /.image -->

                            </div>
                            <!-- /.product-image -->
                          </div>
                          <!-- /.col -->
                          <div class="col col-xs-7">
                            <div class="product-info">
                              <h3 class="name"><a href="#">{{$product['product_name_vn']}}</a></h3>
                              <div class="rating rateit-small"></div>
                              <div class="product-price"> <span class="price"> {{number_format($product['selling_price'],0,'','.')}} Đ </span> </div>
                              <!-- /.product-price -->

                            </div>
                          </div>
                          <!-- /.col -->
                        </div>
                        <!-- /.product-micro-row -->
                      </div>
                      <!-- /.product-micro -->

                    </div>
                   @endforeach
                  </div>
                </div>

                @endforeach
              </div>
            </div>
            <!-- /.sidebar-widget-body -->
          </div>
          <!-- /.sidebar-widget -->
          <!-- ============================================== SPECIAL DEALS : END ============================================== -->


        </div>
        <!-- /.sidemenu-holder -->
        <!-- ============================================== SIDEBAR : END ============================================== -->

        <!-- ============================================== CONTENT ============================================== -->
        <div class="col-xs-12 col-sm-12 col-md-9 homebanner-holder">
          <!-- ========================================== SECTION – HERO ========================================= -->
          <div id="hero">
            <div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">
                @foreach ($sliders as $slider)
                <div class="item" style="background-image: url({{$slider->slider_img}});">
                    <div class="container-fluid">
                    <div class="caption bg-color vertical-center text-left">
                        <div class="big-text fadeInDown-1"> {{$slider->slider_title}}</div>
                        <div class="excerpt fadeInDown-2 hidden-xs"> <span>{{$slider->slider_description}}</span> </div>
                        {{-- <div class="button-holder fadeInDown-3"> <a href="index.php?page=single-product" class="btn-lg btn btn-uppercase btn-primary shop-now-button">Shop Now</a> </div> --}}
                    </div>
                    <!-- /.caption -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                @endforeach
              <!-- /.item -->

            </div>
            <!-- /.owl-carousel -->
          </div>

          <!-- ========================================= SECTION – HERO : END ========================================= -->


          <!-- ============================================== SCROLL TABS ============================================== -->
          <div id="product-tabs-slider" class="scroll-tabs outer-top-vs wow fadeInUp">
            <div class="more-info-tab clearfix ">
              <h3 class="new-product-title pull-left">Sản phẩm mới</h3>
              <ul class="nav nav-tabs nav-tab-line pull-right" id="new-products-1">
                <li class="active"><a data-transition-type="backSlide" href="#all" data-toggle="tab">Tất cả</a></li>
                @foreach ($categories as $cat)
                    <li><a data-transition-type="backSlide" href="#category{{$cat->id}}" data-toggle="tab">{{$cat->category_name_vn}}</a></li>
                @endforeach

              </ul>
              <!-- /.nav-tabs -->
            </div>
            <div class="tab-content outer-top-xs">
                <div class="tab-pane in active" id="all">
                    <div class="product-slider">
                      <div class="owl-carousel home-owl-carousel custom-carousel owl-theme" data-item="4">
                         @foreach ($products as $pro)
                        <div class="item item-carousel">
                          <div class="products">
                            <div class="product">
                              <div class="product-image">
                                <div class="image"> <a href="{{url('product/detail/'.$pro->id.'/'.$pro->product_slug_vn)}}"><img  src="{{$pro->product_thumbnail}}" alt=""></a> </div>
                                <!-- /.image -->

                                @if ($pro->discount_price != Null)
                                        @php
                                            $discount = ($pro->selling_price - $pro->discount_price)/$pro->selling_price*100
                                        @endphp
                                        <div class="tag hot"><span>{{round($discount)}}%</span></div>
                                        @else
                                            <div class="tag new"><span>new</span></div>
                                        @endif
                              </div>
                              <!-- /.product-image -->

                              <div class="product-info text-left">
                                <h3 class="name"><a href="detail.html">{{$pro->product_name_vn}}</a></h3>
                                <div class="rating rateit-small"></div>
                                <div class="description"></div>
                                <div class="product-price">
                                    @if ($pro->discount_price != Null)
                                        <span class="price">{{number_format($pro->discount_price,0,'','.')}} Đ</span>
                                        <span class="price-before-discount"> {{number_format($pro->selling_price,0,'','.')}} Đ</span>
                                    @else
                                         <span class="price"> {{number_format($pro->selling_price,0,'','.')}} Đ</span>
                                    @endif
                                </div>
                                <!-- /.product-price -->

                              </div>
                              <!-- /.product-info -->
                              <div class="cart clearfix animate-effect">
                                <div class="action">
                                  <ul class="list-unstyled">
                                    <li class="add-cart-button btn-group">
                                        <button  onclick="viewModalProduct(this.id)" id="{{$pro->id}}" class="btn btn-primary icon btnadd" data-toggle="modal" data-target="#exampleModal"  type="button" title="Add Cart"> <i class="fa fa-shopping-cart"></i> </button>
                                      <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                    </li>
                                    <li class="lnk wishlist"> <a data-toggle="tooltip" data-id="{{$pro->id}}" class="add-to-cart addWishlist" href="" title="Yêu thích"> <i class="icon fa fa-heart"></i> </a> </li>

                                  </ul>
                                </div>
                                <!-- /.action -->
                              </div>
                              <!-- /.cart -->
                            </div>
                            <!-- /.product -->

                          </div>
                          <!-- /.products -->
                        </div>
                        <!-- /.item -->
                       @endforeach
                      </div>
                      <!-- /.home-owl-carousel -->
                    </div>
                    <!-- /.product-slider -->
                  </div>
                  <!-- /.tab-pane -->
                @foreach ($categories as $category)
                    <div class="tab-pane" id="category{{$category->id}}">
                        <div class="product-slider">
                        <div class="owl-carousel home-owl-carousel custom-carousel owl-theme" data-item="4">
                            @php
                                $products = App\Models\Product::where('status',1)->where('category_id', $category->id)->limit(12)->get();
                            @endphp
                            @forelse ( $products as $product)
                            <div class="item item-carousel">
                                <div class="products">
                                    <div class="product">
                                    <div class="product-image">
                                        <div class="image"> <a href="{{url('product/detail/'.$product->id.'/'.$product->product_slug_vn)}}"><img  src="{{asset($product->product_thumbnail)}}" alt=""></a> </div>
                                        <!-- /.image -->
                                        @if ($product->discount_price != Null)
                                        @php
                                            $discount = ($product->selling_price - $product->discount_price)/$product->selling_price*100
                                        @endphp
                                        <div class="tag hot"><span>{{round($discount)}}%</span></div>
                                        @else
                                            <div class="tag new"><span>new</span></div>
                                        @endif

                                    </div>
                                    <!-- /.product-image -->

                                    <div class="product-info text-left">
                                        <h3 class="name"><a href="detail.html">{{$product->product_name_vn}}</a></h3>
                                        <div class="rating rateit-small"></div>
                                        <div class="description"></div>

                                        <div class="product-price">
                                            @if ($product->discount_price != Null)
                                                <span class="price"> {{number_format($product->selling_price,0,'','.')}} Đ</span>
                                                <span class="price-before-discount">{{number_format($product->discount_price,0,'','.')}} Đ</span>
                                            @else
                                            <span class="price"> {{number_format($product->selling_price,0,'','.')}} Đ</span>
                                            @endif
                                        </div>
                                        <!-- /.product-price -->

                                    </div>
                                    <!-- /.product-info -->
                                    <div class="cart clearfix animate-effect">
                                        <div class="action">
                                        <ul class="list-unstyled">
                                            <li class="add-cart-button btn-group">
                                            <button data-toggle="tooltip" class="btn btn-primary icon" type="button" title="Add Cart"> <i class="fa fa-shopping-cart"></i> </button>
                                            <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                            </li>
                                            <li class="lnk wishlist"> <a data-toggle="tooltip" class="add-to-cart" href="detail.html" title="Wishlist"> <i class="icon fa fa-heart"></i> </a> </li>


                                        </ul>
                                        </div>
                                        <!-- /.action -->
                                    </div>
                                    <!-- /.cart -->
                                    </div>
                                    <!-- /.product -->
                                </div>
                                <!-- /.products -->
                                </div>
                                <!-- /.item -->
                            @empty
                            <span class="text-danger">Tạm thời không có sản phẩm nào.</span>
                            @endforelse
                        </div>
                        <!-- /.home-owl-carousel -->
                        </div>
                        <!-- /.product-slider -->
                    </div>
                    <!-- /.tab-pane -->
                @endforeach
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.scroll-tabs -->
          <!-- ============================================== SCROLL TABS : END ============================================== -->
          <!-- ============================================== WIDE PRODUCTS ============================================== -->

          <!-- /.wide-banners -->

          <!-- ============================================== WIDE PRODUCTS : END ============================================== -->
          <!-- ============================================== FEATURED PRODUCTS ============================================== -->
          <section class="section featured-product wow fadeInUp">
            <h3 class="section-title">Sản phẩm nổi bật</h3>
            <div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">
@foreach ($products_feature as $product)
<div class="item item-carousel">
    <div class="products">
        <div class="product">
        <div class="product-image">
            <div class="image"> <a href="{{url('product/detail/'.$product->id.'/'.$product->product_slug_vn)}}"><img  src="{{asset($product->product_thumbnail)}}" alt=""></a> </div>
            <!-- /.image -->
            @if ($product->discount_price != Null)
            @php
                $discount = ($product->selling_price - $product->discount_price)/$product->selling_price*100
            @endphp
            <div class="tag hot"><span>{{round($discount)}}%</span></div>
            @else
                <div class="tag new"><span>new</span></div>
            @endif
        </div>
        <!-- /.product-image -->

        <div class="product-info text-left">
            <h3 class="name"><a href="detail.html">{{$product->product_name_vn}}</a></h3>
            <div class="rating rateit-small"></div>
            <div class="description"></div>

            <div class="product-price">
                @if ($product->discount_price != Null)
                    <span class="price">{{number_format($product->discount_price,0,'','.')}} Đ</span>
                    <span class="price-before-discount">{{number_format($product->selling_price,0,'','.')}}Đ</span>
                @else
                <span class="price"> {{number_format($product->selling_price,0,'','.')}} Đ</span>
                @endif
            </div>


            <!-- /.product-price -->

        </div>
        <!-- /.product-info -->
        <div class="cart clearfix animate-effect">
            <div class="action">
            <ul class="list-unstyled">
                <li class="add-cart-button btn-group">
                <button  onclick="viewModalProduct(this.id)" id="{{$product->id}}" class="btn btn-primary icon btnadd" data-toggle="modal" data-target="#exampleModal"  type="button" title="Add Cart"> <i class="fa fa-shopping-cart"></i> </button>
                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                </li>
                <li class="lnk wishlist"> <a data-toggle="tooltip" data-id="{{$product->id}}" class="add-to-cart addWishlist" href="" title="Yêu thích"> <i class="icon fa fa-heart"></i> </a> </li>

            </ul>
            </div>
            <!-- /.action -->
        </div>
        <!-- /.cart -->
        </div>
        <!-- /.product -->
    </div>
    <!-- /.products -->
    </div>
    <!-- /.item -->
@endforeach



              <!-- /.item -->
            </div>
            <!-- /.home-owl-carousel -->
          </section>
          <!-- /.section -->
          <!-- ============================================== FEATURED PRODUCTS : END ============================================== -->
          <!-- ============================================== WIDE PRODUCTS ============================================== -->

          <!-- /.wide-banners -->
          <!-- ============================================== WIDE PRODUCTS : END ============================================== -->
          <!-- ============================================== PRODUCT CATEGORY ============================================== -->

          <section class="section featured-product wow fadeInUp">
            <h3 class="section-title">{{$skip_category_0->category_name_vn}}</h3>
            <div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">
@foreach ($skip_product_0 as $product)
<div class="item item-carousel">
    <div class="products">
        <div class="product">
        <div class="product-image">
            <div class="image"> <a href="{{url('product/detail/'.$product->id.'/'.$product->product_slug_vn)}}"><img  src="{{asset($product->product_thumbnail)}}" alt=""></a> </div>
            <!-- /.image -->
            @if ($product->discount_price != Null)
            @php
                $discount = ($product->selling_price - $product->discount_price)/$product->selling_price*100
            @endphp
            <div class="tag hot"><span>{{round($discount)}}%</span></div>
            @else
                <div class="tag new"><span>new</span></div>
            @endif

        </div>
        <!-- /.product-image -->

        <div class="product-info text-left">
            <h3 class="name"><a href="detail.html">{{$product->product_name_vn}}</a></h3>
            <div class="rating rateit-small"></div>
            <div class="description"></div>

            <div class="product-price">
                @if ($product->discount_price != Null)
                    <span class="price">{{number_format($product->discount_price,0,'','.')}} Đ</span>
                    <span class="price-before-discount"> {{number_format($product->selling_price,0,'','.')}} Đ</span>
                @else
                <span class="price"> {{number_format($product->selling_price,0,'','.')}} Đ</span>
                @endif
            </div>


            <!-- /.product-price -->

        </div>
        <!-- /.product-info -->
        <div class="cart clearfix animate-effect">
            <div class="action">
            <ul class="list-unstyled">
                <li class="add-cart-button btn-group">
                    <button  onclick="viewModalProduct(this.id)" id="{{$product->id}}" class="btn btn-primary icon btnadd" data-toggle="modal" data-target="#exampleModal"  type="button" title="Add Cart"> <i class="fa fa-shopping-cart"></i> </button>
                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                </li>
                <li class="lnk wishlist"> <a data-toggle="tooltip" data-id="{{$product->id}}" class="add-to-cart addWishlist" href="" title="Yêu thích"> <i class="icon fa fa-heart"></i> </a> </li>

            </ul>
            </div>
            <!-- /.action -->
        </div>
        <!-- /.cart -->
        </div>
        <!-- /.product -->
    </div>
    <!-- /.products -->
    </div>
    <!-- /.item -->
@endforeach



              <!-- /.item -->
            </div>
            <!-- /.home-owl-carousel -->
          </section>
          <section class="section featured-product wow fadeInUp">
            <h3 class="section-title">{{$skip_category_1->category_name_vn}}</h3>
            <div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">
@foreach ($skip_product_1 as $product)
<div class="item item-carousel">
    <div class="products">
        <div class="product">
        <div class="product-image">
            <div class="image"> <a href="{{url('product/detail/'.$product->id.'/'.$product->product_slug_vn)}}"><img  src="{{asset($product->product_thumbnail)}}" alt=""></a> </div>
            <!-- /.image -->
            @if ($product->discount_price != Null)
            @php
                $discount = ($product->selling_price - $product->discount_price)/$product->selling_price*100
            @endphp
            <div class="tag hot"><span>{{round($discount)}}%</span></div>
            @else
                <div class="tag new"><span>new</span></div>
            @endif

        </div>
        <!-- /.product-image -->

        <div class="product-info text-left">
            <h3 class="name"><a href="detail.html">{{$product->product_name_vn}}</a></h3>
            <div class="rating rateit-small"></div>
            <div class="description"></div>

            <div class="product-price">
                @if ($product->discount_price != Null)
                    <span class="price"> {{number_format($product->discount_price,0,'','.')}} Đ</span>
                    <span class="price-before-discount">{{number_format($product->selling_price,0,'','.')}} Đ</span>
                @else
                <span class="price"> {{number_format($product->selling_price,0,'','.')}} Đ</span>
                @endif
            </div>


            <!-- /.product-price -->

        </div>
        <!-- /.product-info -->
        <div class="cart clearfix animate-effect">
            <div class="action">
            <ul class="list-unstyled">
                <li class="add-cart-button btn-group">
                    <button  onclick="viewModalProduct(this.id)" id="{{$pro->id}}" class="btn btn-primary icon btnadd" data-toggle="modal" data-target="#exampleModal"  type="button" title="Add Cart"> <i class="fa fa-shopping-cart"></i> </button>
                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                </li>
                <li class="lnk wishlist"> <a data-toggle="tooltip" data-id="{{$pro->id}}" class="add-to-cart addWishlist" href="" title="Yêu thích"> <i class="icon fa fa-heart"></i> </a> </li>

            </ul>
            </div>
            <!-- /.action -->
        </div>
        <!-- /.cart -->
        </div>
        <!-- /.product -->
    </div>
    <!-- /.products -->
    </div>
    <!-- /.item -->
@endforeach



              <!-- /.item -->
            </div>
            <!-- /.home-owl-carousel -->
          </section>

          <!-- ============================================== PRODUCT CATEGORY  : END ============================================== -->



          <!-- ============================================== BLOG SLIDER ============================================== -->
          <section class="section latest-blog outer-bottom-vs wow fadeInUp">
            <h3 class="section-title">Bài viết mới nhất</h3>
            <div class="blog-slider-container outer-top-xs">
              <div class="owl-carousel blog-slider custom-carousel">
                @foreach ($blogs as $blog)
                <div class="item">
                    <div class="blog-post">
                      <div class="blog-post-image">
                        <div class="image"> <a href="{{url('blog/'.$blog->post_slug_vn)}}"><img src="{{asset($blog->post_image)}}" alt=""></a> </div>
                      </div>
                      <!-- /.blog-post-image -->
                      <div class="blog-post-info text-left">
                        <h3 class="name" style="margin-top:12px;"><a href="{{url('blog/'.$blog->post_slug_vn)}}">{{$blog->post_title_vn}}</a></h3>
                        <span class="info">{{$blog->created_at}}</span>
                        {{-- <p class="text">  {!! Str::limit($blog->post_detail_vn, 150) !!}</p> --}}
                        <a href="{{url('blog/'.$blog->post_slug_vn)}}" class="lnk btn btn-primary">Đọc thêm</a>
                      </div>
                      <!-- /.blog-post-info -->

                    </div>
                    <!-- /.blog-post -->
                  </div>
                  <!-- /.item -->
                @endforeach
              </div>
              <!-- /.owl-carousel -->
            </div>
            <!-- /.blog-slider-container -->
          </section>

























        </div>
        <!-- /.homebanner-holder -->
        <!-- ============================================== CONTENT : END ============================================== -->
      </div>
      <!-- /.row -->
      <!-- ============================================== BRANDS CAROUSEL ============================================== -->
     @include('frontend.body.brands')
      <!-- /.logo-slider -->
      <!-- ============================================== BRANDS CAROUSEL : END ============================================== -->
    </div>
    <!-- /.container -->
  </div>

@endsection
