
@php
$user = App\Models\User::findOrFail(Auth::id());
@endphp
<div class="col-md-2">
    <br>
    <div><img width="100%" height="100%" style="border-radius:50%;" src="{{!empty($user->profile_photo_path) ? url('upload/user_profile/'.$user->profile_photo_path) : url('upload/user3-128x128.jpg')}}"  alt=""></div>
    <br>
    <div>
        <ul class="list-group list-group-flush">
            <a href="{{route('dashboard')}}"  class="btn btn-primary btn-sm btn-block">Trang chủ</a>
            <a href="{{route('user.profile')}}" class="btn btn-primary btn-sm btn-block">Cập nhật thông tin</a>
            <a href="{{route('order.view')}}" class="btn btn-primary btn-sm btn-block">Đơn hàng của bạn</a>
            <a href="{{route('user.changPassword')}}" class="btn btn-primary btn-sm btn-block">Thay đổi mật khẩu</a>
            <a href="{{route('user.logout')}}" class="btn btn-danger btn-sm btn-block">Đăng xuất</a>
        </ul>
    </div>
</div>
