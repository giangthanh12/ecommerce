@php
    $product_tags = App\Models\Product::groupBy('product_tags_vn')->select('product_tags_vn')->get();
@endphp
<div class="sidebar-widget product-tag wow fadeInUp">
    <h3 class="section-title">Product tags</h3>
    <div class="sidebar-widget-body outer-top-xs">
      <div class="tag-list">
          @foreach ($product_tags as $tag)
              <a class="item active" title="Phone" href="{{url('product/tag/'.$tag->product_tags_vn)}}">#{{$tag->product_tags_vn}}</a>
          @endforeach
      <!-- /.tag-list -->
    </div>
    <!-- /.sidebar-widget-body -->
  </div>
</div>
