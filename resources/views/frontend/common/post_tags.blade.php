
@php
    $post_tags = App\Models\Post::groupBy('post_tag')->select('post_tag')->get();
@endphp

<div class="sidebar-widget product-tag wow fadeInUp">
	<h3 class="section-title">Post tags</h3>
	<div class="sidebar-widget-body outer-top-xs">
		<div class="tag-list">
            @foreach ($post_tags as $tag)
                <a class="item"  href="{{url('blog/tag/'.$tag->post_tag)}}">#{{$tag->post_tag}}</a>
            @endforeach
		</div><!-- /.tag-list -->
	</div><!-- /.sidebar-widget-body -->
</div><!-- /.sidebar-widget -->
