@php
          $categories =  App\Models\Category::orderBy('category_name_vn', 'ASC')->get();
@endphp

<div class="side-menu animate-dropdown outer-bottom-xs">
    <div class="head"><i class="icon fa fa-align-justify fa-fw"></i>Danh mục</div>
    <nav class="yamm megamenu-horizontal">
      <ul class="nav">
        @foreach ($categories as $category)
        <li class="dropdown menu-item"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon {{$category->category_icon}}" aria-hidden="true"></i>{{$category->category_name_vn}}</a>
            <ul class="dropdown-menu mega-menu">
              <li class="yamm-content">
                <div class="row">
                    @php
                         $subCateogries = App\Models\SubCategory::where('category_id', $category->id)->orderBy('subCategory_name_vn', 'ASC')->get();
                    @endphp
                    @foreach ($subCateogries as $subCategory)
                    <div class="col-sm-12 col-md-3">
                     <h2 class="title"><a style="padding: 0px;" href="{{url('subcategory/'.$subCategory->id.'/'.$subCategory->subCategory_slug_vn)}}">{{$subCategory->subCategory_name_vn}}</a></h2>
                        <ul class="links list-unstyled">
                            @php
                                   $subsubCategories = App\Models\SubSubCategory::where('subCategory_id', $subCategory->id)->orderBy('Subsubcategory_name_vn', 'ASC')->get();
                            @endphp
                        @foreach ($subsubCategories as $subsubCategory)
                            <li><a href="{{url('subsubCategory/'.$subsubCategory->id.'/'.$subsubCategory->Subsubcategory_slug_vn)}}">{{$subsubCategory->Subsubcategory_name_vn}}</a></li>
                        @endforeach


                        </ul>
                  </div>
                    @endforeach

                  <!-- /.col -->

                </div>
                <!-- /.row -->
              </li>
              <!-- /.yamm-content -->
            </ul>
            <!-- /.dropdown-menu --> </li>
          <!-- /.menu-item -->
        @endforeach
      </ul>
      <!-- /.nav -->
    </nav>
    <!-- /.megamenu-horizontal -->
  </div>
