@extends('frontend.main_layout')

@section('content')
<div class="body-content">
	<div class="container">
        <div class="row">
           {{-- Load user.sidebar --}}
           @include('frontend.common.user_sidebar')
           {{-- end user.sidebar --}}
            <div class="col-md-2">

            </div>
            <div class="col-md-6">
                <div class="card">
                    <h3 class="text-center">Xin chào <strong>{{Auth::user()->name}}</strong>, cập nhật thông tin của bạn tại đây.</h3>
                <div class="body-card">
                   <form action="{{route('user.storeProfile')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label class="info-title" for="name">Tây đầy đủ<span>*</span></label>
                        <input type="text" class="form-control unicase-form-control text-input" value="{{$user->name}}" name="name"  id="name" >
                    </div>
                    <div class="form-group">
                        <label class="info-title" for="email">Email<span>*</span></label>
                        <input type="email" class="form-control unicase-form-control text-input" value="{{$user->email}}" name="email"  id="email" >
                    </div>
                    <div class="form-group">
                        <label class="info-title" for="phone">Số điện thoại<span></span></label>
                        <input type="text" class="form-control unicase-form-control text-input" name="phone"  value="{{$user->phone}}"  id="phone" >
                    </div>
                    <div class="form-group">
                        <label class="info-title" for="profile_photo_path">Cập nhật ảnh<span></span></label>
                        <input type="file"  class="form-control unicase-form-control text-input" name="profile_photo_path"  id="profile_photo_path" >
                    </div>
                    <button type="submit" class="btn-upper btn btn-primary checkout-page-button">Cập nhật</button>
                    </form>
                </div>
                </div>
            </div>

        </div>



		</div><!-- /.sigin-in-->
</div><!-- /.body-content -->
<br>
<br>
<script>
    var showImage = document.querySelector('#showImage');
   var image = document.querySelector('#profile_photo_path');

   image.addEventListener('change', function(e) {
    const file = image.files[0];
      var reader = new FileReader();
      reader.onload = function() {
          showImage.setAttribute("src",reader.result);
      }
      if (file) {
            reader.readAsDataURL(file);
        }
   })

</script>
@endsection

