@extends('frontend.main_layout')

@section('content')
<div class="body-content">
	<div class="container">
        <div class="row">
            {{-- Load user.sidebar --}}
            @include('frontend.common.user_sidebar')
            {{-- end user.sidebar --}}
            <div class="col-md-2">

            </div>
            <div class="col-md-6">
                <div class="card">
                    <h3 class="text-center">Đổi mật khẩu</h3>
                <div class="body-card">
                   <form action="{{route('user.saveChangePassword')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label class="info-title" for="current_password">Mật khẩu hiện tại<span>*</span></label>
                        <input type="password" class="form-control unicase-form-control text-input"  name="current_password"  id="current_password" >
                        @error('current_password')
                            <span class="text-danger"><strong>{{$message}}</strong></span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="info-title" for="password">Mật khẩu mới<span>*</span></label>
                        <input type="password" class="form-control unicase-form-control text-input" name="password"  id="password" >
                        @error('password')
                            <span class="text-danger"><strong>{{$message}}</strong></span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="info-title" for="password_confirmation">Nhập lại mật khẩu<span>*</span></label>
                        <input type="password" class="form-control unicase-form-control text-input"  name="password_confirmation"  id="password_confirmation" >
                        @error('password_confirmation')
                            <span class="text-danger"><strong>{{$message}}</strong></span>
                        @enderror
                    </div>

                    <button type="submit" class="btn-upper btn btn-primary checkout-page-button">Cập nhật</button>
                    </form>
                </div>
                </div>
            </div>
        </div>



		</div><!-- /.sigin-in-->
</div><!-- /.body-content -->
<br>
<br>

@endsection

