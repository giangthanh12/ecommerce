@extends('frontend.main_layout')
@section('title', 'Giỏ hàng')
@section('content')
<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="#">Home</a></li>
				<li class='active'>Checkout</li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->

<div class="body-content">
	<div class="container">
		<div class="checkout-box ">
      <form action="{{route('checkout.save')}}" method="GET" id="form-checkout">
        	<div class="row">
				<div class="col-md-8">
					<div class="panel-group checkout-steps" id="accordion">
						<!-- checkout-step-01  -->
<div class="panel panel-default checkout-step-01">
	<!-- panel-heading -->
	<div class="panel-heading">
    	<h4 class="unicase-checkout-title">
	        <a>
	          <span>Thông tin thanh toán</span>
	        </a>
	     </h4>
    </div>
    <!-- panel-heading -->
	<div id="collapseOne" class="panel-collapse collapse in">

		<!-- panel-body  -->
	    <div class="panel-body">
			<div class="row">
				<!-- guest-login -->
				<div class="col-md-6 col-sm-6 guest-login">
					<div class="form-group">
					    <label class="info-title" for="shipping_name">Tên người nhận <span>*</span></label>
					    <input type="text" class="form-control unicase-form-control text-input" name="shipping_name" value="{{Auth::user()->name}}" id="shipping_name" placeholder="">
					</div>
                    <div class="form-group">
					    <label class="info-title" for="shipping_email">Email <span>*</span></label>
					    <input type="email" class="form-control unicase-form-control text-input" name="shipping_email" value="{{Auth::user()->email}}" id="shipping_email" placeholder="">
					</div>
                      <div class="form-group">
					    <label class="info-title" for="shipping_phone">Số điện thoại <span>*</span></label>
					    <input type="number" class="form-control unicase-form-control text-input" name="shipping_phone" value="{{Auth::user()->phone}}" id="shipping_phone" placeholder="">
					  </div>
                      <div class="form-group">
					    <label class="info-title" for="post_code">Mã bưu điện <span>*</span></label>
					    <input type="text" class="form-control unicase-form-control text-input" name="post_code" id="post_code" placeholder="">
					  </div>
				</div>
				<!-- guest-login -->

				<!-- already-registered-login -->
				<div class="col-md-6 col-sm-6 already-registered-login">
                    <div class="form-group">
                        <h5>Tỉnh thành<span class="text-danger"> * </span></h5>
                        <div class="controls">
                          <select name="matp" id="matp" class="form-control choose" >
                            <option  selected="" disabled>--- Lựa chọn ---</option>
                            @foreach ($provinces as $province)
                              <option value="{{$province->id}}">{{$province->name}}</option>
                            @endforeach
                          </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <h5>Quận huyện<span class="text-danger"> * </span></h5>
                        <div class="controls">
                          <select name="maqh" id="maqh" class="form-control choose" >
                            <option  selected="" disabled>--- Lựa chọn ---</option>
                          </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <h5>Xã phường<span class="text-danger"> * </span></h5>
                        <div class="controls">
                          <select name="maxa" id="maxa" class="form-control" >
                            <option  selected="" disabled>--- Lựa chọn ---</option>
                          </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <h5>Địa chỉ cụ thể<span class="text-danger"> * </span></h5>
                        <div class="controls">
                         <textarea name="notes" id="notes"  rows="5" class="form-control"></textarea>
                        </div>
                    </div>
				</div>
				<!-- already-registered-login -->
			</div>
		</div>
		<!-- panel-body  -->

	</div><!-- row -->
</div>
<!-- checkout-step-01  -->
					</div><!-- /.checkout-steps -->
				</div>
				<div class="col-md-4">
					<!-- checkout-progress-sidebar -->
          <div class="checkout-progress-sidebar ">
            <div class="panel-group">
              <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="unicase-checkout-title">Thông tin giỏ hàng</h4>
                  </div>
                  <div class="">
                  <ul class="nav nav-checkout-progress list-unstyled">
                              @foreach ($dataCart as $item)
                              <li>
                                  <img src="{{asset($item->options->image)}}" width="60" height="60"  alt="">

                                      <span><strong>Số lượng: </strong> {{$item->qty}}</span>
                                      <span><strong>Màu sắc: </strong> {{$item->options->color}}</span>
                                      <span><strong>Kích cỡ: </strong> {{$item->options->size}}</span>
                              </li>
                              @endforeach
                              <hr>
                              @if (Session::has('coupon'))
                                  <li><strong>Tạm tính: </strong> {{number_format($total,0,'','.')}} ₫</li>
                                  <hr>
                                  <li><strong>Coupon: </strong> {{Session::get('coupon')['coupon_name']}} ({{Session::get('coupon')['coupon_discount']}}%)</li>
                                  <hr>
                                  <li><strong>Số tiền giảm: </strong>{{number_format(Session::get('coupon')['ammount_discount'],0,'','.')}} ₫</li>
                                  <hr>
                                  <li><strong>Tổng tiền: </strong>{{number_format(Session::get('coupon')['grand_total'],0,'','.')}} ₫</li>
                              @else
                                  <li><strong>Tạm tính: </strong> {{number_format($total,0,'','.')}} ₫</li>
                                  <hr>
                                  <li><strong>Tổng tiền: </strong> {{number_format($total,0,'','.')}} ₫</li>
                              @endif
                  </ul>
                </div>
              </div>
            </div>
              <div class="panel-group">
              <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="unicase-checkout-title">Hình thức thanh toán</h4>
                  </div>
                  <div class="method-payment">
                          <div class="row">
                              <div class="col-sm-4">
                              <label for="">MOMO</label>
                              <input type="radio" name="payment_method" value="momo">
                            <div>
                                <img width="45" height="28" src="https://play-lh.googleusercontent.com/dQbjuW6Jrwzavx7UCwvGzA_sleZe3-Km1KISpMLGVf1Be5N6hN6-tdKxE5RDQvOiGRg" alt="">
                              </div>
                          </div>

                          <!-- <div class="col-sm-4">
                              <label for="">VN-PAY</label>
                              <input type="radio" name="payment_method" value="vnpay">
                            <div>
                                <img src="{{asset('frontend/assets/images/payments/2.png')}}" alt="" >
                              </div>
                          </div> -->
                          <div class="col-sm-4">
                              <label for="">Home</label>
                              <input type="radio" name="payment_method" checked value="home">
                            <div>
                                <img src="{{asset('frontend/assets/images/payments/9.jpg')}}" width="45" height="28" alt="" >
                              </div>
                          </div>
                          </div>
                          <hr>
                          <button type="submit" class="btn-upper btn btn-primary checkout-page-button">Thanh toán</button>

                </div>
              </div>
            </div>
          </div>
<!-- checkout-progress-sidebar -->
        </div>
			</div><!-- /.row -->
      </form>

		</div><!-- /.checkout-box -->
		<!-- ============================================== BRANDS CAROUSEL ============================================== -->
@include('frontend.body.brands')
<!-- ============================================== BRANDS CAROUSEL : END ============================================== -->	</div><!-- /.container -->
</div><!-- /.body-content -->
<style>
    label.error {
        color:red;
    }
</style>
<script type="text/javascript">
    $("#form-checkout").validate({
        rules: {
            shipping_name: {
                required: true,
            },
            shipping_email: {
                required: true,
                email: true
            },
            shipping_phone: {
                required: true,
                minlength:10
            },
            post_code:{
                required: true,
                minlength:6,
            },
            matp:{
                required: true,
            },
            maqh:{
                required: true,
            },
            maxa:{
                required: true,
            },
            notes:{
                required: true,
            },

        },
        messages: {
            shipping_name: {
                required: 'Vui lòng điền tên người nhận',
            },
            shipping_email: {
                required: 'Vui lòng điền địa chỉ email người nhận',
                email: 'Email không đúng định dạng'
            },
            shipping_phone: {
                required: 'Vui lòng điền số điện thoại người nhận',
                minlength:'Độ dài tối thiểu 10 ký tự'
            },
            post_code:{
                required: 'Vui lòng điền mã bưu điện',
                minlength:'Độ dài tối thiểu 6 ký tự',
            },
            matp:{
                required: 'Vui lòng chọn tỉnh thành phố',
            },
            maqh:{
                required: 'Vui lòng chọn quận huyện',
            },
            maxa:{
                required: 'Vui lòng chọn xã phường',
            },
            notes:{
                required: 'Vui lòng điền chi tiết địa chỉ',
            },

        }
        });
</script>
@endsection
