@extends('frontend.main_layout')
@section('title', 'Giỏ hàng')
@section('content')
<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="#">Home</a></li>
				<li class='active'>Checkout</li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->

<div class="body-content">
	<div class="container">
		<div class="checkout-box ">
            <form action="{{route('checkout.store')}}" method="POST">
                @csrf
        	<div class="row">
				<div class="col-md-6">
                        <h4 style="font-weight: bold;
                        text-transform: uppercase;">
                            Thông tin thanh toán
                         </h4>
                         <hr>
                    <ul class="nav nav-checkout-progress list-unstyled">

                        @if (Session::has('coupon'))
                            <li><strong>Tạm tính: </strong> {{number_format($total,0,'','.')}} ₫</li>
                            <hr>
                            <li><strong>Coupon: </strong> {{Session::get('coupon')['coupon_name']}} ({{Session::get('coupon')['coupon_discount']}}%)</li>
                            <hr>
                            <li><strong>Số tiền giảm: </strong>{{number_format(Session::get('coupon')['ammount_discount'],0,'','.')}} ₫</li>
                            <hr>
                            <li><strong>Tổng tiền: </strong>{{number_format(Session::get('coupon')['grand_total'],0,'','.')}} ₫</li>
                        @else
                            <li><strong>Tạm tính: </strong> {{number_format($total,0,'','.')}} ₫</li>
                            <hr>
                            <li><strong>Tổng tiền: </strong> {{number_format($total,0,'','.')}} ₫</li>
                        @endif
                     </ul>
                     <hr>
                     <input type="hidden" name="name" value="{{$data['shipping_name']}}">
                     <input type="hidden" name="email" value="{{$data['shipping_email']}}">
                     <input type="hidden" name="phone" value="{{$data['shipping_phone']}}">
                     <input type="hidden" name="post_code" value="{{$data['post_code']}}">
                     <input type="hidden" name="matp" value="{{$data['matp']}}">
                     <input type="hidden" name="maqh" value="{{$data['maqh']}}">
                     <input type="hidden" name="maxa" value="{{$data['maxa']}}">
                     <input type="hidden" name="notes" value="{{$data['notes']}}">
                     <input type="hidden" name="payment_method" value="{{$payment_method}}">
                     @if ($payment_method === 'home' )
                     <button type="submit" class="btn-upper btn btn-primary checkout-page-button" >Thanh toán tại nhà</button>
                     @endif
                      @if ($payment_method === 'momo' )
                     <button type="submit" class="btn-upper btn btn-primary checkout-page-button" >Thanh toán qua MOMO</button>
                     @endif
				</div>
				<div class="col-md-6">

                </div>
			</div>
        </form>
		</div><!-- /.checkout-box -->
		<!-- ============================================== BRANDS CAROUSEL ============================================== -->
@include('frontend.body.brands')
<!-- ============================================== BRANDS CAROUSEL : END ============================================== -->	</div><!-- /.container -->
</div><!-- /.body-content -->

@endsection
