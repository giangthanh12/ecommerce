@extends('frontend.main_layout')
@section('title', $product->product_name_vn)
@section('content')
<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="#">Trang chủ</a></li>
				<li class='active'>{{$product->product_name_vn}}</li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->
<div class="body-content outer-top-xs">
	<div class='container'>
		<div class='row single-product'>
			<div class='col-md-3 sidebar'>
				<div class="sidebar-module-container">


    	<!-- ============================================== HOT DEALS ============================================== -->
<div class="sidebar-widget hot-deals wow fadeInUp ">
	<h3 class="section-title">Sản phẩm bán chạy</h3>
	<div class="owl-carousel sidebar-carousel custom-carousel owl-theme ">



        @foreach ($products_hotDeals as $pro)
        <div class="item">
            <div class="products">
            <div class="hot-deal-wrapper">
                <div class="image"><a href="{{url('product/detail/'. $pro->id.'/'.$pro->product_slug_vn)}}"><img src="{{asset($pro->product_thumbnail)}}" alt=""></a>  </div>
                <div class="sale-offer-tag">
                    @php
                    $discount = ($pro->selling_price - $pro->discount_price)/$pro->selling_price*100
                @endphp
                    <span>{{round($discount)}}%<br>
                off </span>

           </div>
                <div class="timing-wrapper">
                <div class="box-wrapper">
                    <div class="date box"> <span class="key">120</span> <span class="value">DAYS</span> </div>
                </div>
                <div class="box-wrapper">
                    <div class="hour box"> <span class="key">20</span> <span class="value">HRS</span> </div>
                </div>
                <div class="box-wrapper">
                    <div class="minutes box"> <span class="key">36</span> <span class="value">MINS</span> </div>
                </div>
                <div class="box-wrapper hidden-md">
                    <div class="seconds box"> <span class="key">60</span> <span class="value">SEC</span> </div>
                </div>
                </div>
            </div>
            <!-- /.hot-deal-wrapper -->

            <div class="product-info text-left m-t-20">
                <h3 class="name"><a href="detail.html">{{$pro->product_name_vn}}</a></h3>
                <div class="rating rateit-small"></div>
                <div class="product-price">
                    @if ($pro->discount_price != Null)
                    <span class="price"> {{number_format($pro->discount_price,0,'','.')}} Đ</span>
                    <span class="price-before-discount">{{number_format($pro->selling_price,0,'','.')}} Đ</span>
                @else
                <span class="price"> {{number_format($pro->selling_price,0,'','.')}} Đ</span>
                @endif

                </div>
                <!-- /.product-price -->

            </div>
            <!-- /.product-info -->

            </div>
        </div>
        @endforeach
    </div><!-- /.sidebar-widget -->
</div>
<!-- ============================================== HOT DEALS: END ============================================== -->

				</div>
			</div><!-- /.sidebar -->
			<div class='col-md-9'>
            <div class="detail-block">
				<div class="row  wow fadeInUp">

					     <div class="col-xs-12 col-sm-6 col-md-5 gallery-holder">
    <div class="product-item-holder size-big single-product-gallery small-gallery">

        <div id="owl-single-product">
            <div class="single-product-gallery-item" id="slide">
                <a data-lightbox="image-1" data-title="Gallery" href="{{asset($product->product_thumbnail)}}">
                    <img class="img-responsive" id="product_modal_image" alt="" src="{{asset($product->product_thumbnail)}}" data-echo="{{asset($product->product_thumbnail)}}" />
                </a>
            </div><!-- /.single-product-gallery-item -->
            @foreach ($multiImages as $image)
            <div class="single-product-gallery-item" id="slide{{$image->id}}">
                <a data-lightbox="image-1" data-title="Gallery" href="{{asset($image->photo_name)}}">
                    <img class="img-responsive" alt="" src="{{asset($image->photo_name)}}" data-echo="{{asset($image->photo_name)}}" />
                </a>
            </div><!-- /.single-product-gallery-item -->
            @endforeach
        </div><!-- /.single-product-slider -->


        <div class="single-product-gallery-thumbs gallery-thumbs">

            <div id="owl-single-product-thumbnails">
                <div class="item">
                    <a class="horizontal-thumb active" data-target="#owl-single-product" data-slide="1" href="#slide">
                        <img class="img-responsive" width="85" alt="" src="{{asset($product->product_thumbnail)}}" data-echo="{{asset($product->product_thumbnail)}}" />
                    </a>
                </div>
                @php
                    $t = 1;
                @endphp
                @foreach ($multiImages as $image)
                <div class="item">
                    <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="{{++$t}}" href="#slide{{$image->id}}">
                        <img class="img-responsive" width="85" alt="" src="{{asset($image->photo_name)}}" data-echo="{{asset($image->photo_name)}}"/>
                    </a>
                </div>
                @endforeach
            </div><!-- /#owl-single-product-thumbnails -->
        </div><!-- /.gallery-thumbs -->

    </div><!-- /.single-product-gallery -->
</div><!-- /.gallery-holder -->
					<div class='col-sm-6 col-md-7 product-info-block'>
						<div class="product-info">
							<h1 class="name" id="product_modal_title">{{$product->product_name_vn}}</h1>
							<div class="stock-container info-container m-t-10">
								<div class="row">
									<div class="col-sm-2">
										<div class="stock-box">
											<span class="label">Tình trạng :</span>
										</div>
									</div>
									<div class="col-sm-9">
										<div class="stock-box">
                                            @if ($product->product_qty > 0)
                                                <span class="value">Còn hàng</span>
                                            @else
                                            <span class="value">Hết hàng</span>
                                            @endif

										</div>
									</div>
								</div><!-- /.row -->
							</div><!-- /.stock-container -->

							<div class="description-container m-t-20">
								{!! $product->short_descp_vn !!}
							</div><!-- /.description-container -->

							<div class="price-container info-container m-t-20">
								<div class="row">
									<div class="col-sm-6">
										<div class="price-box">
                                            @if ($product->discount_price != Null)
                                            <input type="hidden" id="product_price" value="{{$product->discount_price}}">
                                            <span class="price" style="font-size: 25px;"> {{number_format($product->discount_price,0,'','.')}} Đ</span>
                                            <span class="price-strike">{{number_format($product->selling_price,0,'','.')}} Đ</span>
                                            @else
                                                <span class="price"> {{number_format($product->selling_price,0,'','.')}} Đ</span>
                                                <input type="hidden" id="product_price" value="{{$product->selling_price}}">
                                            @endif
										</div>
									</div>

									<div class="col-sm-6">
										<div class="favorite-button m-t-10">
											<a class="btn btn-primary add-to-cart addWishlist" data-toggle="tooltip" data-id="{{$product->id}}" data-placement="right" title="Wishlist" href="#">
											    <i class="fa fa-heart"></i>
											</a>
										</div>
									</div>

								</div><!-- /.row -->
							</div><!-- /.price-container -->
                            <div class="select-option">
                                <div class="row">
                                    <div class="col-sm-6">
                                        @if ($product->product_color_vn != Null)
                                        <div class="form-group">
                                           <select class="form-control unicase-form-control selectpicker" id="product_modal_color" style="display: none;">
                                                <option value="notValue" disabled="" selected>--Chọn màu sản phẩm--</option>
                                                @foreach ($product_colors as $color)
                                                    <option value="{{$color}}">{{ucwords($color)}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @endif
                                    </div>
                                    <div class="col-sm-6">
                                        @if ($product->product_size_vn != Null)
                                        <div class="form-group">
                                            <select class="form-control unicase-form-control selectpicker" id="product_modal_size" style="display: none;">
                                                <option value="notValue" disabled="" selected>--Chọn kích cỡ sản phẩm--</option>
                                                @foreach ($product_sizes as $size)
                                                    <option value="{{$size}}">{{ucwords($size)}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>

							<div class="quantity-container info-container">
								<div class="row">

									<div class="col-sm-2">
										<span class="label">Số lượng :</span>
									</div>

									<div class="col-sm-2">
										<div class="cart-quantity">
                                            <div class="quant-input">
                                                <input type="number" id="product_qty" value="1" min="1">
                                            </div>
							            </div>
									</div>

									<div class="col-sm-7">

                                        <input type="hidden" id="product_id" value="{{$product->id}}">
										<button type="button" onclick="addToCart()" class="btn btn-primary"><i class="fa fa-shopping-cart inner-right-vs"></i> ADD TO CART</button>
									</div>


								</div><!-- /.row -->
							</div><!-- /.quantity-container -->






						</div><!-- /.product-info -->
					</div><!-- /.col-sm-7 -->
				</div><!-- /.row -->
                </div>

				<div class="product-tabs inner-bottom-xs  wow fadeInUp">
					<div class="row">
						<div class="col-sm-3">
							<ul id="product-tabs" class="nav nav-tabs nav-tab-cell">
								<li class="active"><a data-toggle="tab" href="#description">Mô tả sản phẩm</a></li>
								<li><a data-toggle="tab" href="#review">Đánh giá</a></li>

							</ul><!-- /.nav-tabs #product-tabs -->
						</div>
						<div class="col-sm-9">

							<div class="tab-content">

								<div id="description" class="tab-pane in active">
									<div class="product-tab">
										{!! $product->long_descp_vn!!}
									</div>
								</div><!-- /.tab-pane -->

								<div id="review" class="tab-pane">
                                {{-- row --}}
									<div class="row">
                                        <div id="rating">

                                            {{-- load rating --}}
                                        </div>

									</div>
                                    {{-- end row --}}
									<div class="row" >
										<div class="col-sm-4">
											<h3 class="mt-4 mb-3">Đánh giá sao</h3>
											<div class="mb-3">
                                                @for ($i = 1; $i <= 5; $i++)
                                                <p style="display:inline; cursor:pointer;" data-index="{{$i}}" data-product_id="{{$product->id}}" data-rating="{{$roundRating}}" id="{{$product->id}}-{{$i}}" class="ratings"><i style="font-size: 24px; color:{{$i <= $roundRating ? '#ffc107' : ''}}" class="fa fa-star star-light mr-1 main_star"></i></p>
                                                @endfor


											</div>
										</div>
										<div class="col-sm-8">

										</div>
									</div>
									<br>
									<style>
										.text-warning{
											color:#ffc107;
										}
									</style>
									{{-- end rating --}}
									<div class="product-tab">

										<div class="product-reviews">
											<h4 class="title">Khách hàng đánh giá</h4>
											{{-- rating --}}
				@php
					$reviewsdisplay = App\Models\Review::where('product_id',$product->id)->where('status',1)->limit(3)->orderBy('id', 'DESC')->get();
					$reviews = App\Models\Review::where('product_id',$product->id)->where('status',1)->orderBy('id', 'DESC')->get();

				@endphp
                                            @foreach ($reviewsdisplay as $review)
                                            <div class="reviews">
												<div class="review">
                                                    <div class="row" style="margin-bottom:5px;">
                                                        <div class="col-sm-3">
                                                            <img width="30" height="30" src="{{!empty($review->user->profile_photo_path) ? url('upload/user_profile/'.$review->user->profile_photo_path) : url('upload/user3-128x128.jpg')}}" alt="">
                                                            <b>{{$review->user->name}}</b>
                                                        </div>
                                                        <div class="col-sm-9">

                                                        </div>
                                                    </div>
													<div class="text">{{$review->comment}}</div>
												</div>
											</div><!-- /.reviews -->
                                            @endforeach
                                            <input type="hidden" name="product_id" id="product_id" value="{{$product->id}}">
                                            <input type="hidden" name="count_review" id="count_review" value="{{count($reviews)}}">
                                            <input type="hidden" id="offset_review" name="offset_review" value="{{count($reviews)-3}}">
                                            @if (count($reviews) >3)
                                            <style>
                                                #loader {
                                                  display: none;
                                                  border: 2px solid #f3f3f3;
                                                  border-radius: 50%;
                                                  border-top: 2px solid #3498db;
                                                  width: 17px;
                                                  height: 17px;
                                                  -webkit-animation: spin 2s linear infinite; /* Safari */
                                                  animation: spin 2s linear infinite;
                                                }

                                                /* Safari */
                                                @-webkit-keyframes spin {
                                                  0% { -webkit-transform: rotate(0deg); }
                                                  100% { -webkit-transform: rotate(360deg); }
                                                }

                                                @keyframes spin {
                                                  0% { transform: rotate(0deg); }
                                                  100% { transform: rotate(360deg); }
                                                }
                                                </style>

<a  id="loadmore" style="display:block; cursor: pointer;" class="text-center">Tải thêm </a>
<div id="loader"></div>
                                            @endif




										</div><!-- /.product-reviews -->



										<div class="product-add-review">


                                            @guest
                                            <p>Để đánh giá sản phẩm, bạn tiến hành đăng nhập <a href="{{route('login')}}">tại đây</a></p>
                                            @else

                                               <div class="review-form">
												<div class="form-container">
													<form role="form" action="{{route('Review.save')}}" method="POST" class="cnt-form">
                                                        @csrf
                                                        <input type="hidden" name="product_id" value="{{$product->id}}">
														<div class="row">
															<div class="col-md-12">
																<div class="form-group">
																	<label for="exampleInputReview">Đánh giá <span class="astk">*</span></label>
																	<textarea class="form-control txt txt-review" id="exampleInputReview" name="comment" rows="4" placeholder=""></textarea>
																</div><!-- /.form-group -->
															</div>
														</div><!-- /.row -->

														<div class="action text-right">
															<button type="submit" class="btn btn-primary btn-upper">Đánh giá</button>
														</div><!-- /.action -->
													</form><!-- /.cnt-form -->
												</div><!-- /.form-container -->
											</div><!-- /.review-form -->
                                            @endguest
                                                                  <!-- Go to www.addthis.com/dashboard to customize your tools -->


										</div><!-- /.product-add-review -->

							        </div><!-- /.product-tab -->
								</div><!-- /.tab-pane -->
							</div><!-- /.tab-content -->
						</div><!-- /.col -->
					</div><!-- /.row -->
				</div><!-- /.product-tabs -->

				<!-- ============================================== UPSELL PRODUCTS ============================================== -->
<section class="section featured-product wow fadeInUp">
	<h3 class="section-title">Sản phẩm tương tự</h3>
	<div class="owl-carousel home-owl-carousel upsell-product custom-carousel owl-theme outer-top-xs">

@foreach ($products_related as $product_related)
<div class="item item-carousel">
    <div class="products">

<div class="product">
<div class="product-image">
    <div class="image">
        <a href="{{url('product/detail/'.$product_related->id.'/'.$product_related->product_slug_vn)}}"><img  src="{{asset($product_related->product_thumbnail)}}" alt=""></a>
    </div><!-- /.image -->
                @if ($product_related->discount_price != Null)
                @php
                    $discount = ($product_related->selling_price - $product_related->discount_price)/$product_related->selling_price*100
                @endphp
                <div class="tag sale"><span>{{round($discount)}}%</span></div>
                @else
                    <div class="tag new"><span>new</span></div>
                @endif
</div><!-- /.product-image -->


<div class="product-info text-left">
    <h3 class="name"><a href="{{url('product/detail/'.$product_related->id.'/'.$product_related->product_slug_vn)}}">{{$product_related->product_name_vn}}</a></h3>
    <div class="rating rateit-small"></div>
    <div class="description"></div>

    <div class="product-price">
        @if ($product_related->discount_price != Null)
        <span class="price"> {{number_format($product_related->selling_price,0,'','.')}} Đ</span>
        <span class="price-before-discount">{{number_format($product_related->discount_price,0,'','.')}} Đ</span>
    @else
    <span class="price"> {{number_format($product_related->selling_price,0,'','.')}} Đ</span>
    @endif

    </div><!-- /.product-price -->

</div><!-- /.product-info -->
            <div class="cart clearfix animate-effect">
        <div class="action">
            <ul class="list-unstyled">
                <li class="add-cart-button btn-group">
                    <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                        <i class="fa fa-shopping-cart"></i>
                    </button>
                    <button class="btn btn-primary cart-btn" type="button">Add to cart</button>

                </li>

                <li class="lnk wishlist">
                    <a class="add-to-cart" href="detail.html" title="Wishlist">
                         <i class="icon fa fa-heart"></i>
                    </a>
                </li>

                <li class="lnk">
                    <a class="add-to-cart" href="detail.html" title="Compare">
                        <i class="fa fa-signal"></i>
                    </a>
                </li>
            </ul>
        </div><!-- /.action -->
    </div><!-- /.cart -->
    </div><!-- /.product -->

    </div><!-- /.products -->
</div><!-- /.item -->
@endforeach

			</div><!-- /.home-owl-carousel -->
</section><!-- /.section -->
<!-- ============================================== UPSELL PRODUCTS : END ============================================== -->

			</div><!-- /.col -->
			<div class="clearfix"></div>
		</div><!-- /.row -->

























		<!-- ==== ================== BRANDS CAROUSEL ============================================== -->
@include('frontend.body.brands')
<!-- == = BRANDS CAROUSEL : END = -->	</div><!-- /.container -->
</div><!-- /.body-content -->
<!-- Go to www.addthis.com/dashboard to customize your tools -->


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" type="text/javascript"></script>

<script>
    $(document).ready(function() {
       $('#loadmore').click(function(e) {
           e.preventDefault();
         var count_reviews = $('#count_review').val();
         var product_id = $('#product_id').val();
         var offset_review = $('#offset_review').val(); // danh mục hiện tại của bản ghi
         var offset_get = offset_review - 3; // danh mục để lấy bản ghi
         var _token = $('input[name="_token"]').val();
        if(offset_get >= 0 && count_reviews >6) {
            $("#loader").attr('style', 'display:inline-block');
			console.log(0);
            $.ajax({
                type: "GET",
                url: "{{url('load-comment')}}",
                data: {count_reviews:count_reviews,product_id:product_id, offset_get:offset_get, _token:_token },

                success: function (response) {
                    $(".reviews:last").after(response).show().fadeIn("slow");

                    $('#offset_review').val(offset_get);
                    $("#loader").attr('style', 'display:none');
                    if(offset_get <3) {
                        $("#loadmore").text("Thu gọn");
                    }

                }
            });
       }
       else if(offset_get > -3 && offset_get <0 &&  count_reviews <6) {
		   console(1);
        $("#loader").attr('style', 'display:inline-block');
        $.ajax({
                type: "GET",
                url: "{{url('load-comment')}}",
                data: {count_reviews:count_reviews,product_id:product_id, offset_get:offset_get, _token:_token },
                success: function (response) {
                    $(".reviews:last").after(response).show().fadeIn("slow");

                    $('#offset_review').val(offset_get);
                    $("#loader").attr('style', 'display:none');
                    if(offset_get <3) {
                        $("#loadmore").text("Thu gọn");
                    }

                }
            });
       }

       else {
        $("#loader").attr('style', 'display:none');

        $('.reviews:nth-child(4)').nextAll('.reviews').remove();
        $('#offset_review').val(count_reviews-3);
        $("#loadmore").text("Xem thêm");
       }


       })
    })
</script>
<script>
    function loadRating() {
        var _token = $('input[name="_token"]').val();
        var product_id = $('#product_id').val();
        $.ajax({
            type: "GET",
            url: "{{ url('load-rating') }}",
            data: {_token:_token,product_id:product_id},
            success: function (response) {
                $('#rating').html(response);
            }
        });
    }
    loadRating();
	function remove_background(product_id) {
        for($count = 0; $count<=5; $count++) {
            $('#'+product_id+'-'+$count + ' i').css('color', '#333');
        }
    }
    $(document).on('mouseenter', '.ratings',function() {
          index = $(this).data('index');
          product_id = $(this).data('product_id');

            remove_background(product_id);
            for($count = 0; $count<=index; $count++) {
                $('#'+product_id+'-'+$count + ' i').css('color', '#ffc107');
            }
        });
        // nhả chuột
        $(document).on('mouseleave', '.ratings', function() {
          index = $(this).data('index');
          product_id = $(this).data('product_id');
          rating = $(this).data('rating');

          remove_background(product_id);
          for($count = 0; $count<=rating; $count++) {
                $('#'+product_id+'-'+$count + ' i').css('color', '#ffc107');
            }
        });
        $(document).on('click', '.ratings', function() {
          index = $(this).data('index');
          product_id = $(this).data('product_id');
          _token = $('input[name="_token"]').val();
         $.ajax({
                    url: "{{url('rating-star')}}",
                    method: 'POST',
                    data:{index:index,product_id:product_id,_token:_token},
                    success:function(data){
                        if(data == 1) {
                            alert("Bạn đã đánh giá thành công "+index+" trên 5 sao");
                        }
                        loadRating();
                    }
            });
          remove_background(product_id);
          for($count = 0; $count<=rating; $count++) {
                $('#'+product_id+'-'+$count).css('color', '#ffc107');
            }
        });
</script>
@endsection
