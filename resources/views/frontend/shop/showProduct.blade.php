@extends('frontend.main_layout')

@section('title', 'Sản phẩm')
@section('content')

<div class="breadcrumb">
    <div class="container">
      <div class="breadcrumb-inner">
        <ul class="list-inline list-unstyled">
          <li><a href="{{url('/')}}">Trang chủ</a></li>
          <li class='active'><a href="">Sản phẩm</a></li>
        </ul>
      </div>
      <!-- /.breadcrumb-inner -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.breadcrumb -->
  <div class="body-content outer-top-xs">
    <div class='container'>
      <div class='row'>
        <div class='col-md-3 sidebar'>
          <!-- ================================== TOP NAVIGATION ================================== -->
          @include('frontend.common.vertical_menu')
          <!-- /.side-menu -->
          <!-- ================================== TOP NAVIGATION : END ================================== -->
          <div class="sidebar-module-container">
            <div class="sidebar-filter">
              <!-- ============================================== SIDEBAR CATEGORY ============================================== -->
              <div class="sidebar-widget wow fadeInUp">
                <h3 class="section-title">Lọc theo</h3>
                <div class="widget-header">
                  <h4 class="widget-title">Danh mục</h4>
                </div>
                <div class="sidebar-widget-body">
                  <div class="accordion">
                      <form action="{{url('shop/by-category')}}" id="formSorbCategoryProduct" method="post">

                        @csrf
                        @foreach ($categories as $category)
                            <label class="container_checkbox">{{$category->category_name_vn}}
                                <input type="checkbox" name="category" class="categoryProduct"  value="{{$category->id}}" >
                                <span class="checkmark"></span>
                            </label>
                        <!-- /.accordion-group -->
                          @endforeach
                    </form>
                  </div>
                  <!-- /.accordion -->
                </div>
                <!-- /.sidebar-widget-body -->
              </div>

            @include('frontend.common.product_tags')
              <!-- /.sidebar-widget -->
            <!----------- Testimonials------------->


              <!-- ============================================== Testimonials: END ============================================== -->

              <div class="home-banner"> <img src="{{asset('frontend/assets/images/banners/LHS-banner.jpg')}}" alt="Image"> </div>
            </div>
            <!-- /.sidebar-filter -->
          </div>
          <!-- /.sidebar-module-container -->
        </div>
        <!-- /.sidebar -->
        <div class='col-md-9'>
          <!-- ========================================== SECTION – HERO ========================================= -->




          <div class="clearfix filters-container">
            <div class="row">
              <div class="col col-sm-6 col-md-2">
                <div class="filter-tabs">
                  <ul id="filter-tabs" class="nav nav-tabs nav-tab-box nav-tab-fa-icon">
                    <li class="active"> <a data-toggle="tab" href="#grid-container"><i class="icon fa fa-th-large"></i>Grid</a> </li>
                    <li><a data-toggle="tab" href="#list-container"><i class="icon fa fa-th-list"></i>List</a></li>
                  </ul>
                </div>
                <!-- /.filter-tabs -->
              </div>
              <!-- /.col -->
              <div class="col col-sm-12 col-md-6">
                <div class="col col-sm-3 col-md-6 no-padding">
                  <div class="lbl-cnt">
                    <div class="fld inline">
                        <div class="form-group">
                          <select class="form-control form-control-sm" name="sort_by" id="sort_by">
                            <option value="">--- Lọc sản phẩm ---</option>
                            <option value="a-z">Từ A-Z</option>
                            <option value="z-a">Từ Z-A</option>
                            <option value="high-low">Giá từ cao đến thấp</option>
                            <option value="low-high">Giá từ thấp đến cao</option>
                        </select>
                        </div>
                    </div>
                    <!-- /.fld -->
                  </div>
                  <!-- /.lbl-cnt -->
                </div>
                <!-- /.col -->
                <div class="col col-sm-3 col-md-6 no-padding">
                  <div class="lbl-cnt"> <span class="lbl">Show</span>
                    <div class="fld inline">
                        <div class="form-group">
                          <select class="form-control form-control-sm" name="showProduct" id="showProduct">
                            <option value="3">3</option>
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="15">15</option>
                            <option  value="20">20</option>
                        </select>
                        </div>
                    </div>
                    <!-- /.fld -->
                  </div>
                  <!-- /.lbl-cnt -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.col -->




              {{-- Phân trang --}}
              <div class="col col-sm-6 col-md-4 text-right">
                <div class="pagination-container">
                    <div class="form-group">
                      <select class="form-control form-control-sm" name="sort-price" id="sort-price">
                        <option value="">--- Lọc giá ---</option>
                        <option  value="<500000">Dưới 500.000₫</option>
                        <option value="500000-2000000">500.000đ - 2.000.000đ</option>
                        <option    value="2000000-5000000">2.000.000₫ - 5.000.000₫</option>
                        <option value=">5000000">Trên 5.000.000đ</option>
                    </select>
                    </div>
                </div>
               </div>
              <!-- /.col -->





            </div>
            <!-- /.row -->



          </div>
          <div class="search-result-container ">
            <div id="myTabContent" class="tab-content category-list">
              <div class="tab-pane active " id="grid-container">
                <div class="category-product">
                  <div class="row" id="grid_product">

                    @include('frontend.shop.gridProduct')

                  </div>
                  <!-- /.row -->
                </div>
                <!-- /.category-product -->

              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane "  id="list-container">
                <div class="category-product" id="list_product">

                    @include('frontend.shop.listProduct')


                </div>
                <!-- /.category-product -->
              </div>
              <!-- /.tab-pane #list-container -->
            </div>
            <!-- /.tab-content -->
            <div class="clearfix filters-container">
              <div class="text-right" id="paginate_product">

                @include('frontend.shop.paginateProduct')
              </div>


            </div>
            <!-- /.filters-container -->
            <div id="loading_product">
                <img src="{{asset('frontend/assets/images/loading2.svg')}}" width="50" height="50" alt="">
            </div>
          </div>
          <!-- /.search-result-container -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <!-- ============================================== BRANDS CAROUSEL ============================================== -->
     @include('frontend.body.brands')
      <!-- /.logo-slider -->
      <!-- ============================================== BRANDS CAROUSEL : END ============================================== --> </div>
    <!-- /.container -->

  </div>
    </div>
  <!-- /.body-content -->

  <script src="{{asset('frontend/assets/js/jquery-1.11.1.min.js')}}"></script>
  <script>

    function filter_product(page=1) {
        let category_product = filter('categoryProduct');
        let sort_price = $('#sort-price').val(); // lấy giá trị sort_price khi change
        let show = $('#showProduct').val();
        let sort_by = $('#sort_by').val();

        $.ajax({
            type: "GET",
            url: "{{ url('shop') }}",
            data: {sort_price:sort_price, show:show, sort_by:sort_by,category_product:category_product, page:page},
            dataType: "JSON",
            beforeSend:function() {
                $('#loading_product').css('display','block');
            },
            success: function (response) {
                $('#paginate_product').html(response.product_paginate);
                $('#grid_product').html(response.products_grid);
                $('#list_product').html(response.products_list);
            },
            complete: function() {
                $('#loading_product').css('display','none');
            }
        });
    }

    function filter(classname) {
        let filter = [];
        $('.'+classname+':checked').each(function(index, input) {
            filter.push(input.value);
        });
        return filter;
    }

    $('#sort-price').change(function() {
        filter_product();
    });
    $('#showProduct').change(function() {
        filter_product();
    });
    $('#sort_by').change(function() {
        filter_product();
    });
    $('.categoryProduct').change(function() {
        filter_product();
    });
    $(document).on('click', '.pager li a', function(e) {
        e.preventDefault();
        let href = $(this).attr('href');
        let page = href.split('page=')[1];
        filter_product(page);
    })


  </script>
@endsection
