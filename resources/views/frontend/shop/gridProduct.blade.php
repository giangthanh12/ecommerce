@forelse ($products as $product)
<div class="col-sm-6 col-md-4 wow fadeInUp">
  <div class="products">
    <div class="product">
      <div class="product-image">
        <div class="image"> <a href="{{url('product/detail/'.$product->id.'/'.$product->product_slug_vn)}}"><img  src="{{asset($product->product_thumbnail)}}" alt=""></a> </div>
        <!-- /.image -->

        @if ($product->discount_price != Null)
        @php
            $discount = ($product->selling_price - $product->discount_price)/$product->selling_price*100
        @endphp
        <div class="tag hot"><span>{{round($discount)}}%</span></div>
        @else
            <div class="tag new"><span>new</span></div>
        @endif
      </div>
      <!-- /.product-image -->

      <div class="product-info text-left">
        <h3 class="name"><a href="detail.html">{{$product->product_name_vn}}</a></h3>
        <div class="rating rateit-small"></div>
        <div class="description"></div>
        <div class="product-price">
            @if ($product->discount_price != Null)
            <span class="price">{{number_format($product->discount_price,0,'','.')}} Đ</span>
            <span class="price-before-discount"> {{number_format($product->selling_price,0,'','.')}} Đ</span>
        @else
        <span class="price"> {{number_format($product->selling_price,0,'','.')}} Đ</span>
        @endif
        </div>
        <!-- /.product-price -->
      </div>
      <!-- /.product-info -->
      <div class="cart clearfix animate-effect">
        <div class="action">
          <ul class="list-unstyled">
            <li class="add-cart-button btn-group">
                <button  onclick="viewModalProduct(this.id)" id="{{$product->id}}" class="btn btn-primary icon btnadd" data-toggle="modal" data-target="#exampleModal"  type="button" title="Add Cart"> <i class="fa fa-shopping-cart"></i> </button>
              <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
            </li>
            <li class="lnk wishlist"> <a data-toggle="tooltip" data-id="{{$product->id}}" class="add-to-cart addWishlist" href="" title="Yêu thích"> <i class="icon fa fa-heart"></i> </a> </li>

          </ul>
        </div>
        <!-- /.action -->
      </div>
      <!-- /.cart -->
    </div>
    <!-- /.product -->

  </div>
  <!-- /.products -->
</div>
<!-- /.item -->

@empty

<div class="alert alert-warning" role="alert">
    Không có sản phẩm!
  </div>
@endforelse
