@foreach ($products as $product)
                    <div class="category-product-inner wow fadeInUp">
                    <div class="products">
                      <div class="product-list product">
                        <div class="row product-list-row">
                          <div class="col col-sm-4 col-lg-4">
                            <div class="product-image">
                              <div class="image"><a href="{{url('product/detail/'.$product->id.'/'.$product->product_slug_vn)}}"><img src="{{asset($product->product_thumbnail)}}" alt=""></a>  </div>
                            </div>
                            <!-- /.product-image -->
                          </div>
                          <!-- /.col -->
                          <div class="col col-sm-8 col-lg-8">
                            <div class="product-info">
                              <h3 class="name"><a href="{{url('product/detail/'.$product->id.'/'.$product->product_slug_vn)}}">{{$product->product_name_vn}}</a></h3>
                              <div class="rating rateit-small"></div>
                              <div class="product-price">
                                @if ($product->discount_price != Null)
                                <span class="price">{{number_format($product->discount_price,0,'','.')}} Đ </span>
                                <span class="price-before-discount">{{number_format($product->selling_price,0,'','.')}} Đ</span>
                            @else
                            <span class="price"> {{number_format($product->selling_price,0,'','.')}} Đ</span>
                            @endif
                                </div>
                              <!-- /.product-price -->
                              <div class="description m-t-10">{!! $product->short_descp_vn !!}</div>
                              <div class="cart clearfix animate-effect">
                                <div class="action">
                                  <ul class="list-unstyled">
                                    <li class="add-cart-button btn-group">
                                        <button  onclick="viewModalProduct(this.id)" id="{{$product->id}}" class="btn btn-primary icon btnadd" data-toggle="modal" data-target="#exampleModal"  type="button" title="Add Cart"> <i class="fa fa-shopping-cart"></i> </button>

                                    </li>
                                    <li class="lnk wishlist"> <a data-toggle="tooltip" data-id="{{$product->id}}" class="add-to-cart addWishlist" href="" title="Yêu thích"> <i class="icon fa fa-heart"></i> </a> </li>

                                  </ul>
                                </div>
                                <!-- /.action -->
                              </div>
                              <!-- /.cart -->

                            </div>
                            <!-- /.product-info -->
                          </div>
                          <!-- /.col -->
                        </div>
                        <!-- /.product-list-row -->

                            @if ($product->discount_price != Null)
                            @php
                                $discount = ($product->selling_price - $product->discount_price)/$product->selling_price*100
                            @endphp
                            <div class="tag hot"><span>{{round($discount)}}%</span></div>
                            @else
                                <div class="tag new"><span>new</span></div>
                            @endif
                      </div>
                      <!-- /.product-list -->
                    </div>
                    <!-- /.products -->
                  </div>
                  <!-- /.category-product-inner -->
                    @endforeach
