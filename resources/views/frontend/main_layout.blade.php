<!DOCTYPE html>
<html lang="en">
<head>
<!-- Meta -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<meta name="keywords" content="MediaCenter, Template, eCommerce">
<meta name="robots" content="all">
{{-- <title>@yield('title')</title> --}}
<title>VERAF CA</title>

<!-- Bootstrap Core CSS -->
<link rel="stylesheet" href="{{asset('frontend/assets/css/bootstrap.min.css')}}">

<!-- Customizable CSS -->

<link rel="stylesheet" href="{{asset('frontend/assets/css/main.css')}}">
<link rel="stylesheet" href="{{asset('frontend/assets/css/blue.css')}}">
<link rel="stylesheet" href="{{asset('frontend/assets/css/owl.carousel.css')}}">
<link rel="stylesheet" href="{{asset('frontend/assets/css/owl.transitions.css')}}">
<link rel="stylesheet" href="{{asset('frontend/assets/css/animate.min.css')}}">
<link rel="stylesheet" href="{{asset('frontend/assets/css/rateit.css')}}">
<link rel="stylesheet" href="{{asset('frontend/assets/css/bootstrap-select.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
<link rel="stylesheet" href="{{asset('frontend/assets/css/alertify.min.css')}}" />
<!-- include a theme -->
<link rel="stylesheet" href="{{asset('frontend/assets/css/themes/default.min.css')}}" />

<!-- Icons/Glyphs -->
<link rel="stylesheet" href="{{asset('frontend/assets/css/font-awesome.css')}}">
<!-- validate -->
<link rel="stylesheet" href="{{asset('frontend/assets/css/validate.css')}}">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="{{asset('frontend/assets/js/jquery.validate.min.js')}}"></script>

<!-- Fonts -->
<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,600italic,700,700italic,800' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
</head>
<body class="cnt-home">
    <!-- Messenger Plugin chat Code -->
 <!-- Messenger Plugin chat Code -->
 <div id="fb-root"></div>

<!-- Your Plugin chat code -->
<div id="fb-customer-chat" class="fb-customerchat">
</div>

<script>
  var chatbox = document.getElementById('fb-customer-chat');
  chatbox.setAttribute("page_id", "102130572145401");
  chatbox.setAttribute("attribution", "biz_inbox");
</script>

<!-- Your SDK code -->
<script>
  window.fbAsyncInit = function() {
    FB.init({
      xfbml            : true,
      version          : 'v12.0'
    });
  };

  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>
<!-- ============================================== HEADER ============================================== -->
@include('frontend.body.header')

<!-- ============================================== HEADER : END ============================================== -->
@yield('content')
<!-- /#top-banner-and-menu -->

<!-- ============================================================= FOOTER ============================================================= -->
@include('frontend.body.footer')
<!-- ============================================================= FOOTER : END============================================================= -->

<!-- For demo purposes – can be removed on production -->

<!-- For demo purposes – can be removed on production : End -->

<!-- JavaScripts placed at the end of the document so the pages load faster -->

{{-- <script src="{{asset('frontend/assets/js/jquery-1.11.1.min.js')}}"></script> --}}
<script src="{{asset('frontend/assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('frontend/assets/js/bootstrap-hover-dropdown.min.js')}}"></script>
<script src="{{asset('frontend/assets/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('frontend/assets/js/echo.min.js')}}"></script>
<script src="{{asset('frontend/assets/js/jquery.easing-1.3.min.js')}}"></script>
<script src="{{asset('frontend/assets/js/bootstrap-slider.min.js')}}"></script>
<script src="{{asset('frontend/assets/js/jquery.rateit.min.js')}}"></script>
<script type="text/javascript" src="{{asset('frontend/assets/js/lightbox.min.js')}}"></script>
<script src="{{asset('frontend/assets/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('frontend/assets/js/wow.min.js')}}"></script>
<script src="{{asset('frontend/assets/js/scripts.js')}}"></script>
<script src="{{asset('frontend/assets/js/alertify.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>


<script>
    @if (Session::has('message'))
    var message = "{{Session::get('message')}}";
    var type = "{{Session::get('type')}}";
    console.log(message);
    switch(type) {
        case 'success':
        toastr.success(message);
        break;
        case 'info':
        toastr.info(message);
        break;
        case 'error':
        toastr.error(message);
        break;
        case 'warning':
        toastr.warning(message);
        break;
    }
    @endif
</script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script type="text/javascript">
function viewModalProduct(id) {

    $.ajax({
        type: "GET",
        url: " {{url('product/modal') }}"+'/'+id,
        dataType: "JSON",
        success: function (data) {
            $('#product_id').val(data.product.id);
            $('#product_modal_title').html(`<b>${data.product.product_name_vn}</b>`);
            $('#product_modal_image').attr('src', `{{ asset('${data.product.product_thumbnail}') }}`);

            if (data.product.discount_price == null) {
                var selling_price = new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(data.product.selling_price);
                console.log(selling_price);
                $('#product_modal_price').html(`<b>${selling_price}</b>`);
                $('#product_price').val(data.product.selling_price);

            } else {
                $('#product_price').val(data.product.discount_price);
                var discount_price = new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(data.product.discount_price);
                $('#product_modal_price').html(`<b>${discount_price}</b>`);
            }

            $('#product_modal_code').html(`<b>${data.product.product_code}</b>`);
            $('#product_modal_brand').html(`<b>${data.product.brand.brand_name_vn}</b>`);
            $('#product_modal_category').html(`<b>${data.product.category.category_name_vn}</b>`);
            if(data.product.product_qty > 0) {
                $('#product_modal_qty').text('Còn hàng');
                $('#product_modal_qty').css("background-color", "#429227");

            }
            else {
                $('#product_modal_qty').html('Hết hàng');
                $('#product_modal_qty').css("background-color", "#ffc107");
            }

            // check product_color_vn  to show screen

            $('#product_modal_color').empty();
            if (data.product.product_color_vn == null) {
                $('#group-color').hide();
            } else {
                $('#group-color').show();
                $('#product_modal_color').append(`<option value="notValue" disabled selected>--Lựa chọn màu--</option>`);
                $.each(data.product_colors, function (index,value) {
                    $('#product_modal_color').append(`<option value="${value}">${value}</option>`);
                });
            }


            // check product_size_vn  to show screen
                 $('#product_modal_size').empty();
            if(data.product.product_size_vn == null) {
                $('#group-size').hide();
            }
            else {
                $('#group-size').show();
                $('#product_modal_size').append(`<option value="notValue" disabled selected>--Lựa chọn kích cỡ--</option>`);
                $.each(data.product_sizes, function (index,value) {
                $('#product_modal_size').append(`<option value="${value}">${value}</option>`);
            });
            }


        }
    });
}
</script>
<script type="text/javascript">
function addToCart() {
  var id = $('#product_id').val();
  var product_name = $('#product_modal_title').text();
  var product_qty = $('#product_qty').val();
  var product_price = $('#product_price').val();
  var product_color = $('#product_modal_color option:selected').val();
  var product_size = $('#product_modal_size option:selected').val();
    if(product_color == 'notValue') {
        const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 2000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
        })

        Toast.fire({
        icon: 'error',
        title: 'Vui lòng chọn màu sản phẩm.'

        })
        return;
    }
    if(product_size == 'notValue') {
        const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 2000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
        })
        Toast.fire({
        icon: 'error',
        title: 'Vui lòng chọn kích cỡ sản phẩm'

        })

    return;
    }

$.ajax({
    type: "POST",
    url: `{{url('cart/add/${id}')}}`,
    data: {product_name:product_name, product_qty:product_qty, product_price:product_price, product_color:product_color, product_size:product_size},
    dataType: "JSON",
    success: function (data) {
        getMiniCart();
        const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 2000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
        })

        Toast.fire({
        icon: 'success',
        title: data.success

        })
        $('#exampleModal').modal('hide');
    }
});

}

</script>
<script type="text/javascript">

function getMiniCart() {
    $.ajax({
        type: "POST",
        url: "{{ url('/get/data/cart') }}",
        dataType: "JSON",
        success: function (data) {
            var total = new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(data.total);
                $('.totalPriceCartMini').text(total);
                $('#countCartMini').text(data.count);
                $('#cartMini').empty();
                $dataMiniCart = '';
            $.each(data.dataCart, function(index, value) {
                var price = new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(value.price);
                $dataMiniCart += `<div class="cart-item product-summary">
                    <div class="row">
                    <div class="col-xs-4">
                        <div class="image"> <a href=""><img src=" {{ asset('${value.options.image}') }} " alt=""></a> </div>
                    </div>
                    <div class="col-xs-7">
                        <h3 class="name"><a href="index.php?page-detail">${value.name}</a>x${value.qty}</h3>
                        <div class="price">${price}</div>
                    </div>
                    <div class="col-xs-1 action"> <a href="#javascript" onclick="DeleteCart('${value.rowId}')"><i class="fa fa-trash"></i></a> </div>
                    </div>
                </div>

                <!-- /.cart-item -->
                <div class="clearfix"></div>
                <hr>`;
            })
            $('#cartMini').html($dataMiniCart);
            console.log($dataMiniCart);
        }
    });
}

function DeleteCart(rowId) {

    $.ajax({
        type: "POST",
        url: `{{ url('/cart/delete/data/${rowId}') }}`,
        dataType: "JSON",
        success: function (data) {
            caculationCart();
            getListCart();
            $('#showTableCoupon').show();
            getMiniCart();
            const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 2000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
        })

        Toast.fire({
        icon: 'success',
        title: data.success
        })
        }
    });
}
getMiniCart();
</script>

{{-- wishlist --}}
<script type="text/javascript">
   $(document).on('click', '.addWishlist', function(event) {
    event.preventDefault();
    var id = $(this).data('id');
  $.ajax({
      type: "POST",
      url: `{{url('addWishlist/${id}')}}`,
      dataType: "JSON",
      success: function (data) {
        const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 2000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
        });
        if(!data.error) {
            Toast.fire({
            icon: 'success',
            title: data.success
            });
        }
        else {
            Toast.fire({
            icon: 'error',
            title: data.error
            });
        }

      }
  });
   })
</script>




<script type="text/javascript">
function getWithlist() {
    $(".lds-ripple").css("display", 'inline-block');
    $.ajax({
        type: "GET",
        url: "{{ url('get-withlist') }}",
        dataType: "JSON",
        success: function (data) {
            $(".lds-ripple").css("display", 'none');
                $rows = '';
            $.each(data.withlist, function(index, value) {
                // var price = new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(value.price);
                if(value.product.discount_price == null) {
                var price =  new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(value.product.selling_price);
                }
                else {
                    var selling_price = new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(value.product.selling_price);
                    var discount_price = new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(value.product.discount_price);
                var price = ` ${discount_price}
							<span>${selling_price}</span> `
                }
                $rows += `<tr>
					<td class="col-md-2"><img src="{{ asset('${value.product.product_thumbnail}')}}" alt="imga"></td>
					<td class="col-md-7">
						<div class="product-name"><a href="#">${value.product.product_name_vn}</a></div>
						<div class="rating">
							<i class="fa fa-star rate"></i>
							<i class="fa fa-star rate"></i>
							<i class="fa fa-star rate"></i>
							<i class="fa fa-star rate"></i>
							<i class="fa fa-star non-rate"></i>
							<span class="review">( 06 Reviews )</span>
						</div>
						<div class="price">
                            ${price}
						</div>
					</td>
					<td class="col-md-2">
						<button type="submit" data-toggle="modal" data-target="#exampleModal" onclick="viewModalProduct(${value.product.id})" id="${value.product.id}" class="btn-upper btn btn-primary">Add to cart</button>
					</td>
					<td class="col-md-1 close-btn">
						<a href="#" href="#javascript" onclick="DeleteWithlist(${value.product.id})" class=""><i class="fa fa-times"></i></a>
					</td>
				</tr>`;
            });
            $('#withlist').html($rows);
        }
    });
}
function DeleteWithlist(id) {
    $.ajax({
        type: "POST",
        url: ` {{ url('delete-withlist/${id}') }}`,
        data: "data",
        dataType: "JSON",
        success: function (data) {
            getWithlist();
            const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
            })

            Toast.fire({
            icon: 'success',
            title: data.success
            })
        }
    });
}
getWithlist()
</script>
{{-- List cart --}}
<script type="text/javascript">
function getListCart() {
    $(".lds-ripple1").css("display", 'inline-block');
    $.ajax({
        type: "GET",
        url: "{{ url('get-listCart') }}",
        dataType: "JSON",
        success: function (data) {
            $(".lds-ripple1").css("display", 'none');
                $rows = '';
            $.each(data.listcart, function(index, value) {
                var price = new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(value.price);
                var subtotal = new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(value.subtotal);

                $rows += `<tr>
                            <td class="romove-item"><a href="#javascript" onclick="DeleteProductCart('${value.rowId}')" title="cancel" class="icon"><i class="fa fa-trash-o"></i></a></td>
                            <td class="cart-image">
                                <a class="entry-thumbnail" href="detail.html">
                                    <img style="width:60px; height:60px;" src="{{ asset('${value.options.image}') }}" alt="">
                                </a>
                            </td>
                            <td class="cart-product-name-info">
                                <h4 class='cart-product-description'><a href="detail.html">${value.name}</a></h4>
                            </td>
                            <td class="cart-product-color">
                            ${value.options.color == null ? `---` : `${value.options.color}`}
                            </td>
                            <td class="cart-product-size">
                                ${value.options.size == null ? `---` : `${value.options.size}`}
                            </td>
                            <td class="cart-product-price">${price}</td>
                            <td class="cart-product-quantity">
                                <div class="quant-input">
                                    <div class="arrows">
                                        <div class="arrow plus gradient"><span onclick="IncreaseCart('${value.rowId}')" class="ir"><i class="icon fa fa-sort-asc"></i></span></div>
                                        <div class="arrow minus gradient"><span onclick="DecreaseCart('${value.rowId}')" class="ir"><i class="icon fa fa-sort-desc"></i></span></div>
                                    </div>
                                    <input type="text" value="${value.qty}">
                                </div>
                            </td>
                            <td class="cart-product-sub-total"><span class="cart-sub-total-price">${subtotal}</span></td>
                        </tr>`;
            });
            $('#showListCart').html($rows);
        }
    });
}
getListCart()

// Increase cart ajax
function IncreaseCart(rowId) {
    $.ajax({
        type: "GET",
        url: `{{ url('increase-qty/${rowId}') }}`,
        dataType: "JSON",
        success: function (response) {
            caculationCart();
            getMiniCart();
            getListCart();
        }
    });
}
function DecreaseCart(rowId) {
    $.ajax({
        type: "GET",
        url: `{{ url('decrease-qty/${rowId}') }}`,
        dataType: "JSON",
        success: function (response) {
            caculationCart() ;
            getMiniCart();
            getListCart();
        }
    });
}
function DeleteProductCart(rowId) {
    $.ajax({
        type: "GET",
        url: `{{ url('delete-product-cart/${rowId}') }}`,
        dataType: "JSON",
        success: function (response) {
            caculationCart();
            $('#showTableCoupon').show();
            getMiniCart();
            getListCart();
        }
    });
}
</script>


{{-- ajax coupon --}}
<script type="text/javascript">
function applyCoupon() {
    var coupon_name = $('#coupon_name_vn').val();
    $.ajax({
        type: "POST",
        url: "{{ url('apply-coupon')}}",
        data: {coupon_name:coupon_name},
        dataType: "JSON",
        success: function (data) {
            $('#coupon_name_vn').val('');
            const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
            })
            if($.isEmptyObject(data.error)) {
                $('#showTableCoupon').hide();
                caculationCart();
                Toast.fire({
                icon: 'success',
                title: data.success
            })
            }

            else {
                Toast.fire({
                icon: 'error',
                title: data.error
            })
            }
        }
    });
}


function caculationCart() {
    $.ajax({
        type: "POST",
        url: "{{ url('caculation-cart') }}",
        data: "data",
        dataType: "JSON",
        success: function (data) {
            if(data.total) {
                $('#showTableCoupon').show();
                var total = new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(data.total);
                $('#CaculationCart').html(`<tr>
				<th>
					<div class="cart-sub-total">
						<span>Tạm tính:</span><span class="">${total}</span>
					</div>
					<div class="cart-grand-total">
						<span>Tổng tiên:</span><span class="">${total}</span>
					</div>
				</th>
			</tr>`);
            }
            else {
                $('#showTableCoupon').hide();
                var subtotal = new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(data.subtotal);
                var ammount_discount = new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(data.ammount_discount);
                var grand_total = new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(data.grand_total);
                $('#CaculationCart').html(`<tr>
				<th>
					<div class="cart-sub-total">
					<span>Tạm tính:</span><span class="">${subtotal} </span>
					</div>
					<div class="cart-coupon-name ">
						<span>Tên mã:</span><span class="">${data.coupon_name}</span>
                        <button type="submit" onclick="removeCoupon()" class="btn btn-danger"> <i class="fa fa-times"></i></button>
					</div>
                    <div class="cart-ammount-discount">
						<span>Số tiền giảm:</span><span class="">${ammount_discount} </span>
					</div>
                    <div class="cart-grand-total">
						<span>Tổng tiên:</span><span class="">${grand_total} </span>
					</div>
				</th>
			</tr>`);
            }
        }
    });
}
function removeCoupon() {
    $.ajax({
        type: "POST",
        url: "{{ url('remove-coupon') }}",
        dataType: "JSON",
        success: function (data) {
            $('#showTableCoupon').show();
            caculationCart();
            const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
            });

            Toast.fire({
            icon: 'success',
            title: data.success
            });
        }
    });
}
caculationCart()
</script>

{{-- and ajax coupon --}}

<script type="text/javascript">
 $(document).on('change', '.choose', function() {
          var elementId = $(this).attr('id');
          var value = $(this).val();

          if(elementId === 'matp') {

            $.ajax({
                    url: `{{ url('get-district/${value}') }}`,
                    type:'GET',
                    dataType: 'JSON', // dịnh dạng kiểu dữ liệu nhận về là json
                    success: function(data) {
                        // var data = JSON.parse(data) ;
                        // vì data nhận về là kiểu json trên có dataType = json nên jquery đã hiểu và phân tích dữ liệu để sử dụng
                        // như bình thường nếu không có dataType thì dữ liệu chỉ hiểu là chuỗi json mà jquery ko hiểu thực chất là một object
                        //nên ta phải dùng Json.parse để chuyển dữ liệu về type javascript
                       $('#maqh').empty();
                       $('#maxa').empty();
                       $('#maxa').append(`<option  selected='' disabled>--- Lựa chọn ---</option>`);
                       $('#maqh').append(`<option  selected='' disabled>--- Lựa chọn ---</option>`);
                       $.each(data, function (index, value) {
                        $('#maqh').append(`<option value="${value.id}">${value.name}</option>`);
                        });
                    }
                });
          }
          else if(elementId === 'maqh') {
            $.ajax({
                    url:  `{{ url('get-wards/${value}') }}`,
                    type:'GET',
                    dataType: 'json', // dịnh dạng kiểu dữ liệu nhận về là json
                    success: function(data) {
                        // var data = JSON.parse(data) ;
                        // vì data nhận về là kiểu json trên có dataType = json nên jquery đã hiểu và phân tích dữ liệu để sử dụng
                        // như bình thường nếu không có dataType thì dữ liệu chỉ hiểu là chuỗi json mà jquery ko hiểu thực chất là một object
                        //nên ta phải dùng Json.parse để chuyển dữ liệu về type javascript
                       $('#maxa').empty();
                       $('#maxa').append(`<option  selected='' disabled>--- Lựa chọn ---</option>`);
                       $.each(data, function (index, value) {
                        $('#maxa').append(`<option value="${value.id}">${value.name}</option>`);
                        });
                    }
                });
          }
        });
</script>
<script>
    $('#product_search').blur(function(event) {
        event.preventDefault();
        $('.show-product').slideUp(1200);
        $('#product_search').val('');
    })

  $('#product_search').keyup(function(e) {

      e.preventDefault();
      var value = $('#product_search').val();
    if(value == '') {
        $('.show-product').html('');
    }

    else {
        $.ajax({
            type: "GET",
            url: "{{url('show-product-search')}}",
            data: {value:value},
            dataType: "JSON",
            success: function (response) {
                $('.show-product').html('');
                $('.show-product').slideDown();;
               $.each(response, function (i, v) {

                var price = new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(v.selling_price);
                $('.show-product').append(`<a href="{{ url('product/detail/${v.id}/${v.product_slug_vn}') }}"> <div class="row product">
                            <div class="col-sm-3">
                                <div class="product_img" style="text-align: center;">
                                    <img width="50" height="50" src="{{ asset('${v.product_thumbnail}') }} " alt="">
                                </div>
                            </div>
                            <div class="col-sm-9">
                         <b>${v.product_name_vn}</b>
                            <p>Giá: ${price}</p>
                            </div>
                        </div> </a>`);
                });
                console.log(response);
            }
        });
    }
  })
</script>



  <!-- Modal -->
  <div class="modal fade " id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="product_modal_title">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <img src="..." id="product_modal_image" class="card-img-top" width="200" height="180" alt="...">
                    </div>
                </div>
                <div class="col-md-4">
                    <ul class="list-group">
                        <li class="list-group-item">Giá: <span id="product_modal_price"></span></li>
                        <li class="list-group-item">Code: <span id="product_modal_code"></span></li>
                        <li class="list-group-item">Thương hiệu: <span id="product_modal_brand"></span></li>
                        <li class="list-group-item">Danh mục: <span id="product_modal_category"></span></li>
                        <li class="list-group-item">Trạng thái: <span id="product_modal_qty" class="badge badge-pill badge-warning"></span></li>
                      </ul>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="group-color">
                                <label for="product_modal_color">Chọn màu sản phẩm</label>
                                <select class="form-control" id="product_modal_color">

                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group" id="group-size">
                                <label for="product_modal_size">Chọn kích cỡ sản phẩm</label>
                                <select class="form-control" id="product_modal_size">

                                </select>
                              </div>
                         </div>
                         <div class="col-md-12">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="product_qty">Số lượng</label>
                                    <input type="number" min="1" value="1" class="form-control" id="product_qty" placeholder="">
                                </div>
                         </div>
                         <input type="hidden" id="product_price" value="">
                            <input type="hidden" id="product_id" value="">
                            <button type="submit" onclick="addToCart()" class="btn btn-primary">Thêm sản phẩm</button>

                    </div>

                </div>
            </div>

        </div>

      </div>
    </div>
  </div>
</body>
</html>
