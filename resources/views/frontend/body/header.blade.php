@php
    $site = App\Models\SiteSetting::find(1);
@endphp

<header class="header-style-1">

    <!-- ============================================== TOP MENU ============================================== -->
    <div class="top-bar animate-dropdown">
      <div class="container">
        <div class="header-top-inner">
          <div class="cnt-account">
            <ul class="list-unstyled">

              <li><a href="{{route('wishlist.all')}}"><i class="icon fa fa-heart"></i>Yêu thích</a></li>
              <li><a href="{{route('cart.show')}}"><i class="icon fa fa-shopping-cart"></i>Giỏ hàng</a></li>
              <li><a href="{{route('checkout')}}"><i class="icon fa fa-check"></i>Thanh toán</a></li>

              @auth
              <li><a href="#javascript" data-toggle="modal" data-target="#checkorder"><i class="icon fa fa-check"></i>Tra cứu đơn hàng</a></li>
               <li><a href="{{route('dashboard')}}"><i class="icon fa fa-user"></i>Tài khoản</a></li>
               <input type="hidden" name="user_id" id="user_id_client" value="{{auth()->user()->id}}">
              @else
              <li><a href="{{route('login')}}"><i class="icon fa fa-lock"></i>Đăng nhập</a><a href="{{route('register')}}">/Đăng ký</a></li>

              @endauth

            </ul>
          </div>

          <div class="clearfix"></div>
        </div>
        <!-- /.header-top-inner -->
      </div>
      <!-- /.container -->
    </div>
    <!-- /.header-top -->
    <!-- ============================================== TOP MENU : END ============================================== -->
    <div class="main-header">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-3 logo-holder">
            <!-- ============================================================= LOGO ============================================================= -->
            <div class="logo"> <a href="{{url('/')}}"> <img src="{{asset($site->logo)}}" alt="logo"> </a> </div>
            <!-- /.logo -->
            <!-- ============================================================= LOGO : END ============================================================= --> </div>
          <!-- /.logo-holder -->

          <div class="col-xs-12 col-sm-12 col-md-7 top-search-holder">
            <!-- /.contact-row -->
            <!-- ============================================================= SEARCH AREA ============================================================= -->
            @php
                $categories = App\Models\Category::get();
            @endphp
            <div class="search-area ">
              <form action="{{route('product.search')}}" method="GET">
                @csrf
                <div class="control-group">
                  <input class="search-field" autocomplete="off" id="product_search" name="product_search" placeholder="Tìm kiếm tại đây..." />

                </div>
              </form>
            </div>

{{-- show product --}}
<div class="show-product" >





</div>
            <style>
.show-product {
    z-index: 1;
    position: absolute;
    background:#75d3fe;
     width: calc(100% - 30px);
}
.row.product {
    padding: 10px;
    border-bottom: 1px solid;
    margin: 0px;
}
            </style>
            <!-- /.search-area -->
            <!-- ============================================================= SEARCH AREA : END ============================================================= -->
        </div>





          <!-- /.top-search-holder -->

          <div class="col-xs-12 col-sm-12 col-md-2 animate-dropdown top-cart-row">
            <!-- ============================================================= SHOPPING CART DROPDOWN ============================================================= -->

            <div class="dropdown dropdown-cart"> <a href="#" class="dropdown-toggle lnk-cart" data-toggle="dropdown">
              <div class="items-cart-inner">
                <div class="basket"> <i class="glyphicon glyphicon-shopping-cart"></i> </div>
                <div class="basket-item-count"><span id="countCartMini"></span></div>
                <div class="total-price-basket"> <span class="lbl"></span> <span class="total-price"><span  class="value totalPriceCartMini">0</span> </span> </div>
              </div>
              </a>
              <ul class="dropdown-menu">
                <li>
                    <div id="cartMini">

                    </div>

                  <div class="clearfix cart-total">
                    <div class="pull-right"> <span class="text">Tổng tiền :</span><span class="totalPriceCartMini"></span> </div>
                    <div class="clearfix"></div>
                    <a href="checkout.html" class="btn btn-upper btn-primary btn-block m-t-20">Thanh toán</a> </div>
                  <!-- /.cart-total-->

                </li>
              </ul>
              <!-- /.dropdown-menu-->
            </div>
            <!-- /.dropdown-cart -->
            <!-- ============================================================= SHOPPING CART DROPDOWN : END============================================================= --> </div>
          <!-- /.top-cart-row -->
        </div>
        <!-- /.row -->

      </div>
      <!-- /.container -->

    </div>
    <!-- /.main-header -->

    <!-- ============================================== NAVBAR ============================================== -->
    <div class="header-nav animate-dropdown">
      <div class="container">
        <div class="yamm navbar navbar-default" role="navigation">
          <div class="navbar-header">
         <button data-target="#mc-horizontal-menu-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
         <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          </div>
          <div class="nav-bg-class">
            <div class="navbar-collapse collapse" id="mc-horizontal-menu-collapse">
              <div class="nav-outer">
                <ul class="nav navbar-nav">
                  <!-- <li class="active dropdown yamm-fw"> <a href="{{url('/')}}" data-hover="dropdown" class="dropdown-toggle" data-toggle="dropdown">Trang chủ</a> </li> -->
@php
    $categories = App\Models\Category::orderBy('category_name_vn', 'ASC')->get();
@endphp
                    @foreach ($categories as $category)
 <li class="dropdown yamm mega-menu"> <a href="home.html" data-hover="dropdown" class="dropdown-toggle" data-toggle="dropdown">{{$category->category_name_vn}}</a>
                    <ul class="dropdown-menu container">
                      <li>
                        <div class="yamm-content ">
                          <div class="row">
                            @php
                                $subCateogries = App\Models\SubCategory::where('category_id', $category->id)->orderBy('subCategory_name_vn', 'ASC')->get();
                            @endphp
                            @foreach ($subCateogries as $subCategory)
                                <div class="col-xs-12 col-sm-6 col-md-2 col-menu">
                                    <h2 class="title"><a style="padding: 0px;" href="{{url('subcategory/'.$subCategory->id.'/'.$subCategory->subCategory_slug_vn)}}">{{$subCategory->subCategory_name_vn}}</a></h2>
                                    <ul class="links">
                                        @php
                                             $subsubCategories = App\Models\SubSubCategory::where('subCategory_id', $subCategory->id)->orderBy('Subsubcategory_name_vn', 'ASC')->get();
                                        @endphp
                                        @foreach ($subsubCategories as $subsubCategory)
                                            <li><a href="{{url('subsubCategory/'.$subsubCategory->id.'/'.$subsubCategory->Subsubcategory_slug_vn)}}">{{$subsubCategory->Subsubcategory_name_vn}}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endforeach

                            <!-- /.col -->
                            <div class="col-xs-12 col-sm-6 col-md-4 col-menu banner-image"> <img class="img-responsive" src="{{asset('frontend/assets/images/banners/top-menu-banner.jpg')}}" alt=""> </div>
                            <!-- /.yamm-content -->
                          </div>
                        </div>
                      </li>
                    </ul>
                  </li>

                    @endforeach
                  <li class="dropdown  navbar-right special-menu"> <a href="{{url('blogs')}}">Bài viết</a> </li>
                  <li class="dropdown  navbar-right special-menu"> <a href="{{route('product.shop')}}">Sản phẩm</a> </li>
                </ul>
                <!-- /.navbar-nav -->
                <div class="clearfix"></div>
              </div>
              <!-- /.nav-outer -->
            </div>
            <!-- /.navbar-collapse -->

          </div>
          <!-- /.nav-bg-class -->
        </div>
        <!-- /.navbar-default -->
      </div>
      <!-- /.container-class -->

    </div>
    <!-- /.header-nav -->
    <!-- ============================================== NAVBAR : END ============================================== -->

  </header>
  <div class="modal fade" id="checkorder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">TRA CỨU TÌNH TRẠNG ĐƠN HÀNG</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{route('order.search')}}" method="get">
                @csrf
                <div class="form-group">
                    <label for="exampleInputEmail1">Mã hóa đơn</label>
                    <input type="text" name="invoice_no" class="form-control" id="invoice_no"  placeholder="Nhập mã đơn hàng">
                </div>
                <button type="submit" class="btn btn-primary">Tìm kiếm</button>
            </form>
        </div>

      </div>
    </div>
  </div>
