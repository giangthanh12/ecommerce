<div class="col-sm-6 text-center">
    <h1 class="text-warning mt-4 mb-4">
        <b><span id="average_rating">{{$avgRating}}</span> / 5</b>
    </h1>
    <div class="mb-3">
        @for ($i = 0; $i < 5; $i++)

            <i class="fa fa-star star-light mr-1 main_star  {{$i<$roundRating ? 'text-warning' :''}}"></i>
        @endfor


    </div>
</div>
<div class="col-sm-6">
    <p>
        <div class="progress-label-left"><b>5</b> <i class="fa fa-star text-warning"></i></div>

        <div class="progress-label-right">(<span id="total_five_star_review">{{$count_rating_5}}</span>)</div>
        <div class="progress">
            <div class="progress-bar bg-warning" role="progressbar"  style="background-color:#ffc107; min-width:0px; width:{{$count_rating_5/$count*100}}%" aria-valuenow="{{$count_rating_5/$count*100}}" aria-valuemin="0" aria-valuemax="100" id="five_star_progress"></div>
        </div>
    </p>
    <p>
        <div class="progress-label-left"><b>4</b> <i class="fa fa-star text-warning"></i></div>

        <div class="progress-label-right">(<span id="total_four_star_review">{{$count_rating_4}}</span>)</div>
        <div class="progress">
            <div class="progress-bar bg-warning" role="progressbar" style="background-color:#ffc107; min-width:0px; width:{{$count_rating_4/$count*100}}%" aria-valuenow="{{$count_rating_4/$count*100}}" aria-valuemin="0" aria-valuemax="100" id="four_star_progress"></div>
        </div>
    </p>
    <p>
        <div class="progress-label-left"><b>3</b> <i class="fa fa-star text-warning"></i></div>

        <div class="progress-label-right">(<span id="total_three_star_review">{{$count_rating_3}}</span>)</div>
        <div class="progress">
            <div class="progress-bar bg-warning" role="progressbar"  style="background-color:#ffc107; min-width:0px; width:{{$count_rating_3/$count*100}}%" aria-valuenow="{{$count_rating_3/$count*100}}" aria-valuemin="0" aria-valuemax="100" id="three_star_progress"></div>
        </div>
    </p>
    <p>
        <div class="progress-label-left"><b>2</b> <i class="fa fa-star text-warning"></i></div>

        <div class="progress-label-right">(<span id="total_two_star_review">{{$count_rating_2}}</span>)</div>
        <div class="progress">
            <div class="progress-bar bg-warning" role="progressbar"  style="background-color:#ffc107; min-width:0px; width:{{$count_rating_2/$count*100}}%" aria-valuenow="{{$count_rating_2/$count*100}}" aria-valuemin="0" aria-valuemax="100" id="two_star_progress"></div>
        </div>
    </p>
    <p>
        <div class="progress-label-left"><b>1</b> <i class="fa fa-star text-warning"></i></div>

        <div class="progress-label-right">(<span id="total_one_star_review">{{$count_rating_1}}</span>)</div>
        <div class="progress">
            <div class="progress-bar bg-warning" role="progressbar" style="background-color:#ffc107; min-width:0px; width:{{$count_rating_1/$count*100}}%" aria-valuenow="{{$count_rating_1/$count*100}}" aria-valuemin="0" aria-valuemax="100" id="one_star_progress"></div>
        </div>
    </p>
</div>
