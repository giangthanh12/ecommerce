@extends('frontend.main_layout')

@section('content')
<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="{{url('/')}}">Trang chủ</a></li>
				<li class='active'>{{$blog->post_title_vn}}</li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->

<div class="body-content">
	<div class="container">
		<div class="row">
			<div class="blog-page">
				<div class="col-md-9">
					<div class="blog-post wow fadeInUp">
	<img class="img-responsive" src="{{asset($blog->post_image)}}" alt="">
	<h1>{{$blog->post_title_vn}}</h1>
	{{-- <span class="author">John Doe</span>
	<span class="review">7 Comments</span> --}}
	<span class="date-time">{{$blog->created_at}}</span>
     <div>
         {!! $blog->post_detail_vn !!}
    </div>
	<div class="social-media">
		<span>Chia sẻ bài viết:</span>
		<div class="addthis_inline_share_toolbox"></div>
	</div>
</div>

<div class="blog-write-comment outer-bottom-xs outer-top-xs">
	<div class="row">
		<div class="col-md-12">
			<h4>Bình luận</h4>
		</div>
		<div class="col-md-4">
			<form class="register-form" role="form">
				<div class="form-group">
			    <label class="info-title" for="exampleInputName">Tên của bạn<span>*</span></label>
			    <input type="email" class="form-control unicase-form-control text-input" id="exampleInputName" placeholder="">
			  </div>
			</form>
		</div>
		<div class="col-md-4">
			<form class="register-form" role="form">
				<div class="form-group">
			    <label class="info-title" for="exampleInputEmail1">Địa chỉ email<span>*</span></label>
			    <input type="email" class="form-control unicase-form-control text-input" id="exampleInputEmail1" placeholder="">
			  </div>
			</form>
		</div>
		<div class="col-md-4">
			<form class="register-form" role="form">
				<div class="form-group">
			    <label class="info-title" for="exampleInputTitle">Tiêu đề <span>*</span></label>
			    <input type="email" class="form-control unicase-form-control text-input" id="exampleInputTitle" placeholder="">
			  </div>
			</form>
		</div>
		<div class="col-md-12">
			<form class="register-form" role="form">
				<div class="form-group">
			    <label class="info-title" for="exampleInputComments">Nội dung <span>*</span></label>
			    <textarea class="form-control unicase-form-control" id="exampleInputComments" ></textarea>
			  </div>
			</form>
		</div>
		<div class="col-md-12 outer-bottom-small m-t-20">
			<button type="submit" class="btn-upper btn btn-primary checkout-page-button">Submit Comment</button>
		</div>
	</div>
</div>
				</div>
				<div class="col-md-3 sidebar">



					<div class="sidebar-module-container">
						<div class="search-area outer-bottom-small">

</div>


				<!-- ==============================================CATEGORY============================================== -->
<div class="sidebar-widget outer-bottom-xs wow fadeInUp">
	<h3 class="section-title">Danh mục bài viết</h3>
	<div class="sidebar-widget-body m-t-10">
        <div class="list-group">
            @foreach ($blogCategories as $cat)
            <a href="{{url('blog/category/'.$cat->post_category_slug_vn)}}" class="list-group-item list-group-item-action {{$blog->post_category_id === $cat->id ? 'active' :''}}">{{$cat->post_category_name_vn}}</a>
            @endforeach
        </div>
	</div><!-- /.sidebar-widget-body -->
</div><!-- /.sidebar-widget -->
	<!-- ============================================== CATEGORY : END ============================================== -->

						<!-- ============================================== PRODUCT TAGS ============================================== -->
@include('frontend.common.post_tags')
<!-- ============================================== PRODUCT TAGS : END ============================================== -->					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-617d5ffe49fd2879"></script>
@endsection

