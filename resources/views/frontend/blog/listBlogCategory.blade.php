@extends('frontend.main_layout')

@section('content')
<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="#">Home</a></li>
				<li class='active'><a href="">{{$postCategory->post_category_name_vn}}</a></li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->

<div class="body-content">
	<div class="container">
		<div class="row">
			<div class="blog-page">
				<div class="col-md-9">
                    @foreach ($blogs as $blog)
                        <div class="blog-post  wow fadeInUp">
                            <a href="{{url('blog/'.$blog->post_slug_vn)}}"><img class="img-responsive" src="{{asset($blog->post_image)}}" alt=""></a>
                            <h1><a href="{{url('blog/'.$blog->post_slug_vn)}}">{{$blog->post_title_vn}}</a></h1>

                            <span class="date-time">{{$blog->created_at}}</span>
                            <p>{!! Str::limit($blog->post_detail_vn, 300) !!}</p>
                            <a href="{{url('blog/'.$blog->post_slug_vn)}}" class="btn btn-upper btn-primary read-more">đọc thêm</a>
                        </div>
                    @endforeach


<div class="clearfix blog-pagination filters-container  wow fadeInUp" style="padding:0px; background:none; box-shadow:none; margin-top:15px; border:none">

	<div class="text-right">
        {{$blogs->links('vendor.pagination.custom')}}
    </div><!-- /.text-right -->

</div><!-- /.filters-container -->
</div>
				<div class="col-md-3 sidebar">



					<div class="sidebar-module-container">
						<div class="search-area outer-bottom-small">
    <form>
        <div class="control-group">
            <input placeholder="Type to search" class="search-field">
            <a href="#" class="search-button"></a>
        </div>
    </form>
</div>

<div class="home-banner outer-top-n outer-bottom-xs">
<img src="{{asset('frontend/assets/images/banners/LHS-banner.jpg')}}" alt="Image">
</div>
				<!-- ==============================================CATEGORY============================================== -->
<div class="sidebar-widget outer-bottom-xs wow fadeInUp">
	<h3 class="section-title">Danh mục bài viết</h3>
	<div class="sidebar-widget-body m-t-10">
        <div class="list-group">
            @foreach ($blogCategories as $cat)
            <a href="{{url('blog/category/'.$cat->post_category_slug_vn)}}" class="list-group-item list-group-item-action {{$blog->post_category_id === $cat->id ? 'active' :''}}">{{$cat->post_category_name_vn}}</a>
            @endforeach
        </div>
	</div><!-- /.sidebar-widget-body -->
</div><!-- /.sidebar-widget -->


	<!-- ============================================== CATEGORY : END ============================================== -->

						<!-- ============================================== PRODUCT TAGS ============================================== -->
@include('frontend.common.post_tags')
<!-- ============================================== PRODUCT TAGS : END ============================================== -->					</div>
				</div>
			</div>
		</div>
		<!-- ============================================== BRANDS CAROUSEL ============================================== -->
@include('frontend.body.brands')
<!-- ============================================== BRANDS CAROUSEL : END ============================================== -->	</div>
</div>


@endsection

