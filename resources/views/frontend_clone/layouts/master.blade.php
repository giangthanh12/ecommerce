<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;400&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="stylesheet" href="{{asset("frontend_clone/css/main.css")}}">
    <link rel="stylesheet" href="{{asset("frontend_clone/css/style.css")}}">
    <link rel="stylesheet" href="{{asset("frontend_clone/css/media.css")}}">
    <script src="{{asset("frontend_clone/js/btnSigIn.js")}}"></script>
    <meta name="viewport" content="width=device-width, inital-scale=1.0">
    <link rel="stylesheet" href="{{asset("frontend_clone/font/fontawesome-free-6.1.2-web/css/all.min.css")}}" >
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    <title>HiBeYo</title>
</head>
<body>
   <div class="all">
      <!-- Header -->
    @include("frontend_clone.body.header")

      <div class=>
         <div class="container ">
            @include("frontend_clone.body.slide")

            <div class="banner container ">
               <div class="banner__img dis_flex" style="padding-left: 0px;padding-right: 0px;">
                  <div class="img__banner">
                     <img class="img__banner__inside" src="{{asset('frontend_clone/img/1.png')}}" alt="">
                  </div>
                  <div class="img__banner">
                     <img class="img__banner__inside" src="{{asset('frontend_clone/img/2.png')}}" alt="">
                  </div>
                  <div class="img__banner">
                     <img class="img__banner__inside" src="{{asset('frontend_clone/img/3.png')}}" alt="">
                  </div>
               </div>
               <div class="banner__img-longer container w-full">
                  <div  class="img-longer" style="display: flex; justify-content: center;">
                     <img class="img__longer loca-1" src="{{asset('frontend_clone/img/bnn1.png')}}" alt="banner img">
                     <img class="img__longer loca-2" src="{{asset('frontend_clone/img/bnn2.png')}}" alt="banner img">
                     <img class="img__longer loca-3" src="{{asset('frontend_clone/img/bnn3.png')}}" alt="banner img">
                     <img class="img__longer loca-4" src="{{asset('frontend_clone/img/bnn4.png')}}" alt="banner img">
                  </div>
               </div>
            </div>
         </div>
         <!-- Best sell -->
         <div class="best-sell  ">
            <div class="best-sell__box container pad-1-lr  ">
               <div class="best-sell__box__header">
                  <img class="box__header__img" src="{{asset("frontend_clone/img/best-sell.png")}}" alt="">
               </div>
               <div class="best-sell__box__items">
                  <div class="best-sell__box__items__list">
                     <div class="item-1 ">
                        <a href="" class="item-1_info">

                        </a>
                     </div>
                     <div class="item-2">
                        <a href="" class="item-2_info">

                        </a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- Featured Products -->
         <div class="products"></div>
         <!-- New Products -->
         <div class="new-products container pad-1-lr">
            <div class="new-products__header container pad-1-lr">
               <div class="new-products__header__img w-full dis_flex">
                  <img class="header__img" src="{{asset("frontend_clone/img/logo_down.png")}}" alt="">
               </div>
            </div>
            <div class="new-products__table">
               <div class="new-products__table__header text-tran">
                  <h3 style="margin: 0;">Sản phẩm mới nhất</h3>
               </div>
               <div class="new-products__table__body grid-clos-6">
                  <a class="table__body__link" href="">
                     <div class="table__body__box">
                        <div class="body__box__img dis_flex">
                           <div class="img__inside pos-rela">
                              <img class="table__img" src="{{asset("frontend_clone/img/sp1.png")}}" alt="Sữa Hikid dê Hàn Quốc tăng chiều cao ( 1- 9 tuổi)">
                           </div>
                        </div>
                        <h4 class="body__box__text text-nm">Sữa Hikid dê Hàn Quốc tăng chiều cao ( 1- 9 tuổi)</h4>
                        <p class="body__box__price">685,000 VNĐ </p>
                     </div>
                  </a>

                  <a class="table__body__link" href="">
                     <div class="table__body__box">
                        <div class="body__box__img dis_flex">
                           <div class="img__inside pos-rela">
                              <img class="table__img" src="{{asset("frontend_clone/img/sp1.png")}}" alt="Sữa Hikid dê Hàn Quốc tăng chiều cao ( 1- 9 tuổi)">
                           </div>
                        </div>
                        <h4 class="body__box__text text-nm">Sữa Hikid dê Hàn Quốc tăng chiều cao ( 1- 9 tuổi)</h4>
                        <p class="body__box__price">685,000 VNĐ </p>
                     </div>
                  </a>

                  <a class="table__body__link" href="">
                     <div class="table__body__box">
                        <div class="body__box__img dis_flex">
                           <div class="img__inside pos-rela">
                              <img class="table__img" src="{{asset("frontend_clone/img/sp1.png")}}" alt="Sữa Hikid dê Hàn Quốc tăng chiều cao ( 1- 9 tuổi)">
                           </div>
                        </div>
                        <h4 class="body__box__text text-nm">Sữa Hikid dê Hàn Quốc tăng chiều cao ( 1- 9 tuổi)</h4>
                        <p class="body__box__price">685,000 VNĐ </p>
                     </div>
                  </a>

                  <a class="table__body__link" href="">
                     <div class="table__body__box">
                        <div class="body__box__img dis_flex">
                           <div class="img__inside pos-rela">
                              <img class="table__img" src="{{asset("frontend_clone/img/sp1.png")}}" alt="Sữa Hikid dê Hàn Quốc tăng chiều cao ( 1- 9 tuổi)">
                           </div>
                        </div>
                        <h4 class="body__box__text text-nm">Sữa Hikid dê Hàn Quốc tăng chiều cao ( 1- 9 tuổi)</h4>
                        <p class="body__box__price">685,000 VNĐ </p>
                     </div>
                  </a>

                  <a class="table__body__link" href="">
                     <div class="table__body__box">
                        <div class="body__box__img dis_flex">
                           <div class="img__inside pos-rela">
                              <img class="table__img" src="{{asset("frontend_clone/img/sp1.png")}}" alt="Sữa Hikid dê Hàn Quốc tăng chiều cao ( 1- 9 tuổi)">
                           </div>
                        </div>
                        <h4 class="body__box__text text-nm">Sữa Hikid dê Hàn Quốc tăng chiều cao ( 1- 9 tuổi)</h4>
                        <p class="body__box__price">685,000 VNĐ </p>
                     </div>
                  </a>

                  <a class="table__body__link" href="">
                     <div class="table__body__box">
                        <div class="body__box__img dis_flex">
                           <div class="img__inside pos-rela">
                              <img class="table__img" src="{{asset("frontend_clone/img/sp1.png")}}" alt="Sữa Hikid dê Hàn Quốc tăng chiều cao ( 1- 9 tuổi)">
                           </div>
                        </div>
                        <h4 class="body__box__text text-nm">Sữa Hikid dê Hàn Quốc tăng chiều cao ( 1- 9 tuổi)</h4>
                        <p class="body__box__price">685,000 VNĐ </p>
                     </div>
                  </a>

                  <a class="table__body__link" href="">
                     <div class="table__body__box">
                        <div class="body__box__img dis_flex">
                           <div class="img__inside pos-rela">
                              <img class="table__img" src="{{asset("frontend_clone/img/sp1.png")}}" alt="Sữa Hikid dê Hàn Quốc tăng chiều cao ( 1- 9 tuổi)">
                           </div>
                        </div>
                        <h4 class="body__box__text text-nm">Sữa Hikid dê Hàn Quốc tăng chiều cao ( 1- 9 tuổi)</h4>
                        <p class="body__box__price">685,000 VNĐ </p>
                     </div>
                  </a>

                  <a class="table__body__link" href="">
                     <div class="table__body__box">
                        <div class="body__box__img dis_flex">
                           <div class="img__inside pos-rela">
                              <img class="table__img" src="{{asset("frontend_clone/img/sp1.png")}}" alt="Sữa Hikid dê Hàn Quốc tăng chiều cao ( 1- 9 tuổi)">
                           </div>
                        </div>
                        <h4 class="body__box__text text-nm">Sữa Hikid dê Hàn Quốc tăng chiều cao ( 1- 9 tuổi)</h4>
                        <p class="body__box__price">685,000 VNĐ </p>
                     </div>
                  </a>

                  <a class="table__body__link" href="">
                     <div class="table__body__box">
                        <div class="body__box__img dis_flex">
                           <div class="img__inside pos-rela">
                              <img class="table__img" src="{{asset("frontend_clone/img/sp1.png")}}" alt="Sữa Hikid dê Hàn Quốc tăng chiều cao ( 1- 9 tuổi)">
                           </div>
                        </div>
                        <h4 class="body__box__text text-nm">Sữa Hikid dê Hàn Quốc tăng chiều cao ( 1- 9 tuổi)</h4>
                        <p class="body__box__price">685,000 VNĐ </p>
                     </div>
                  </a>

                  <a class="table__body__link" href="">
                     <div class="table__body__box">
                        <div class="body__box__img dis_flex">
                           <div class="img__inside pos-rela">
                              <img class="table__img" src="{{asset("frontend_clone/img/sp1.png")}}" alt="Sữa Hikid dê Hàn Quốc tăng chiều cao ( 1- 9 tuổi)">
                           </div>
                        </div>
                        <h4 class="body__box__text text-nm">Sữa Hikid dê Hàn Quốc tăng chiều cao ( 1- 9 tuổi)</h4>
                        <p class="body__box__price">685,000 VNĐ </p>
                     </div>
                  </a>

                  <a class="table__body__link" href="">
                     <div class="table__body__box">
                        <div class="body__box__img dis_flex">
                           <div class="img__inside pos-rela">
                              <img class="table__img" src="{{asset("frontend_clone/img/sp1.png")}}" alt="Sữa Hikid dê Hàn Quốc tăng chiều cao ( 1- 9 tuổi)">
                           </div>
                        </div>
                        <h4 class="body__box__text text-nm">Sữa Hikid dê Hàn Quốc tăng chiều cao ( 1- 9 tuổi)</h4>
                        <p class="body__box__price">685,000 VNĐ </p>
                     </div>
                  </a>

                  <a class="table__body__link" href="">
                     <div class="table__body__box">
                        <div class="body__box__img dis_flex">
                           <div class="img__inside pos-rela">
                              <img class="table__img" src="{{asset("frontend_clone/img/sp1.png")}}" alt="Sữa Hikid dê Hàn Quốc tăng chiều cao ( 1- 9 tuổi)">
                           </div>
                        </div>
                        <h4 class="body__box__text text-nm">Sữa Hikid dê Hàn Quốc tăng chiều cao ( 1- 9 tuổi)</h4>
                        <p class="body__box__price">685,000 VNĐ </p>
                     </div>
                  </a>
               </div>
               <a class="new-products__table__btn__link" href="">
                  <div class="new-products__table__btn dis_flex">
                     <button class="btn text-tran">
                        <span>Xem tất cả sản phẩm</span>
                        <i class="btn__icon fa-solid fa-right-long"></i>
                     </button>
                  </div>
               </a>
            </div>
         </div>
         <!-- News -->
         <div class="news">
            <div class="news__box container pad-1-lr pos-rela ">
               <div class="news__box__inside">
                  <div class="box__inside pos-ab">
                     <img class="news__img" src="{{asset("frontend_clone/img/news.png")}}" alt="">
                  </div>
                  <a href="" class="news__more pos-ab">
                     <div class="news__more__box dis_flex justi-center">
                        <button class="news__more__box__btn algin_center dis_flex text-tran">
                           <span class="news__more__text">Xem tất cả tin tức</span>
                           <i class="btn__icon fa-solid fa-right-long"></i>
                        </button>
                     </div>
                  </a>
                  <div class="new__box__tag dis_flex">
                     <div class="box__tag__all">
                        <a class="box__tag__link" href="">
                           <div class="box__tag__img">
                              <img class="tag__img w-full" src="{{asset("frontend_clone/img/news1.png")}}" alt="">
                           </div>
                           <div class="box__tag__text w-full pos-rela">
                              <div class="box__tag__text__inside pos-rela w-full">
                                 <div class="box__tag__text__back ">
                                    <h4 class="tag__text_header">
                                       Canxi Bio Island Milk Calcium Bone Care 150v - trên 12 tuổi
                                    </h4>
                                    <div class="tag__text__body">
                                       <p class="text__body">
                                          Viên sữa bổ sung canxi Bio Island Milk Calcium Bone Care
                                          được điều chế dựa trên nguồn nguyên liệu
                                          chính là sữa bò nguyên chất,
                                          chính vì vậy viên có hương vị sữa bò,
                                          rất thơm và dễ uống.
                                       </p>
                                    </div>
                                    <div class="tag__text__fooder">
                                       16/07/2021
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </a>
                     </div>

                     <div class="box__tag__all">
                        <a class="box__tag__link" href="">
                           <div class="box__tag__img">
                              <img class="tag__img w-full" src="{{asset("frontend_clone/img/news1.png")}}" alt="">
                           </div>
                           <div class="box__tag__text w-full pos-rela">
                              <div class="box__tag__text__inside pos-rela w-full">
                                 <div class="box__tag__text__back ">
                                    <h4 class="tag__text_header">
                                       Canxi Bio Island Milk Calcium Bone Care 150v - trên 12 tuổi
                                    </h4>
                                    <div class="tag__text__body">
                                       <p class="text__body">
                                          Viên sữa bổ sung canxi Bio Island Milk Calcium Bone Care
                                          được điều chế dựa trên nguồn nguyên liệu
                                          chính là sữa bò nguyên chất,
                                          chính vì vậy viên có hương vị sữa bò,
                                          rất thơm và dễ uống.
                                       </p>
                                    </div>
                                    <div class="tag__text__fooder">
                                       16/07/2021
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </a>
                     </div>

                     <div class="box__tag__all">
                        <a class="box__tag__link" href="">
                           <div class="box__tag__img">
                              <img class="tag__img w-full" src="{{asset("frontend_clone/img/news1.png")}}" alt="">
                           </div>
                           <div class="box__tag__text w-full pos-rela">
                              <div class="box__tag__text__inside pos-rela w-full">
                                 <div class="box__tag__text__back ">
                                    <h4 class="tag__text_header">
                                       Canxi Bio Island Milk Calcium Bone Care 150v - trên 12 tuổi
                                    </h4>
                                    <div class="tag__text__body">
                                       <p class="text__body">
                                          Viên sữa bổ sung canxi Bio Island Milk Calcium Bone Care
                                          được điều chế dựa trên nguồn nguyên liệu
                                          chính là sữa bò nguyên chất,
                                          chính vì vậy viên có hương vị sữa bò,
                                          rất thơm và dễ uống.
                                       </p>
                                    </div>
                                    <div class="tag__text__fooder">
                                       16/07/2021
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <!-- FOOTER -->
      @include("frontend_clone.body.footer")
   </div>

   @include("frontend_clone.body.authenticate")
</body>
</html>
