<div class="header">
    <div class="header__up">
       <div class="header__up--all container dis_flex">
          <div class="header__up__menu-390 ">
             <div class="menu-390__bnt dis_flex">
                <button type="button" class="menu__bnt">
                   <i class="icon__menu__bnt fa-solid fa-list"></i>
                </button>
                <div class="header__up__menu-390__list pos-ab w-full">
                   <div class="menu-390__list">
                      <ul class="menu-390__list__item pad-1-lr show-menu">
                         <li class="menu-390__item h-full text-tran">
                            <a  class="menu-390__link h-full dis_flex pd-tb-75" href="">Trang chủ</a>
                         </li>
                         <li class="menu-390__item h-full text-tran">
                            <a  class="menu-390__link h-full dis_flex pd-tb-75" href="">Tin tức</a>
                         </li>
                         <li class="menu-390__item h-full text-tran">
                            <a  class="menu-390__link h-full dis_flex pd-tb-75" href="">Giới thiệu</a>
                         </li>
                         <li class="menu-390__item h-full text-tran">
                            <a  class="menu-390__link h-full dis_flex pd-tb-75" href="">khuyến mãi</a>
                         </li>
                         <li class="menu-390__item h-full text-tran">
                            <a  class="menu-390__link h-full dis_flex pd-tb-75" href="">sản phẩm</a>
                         </li>
                         <li class="menu-390__item h-full text-tran">
                            <a  class="menu-390__link h-full dis_flex pd-tb-75" href="">liên hệ</a>
                         </li>
                      </ul>
                   </div>
                </div>
             </div>
          </div>
          <!-- Logo -->
          <div class="header__up__logo dis_flex">
             <div class="logo__img">
                <a href="" class="logo__img--outside">
                   <img src="{{asset("frontend_clone/img/logo2 1.png")}}" alt="logo" class="logo__img--inside">
                </a>
                <img src="{{asset("frontend_clone/img/Frame.png")}}" alt="logo down" class="logo__img__dow">
             </div>
          </div>
          <!-- Search-bar -->
          <div class="header__up__search-bar">
             <form action="" class="search-bar--m dis_flex algin_center">
                <input class="search-bar__input" type="text" placeholder="Tìm kiếm sản phẩm tại đây">
                <button type="submit" class="search__btn algin_center flex">
                   <i class="search__btn__icon fa-sharp fa-solid fa-magnifying-glass"></i>
                </button>
             </form>
             <!-- <div class="search-bar__category dis_flex algin_center">
                <select class="search-bar__category-slug" name="" id=""></select>
             </div> -->
          </div>

          <div class="header__up__user dis_flex algin_center">
             <div class="user__log-sig">
                <i class="fa-solid fa-user"></i>
                <button id="login__user" class="user__log-in box text" onclick="myFunction()">Đăng nhập</button>
                <button id="sigin__user" class="user__sig-in box text" onclick="mySigin()">Đăng ký</button>
             </div>
             <!-- User -->
             <div class="user__cart dis_flex algin_center">
                <i class="icon__cart fa-solid fa-cart-shopping"></i>
                <p class="cart__text text">Giỏ hàng</p>
             </div>
          </div>
       </div>
    </div>

    <div class="header__dow">
      <div class="header__dow__nav-bar container ">
          <div class="nav-bar">
             <ul class="nav-bar__list dis_flex">
                <li class="list text text-tran grid__column-4 menu__btn"  data-toggle="dropdown">
                   <button class="menu__box" onclick="dropDown()">
                      <div class="menu__box__btn">
                         <i class="fa-solid fa-bars icon__btn"></i>
                         <span class="menu__text text-tran text">danh mục sản phẩm </span>
                      </div>
                   </button>
                   <div class="list__menu__box">
                      <ul class="slide__menu__list w-full">
                         <li class="menu-list pad-1 text-tran border-col-1 text-nm">Bé uống</li>
                         <li class="menu-list pad-1 text-tran border-col-2 text-nm">bé vệ sinh</li>
                         <li class="menu-list pad-1 text-tran border-col-1 text-nm">bé chơi và học</li>
                         <li class="menu-list pad-1 text-tran border-col-2 text-nm">bé khoẻ và an toàn</li>
                         <li class="menu-list pad-1 text-tran border-col-1 text-nm">dành cho gia đình</li>
                         <li class="menu-list pad-1 text-tran border-col-2 text-nm">góc của mẹ</li>
                         <li class="menu-list pad-1 text-tran border-col-1 text-nm">bánh kẹo đồ uống</li>
                         <li class="menu-list pad-1 text-tran border-col-2 text-nm">hoa quả sạch</li>
                         <li class="menu-list pad-1 text-tran border-col-1 text-nm">bé ăn</li>
                      </ul>
                   </div>
                </li>
                <li class="list text text-tran">
                   <a class="list__link" href="/">Trang chủ</a>
                </li>
                <li class="list text text-tran">
                   <a class="list__link" href="">Tin tức</a>
                </li>
                <li class="list text text-tran">
                   <a class="list__link" href="">Giới thiệu</a>
                </li>
                <li class="list text text-tran">
                   <a class="list__link" href="">khuyến mãi</a>
                </li>
                <li class="list text text-tran">
                   <a class="list__link" href="/html/products.html">sản phẩm</a>
                </li>
                <li class="list text text-tran">
                   <a class="list__link border__end" href="">liên hệ</a>
                </li>
             </ul>
          </div>
      </div>
    </div>
 </div>
