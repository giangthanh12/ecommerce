<footer class="footer ">
    <div class="footer__box w-full pos-rela">
       <div class="footer__deal w-full">
          <div class="footer__deal__box container pad-1-lr">
             <h2 class="deal__box__text">Nhập email của bạn để nhận được thông tin ưu đãi mới nhất của Hibeyo</h2>
             <div class="deal__box__input w-full dis_flex justi-center ">
                <form action="" class="deal__box__input__text dis_flex pos-rela ">
                   <input type="text" class="input__text w-full pd-t-b-2" placeholder="Nhập email của bạn">
                   <div class="deal__box__input__bnt dis_flex pos-ab">
                      <button type="submit" class="input__bnt pd-top-bot pd-l-r-3 text-tran ">Đăng ký</button>
                   </div>
                   <div class="deal__box__input__icon pos-ab dis_flex algin_center">
                      <img class="input__icon w-full" src="{{asset("frontend_clone/img/email.png")}}" alt="">
                   </div>
                </form>
             </div>
          </div>
       </div>
    </div>

    <div class="footer__contacts w-full">
       <div class="footer__contacts__box pos-rela">
          <div class="footer__contacts__logo dis_flex">
             <img src="{{asset("frontend_clone/img/logo 3.png")}}" alt="" class="logo__footer ">
          </div>
          <div class="footer__contacts__location container pad-1-lr text-nm">
             <p>Địa chỉ: Số 241 Hoàng Văn Thụ, Thái Nguyên</p>
             <p>Hotline: 0902 55 99 22</p>
             <p>Email: hibeyo241@gmail.com</p>
             <p>Giờ mở cửa: Từ 8:00 - 22:00 tất cả các ngày trong tuần</p>
          </div>
          <div class="footer__contacts__socail">

          </div>
          <div class="footer__contacts__line container pad-1-lr dis_flex">
             <img class="line__img" src="https://hibeyo.vn/img/img_footer.png" alt="">
          </div>
          <div class="footer__contacts__end">
             <p>© 2021 Hibeyo. All rights reserved.</p>
          </div>
       </div>
    </div>
 </footer>
