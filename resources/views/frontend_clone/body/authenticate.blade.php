<div class="modal dis_flex" id="modal__box" >
    <div class="modal__overlay" id="modal__overlay__box" onclick="myModal()">

    </div>
    <div class="modal__body">
       <!-- Sig in -->
        <div class="auth-form tabcontent" id="Sigin" style="display: none;">
          <div class="auth-form__container">
             <div class="auth-form__header dis_flex">
                <h3 class="auth-form__heading">Đăng ký</h3>
                <button class="auth-form__btn tablinks" onclick="openCity(event, 'Login')"s>Đăng nhập</button>
             </div>

             <div class="auth-form__form">
                <div class="auth-form__gr">
                   <input type="text" class="auth-form__input" placeholder="Email của bạn">
                </div>
                <div class="auth-form__gr">
                   <input type="password" class="auth-form__input" placeholder="Mật khẩu của bạn">
                </div>
                <div class="auth-form__gr">
                   <input type="password" class="auth-form__input" placeholder="Nhập lại mật khẩu">
                </div>
             </div>
             <div class="auth-form__control">
                <button type="button" class="bnt bnt-back bnt--back" onclick="btnBack()">TRỞ LẠI</button>
                <button type="submit" class="bnt bnt--primary">ĐĂNG KÝ</button>
             </div>
          </div>
       </div>

       <!-- Log in -->
       <div class="auth-form tabcontent" id="Login" style="display: none;">
          <div class="auth-form__container">
             <div class="auth-form__header dis_flex">
                <h3 class="auth-form__heading">Đăng nhập</h3>
                <button class="auth-form__btn tablinks" onclick="openCity(event, 'Sigin')" >Đăng ký</button>
             </div>

             <div class="auth-form__form">
                <div class="auth-form__gr">
                   <input type="text" class="auth-form__input" placeholder="Email của bạn">
                </div>
                <div class="auth-form__gr">
                   <input type="password" class="auth-form__input" placeholder="Mật khẩu của bạn">
                </div>
             </div>

             <div class="auth-form__aside">
                <div class="auth-form__help">
                   <a href="" class="auth-form__help-link auth-form__help-ps">Quên mật khẩu</a>
                   <span class="auth-form__help-space"></span>
                   <a href="" class="auth-form__help-link auth-form__help-text">Cần trợ giúp ?</a>
                </div>
             </div>
             <div class="auth-form__control">
                <button type="button" class="bnt bnt-back bnt--back" onclick="btnBack()">TRỞ LẠI</button>
                <button type="button" class="bnt bnt--primary">ĐĂNG NHẬP</button>
             </div>
          </div>
       </div>
    </div>
 </div>
