<?php

namespace App\Providers;

use App\Policies\BrandPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        // Quyền cho brand
        Gate::define('read-brand', [BrandPolicy::class, 'read']);
        Gate::define('create-brand', [BrandPolicy::class, 'create']);
        Gate::define('update-brand', [BrandPolicy::class, 'update']);
        Gate::define('delete-brand', [BrandPolicy::class, 'delete']);
        // Quyền cho brand

            $admin = Auth::guard('admin')->user();
            // kiểm tra xem có phải administrator không nếu là administrator thì được cấp tất cả mọi quyền
            Gate::before(function ($admin, $ability) {
                if ($admin->isAdministrator()) {
                    return true;
                }
                else {
                    return null; // nếu không phải administrator thì trả giá trị null tiếp tục kiểm tra cái khác
                }
            });
    }
}
