<?php

namespace App\Console\Commands;

use App\Models\Admin;
use Illuminate\Console\Command;

class AddNewUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ecommerce:add-user {number}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add new user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        for ($i=0; $i < $this->argument('number'); $i++) { 
          $email = $this->ask("Tài khoản của bạn ?");
          $password = $this->secret("Mật khẩu của bạn ?");
          $admin = new Admin();
          $admin->name = 'admin';
          $admin->email = $email;
          $admin->password = bcrypt($password);
          $admin->phone=123456789;
          $admin->type = 1;
          $admin->save();
        }
        $this->info('success');

    }
}
