<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
    $prefix = request()->route()->getPrefix();
    $prefix = ltrim($prefix,'/');
    $role = Auth::guard('admin')->user()->$prefix;
        if($role != 1) {
            return redirect('admin/dashboard');
        }
        return $next($request);
    }
}
