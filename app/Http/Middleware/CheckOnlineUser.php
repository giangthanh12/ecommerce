<?php

namespace App\Http\Middleware;

use App\Models\User;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class CheckOnlineUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
// Tạm thời chưa thiết lập được middleware hệ thống trong kernel cho chạy tất cả các route
        if (Auth::check()) {
            $expireTime = Carbon::now()->addSeconds(30); // trạng thái thời gian + thêm 30s
            Cache::put('user-is-online' . Auth::user()->id, true, $expireTime); // bộ nhớ sẽ lưu user-on-line trong 30s
            User::where('id',Auth::user()->id)->update(['last_seen' => Carbon::now()]); // thiết lập thời gian
        }
        return $next($request);
    }
}
