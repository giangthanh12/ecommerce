<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use App\Models\MultiImage;
use App\Models\Post;
use App\Models\Product;
use App\Models\Rating;
use App\Models\Slider;
use App\Models\SubCategory;
use App\Models\SubSubCategory;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class IndexController extends Controller
{
    public function index() {
        $products = Product::where('status', 1)->orderBy('id', 'DESC')->get();
        $sliders = Slider::where('status',1)->orderBy('id', 'DESC')->limit(3)->get();
        $categories = Category::orderBy('category_name_vn', 'ASC')->get();
        $products_feature = Product::where('status',1)->where('featured',1)->orderBy('id', 'DESC')->limit(6)->get();
        $product_special_deals = Product::where('status',1)->where('special_deals',1)->orderBy('id', 'DESC')->limit(9)->get();
        $products_hotDeals = Product::where('status',1)->where('hot_deals',1)->where('discount_price', '!=', 'NULL')->orderBy('id', 'DESC')->limit(6)->get();
        $product_special_offer = Product::where('status',1)->where('special_offer',1)->orderBy('id', 'DESC')->limit(9)->get();
        $skip_category_0 = Category::skip(0)->first();
        $skip_product_0 = Product::where('category_id', $skip_category_0->id)->where('status',1)->limit(6)->get();
        $skip_category_1 = Category::skip(1)->first();
        $skip_product_1 = Product::where('category_id', $skip_category_1->id)->where('status',1)->limit(6)->get();
        // $skip_brand_0 = Brand::skip(1)->first();
        // $skip_product_brand_0 = Product::where('brand_id', $skip_brand_0->id)->where('status',1)->limit(6)->get();
        $blogs = Post::where('status',1)->limit(5)->get();

        return view('frontend.index', compact('categories', 'blogs', 'sliders', 'products','skip_category_1','skip_product_1','skip_category_0','skip_product_0','products_feature','products_hotDeals','product_special_offer','product_special_deals'));
    }
    public function profileUser() {
        $id = Auth::id();

        $user = User::find($id);
        return view('frontend.profile.user_profile', compact('user'));
    }
    public function storeProfile(Request $request) {
        $data = User::find(Auth::id());
        $data->name = $request->name;
        $data->email = $request->email;
        $data->phone = $request->phone;
        if($request->file('profile_photo_path')) {
            $file = $request->file('profile_photo_path');
            $file_name = hexdec(uniqid()).$file->getClientOriginalName();
            $file->move(public_path('upload/user_profile'), $file_name);
            if($data->profile_photo_path) {
                unlink(\public_path('upload/user_profile/'.$data->profile_photo_path));
            }
            $data->profile_photo_path = $file_name;
        }
        $data->save();
        $message = array(
            'message'=>'Cập nhập thông tin thành công.',
            'type'=>'success'
        );

        return redirect()->route('dashboard')->with($message);
    }
    public function changePassword() {
        $id = Auth::id();
        $user = User::find($id);
        return view('frontend.profile.user_changePassword', compact('user'));
    }
    public function saveChangePassword(Request $request) {
        $request->validate([
            'current_password'=>'bail|required|min:8',
            'password'=>'bail|required|min:8|confirmed',
        ]);

        $user = User::find(Auth::id());
        $hashedPassword = $user->password;
        if(Hash::check($request->current_password, $hashedPassword)) {
            $user->password = Hash::make($request->password);
            $user->save();
            Auth::logout();
            $notification = array(
                'message'=>'Cập nhật mật khẩu thành công',
                'type'=>'success'
            );
            return redirect()->route('login')->with($notification);
        }
        else {
            $notification = array(
                'message'=>'Mật khẩu hiện tại không chính xác',
                'type'=>'error'
            );
            return redirect()->back()->with($notification);
         }
    }
    public function logoutUser() {
        Auth::logout();
        return redirect()->route('login');
    }
    public function detailProduct($id, $slug) {
        $product = Product::findOrFail($id);
        $products_hotDeals = Product::where('status',1)->where('hot_deals',1)->where('discount_price', '!=', 'NULL')->orderBy('id', 'DESC')->limit(6)->get();
        $count = count(Rating::where('product_id', $id)->get());
        if($count > 0) {
            $totalRating = Rating::where('product_id', $id)->sum('rating');
            $avgRating = $totalRating / $count;
            $roundRating = round($avgRating);
        }
        else {
            $roundRating = 0;
        }

        $product_colors = explode(',', $product->product_color_vn); // chuyển chuỗi màu thành mảng bằng hàm explode
        $product_sizes = explode(',', $product->product_size_vn);// chuyển chuỗi size thành mảng bằng hàm explode
        $multiImages = MultiImage::where('product_id', $id)->get();
        $category_id = $product->category_id;
        $products_related = Product::where('category_id', $category_id)->where('id', '!=', $id)->orderBy('id','DESC')->take(6)->get();
        return view('frontend.product.detail', compact('products_hotDeals', 'roundRating','product', 'multiImages','product_colors','product_sizes','products_related'));
    }


    public function viewProductTag($tag) {
        $categories =  Category::orderBy('category_name_vn', 'ASC')->get();
        $brands = Brand::orderBy('brand_name_vn', 'ASC')->get();
        $products_tag = Product::where('product_tags_vn', $tag)->orderBy('id','DESC')->paginate(3);
        return view('frontend.tag.viewTag', compact('categories','brands','products_tag'));
    }
    public function showProductSubCategory(Request $request,$id) {
        $subcategory = SubCategory::findOrFail($id);
        $categories =  Category::orderBy('category_name_vn', 'ASC')->get();
        $brands = Brand::orderBy('brand_name_vn', 'ASC')->get();
        $products = Product::where('subCategory_id', $id)->where('status',1)->paginate(3);
        //load product ajax
        if($request->ajax()) {
            $products = Product::query();
            $products = $products->where('subCategory_id', $id);
            if ($request->sort_price) {
                switch ($request->sort_price) {
                    case '<500000':
                        $products = $products->whereBetween('selling_price', [0, 500000]);
                        break;
                    case '500000-2000000':
                        $products = $products->whereBetween('selling_price', [500000,2000000]);
                        break;
                    case '2000000-5000000':
                        $products = $products->whereBetween('selling_price', [2000000,5000000]);
                        break;
                    case '>5000000':
                        $products = $products->whereBetween('selling_price', [5000000,100000000]);
                        break;
                }
            }
            if ($request->category_product) {


                $products = $products->whereIn('subsubCategory_id', $request->category_product);
            }

            if ($request->sort_by) {
                switch ($request->sort_by) {
                    case 'a-z':
                        $products = $products->orderBy('product_name_vn', 'ASC');
                        break;
                    case 'z-a':
                        $products = $products->orderBy('product_name_vn', 'DESC');
                        break;
                    case 'high-low':
                        $products = $products->orderBy('selling_price', 'DESC');
                        break;
                    case 'low-high':
                        $products = $products->orderBy('selling_price', 'ASC');
                        break;
                }
            }
            if($request->show) {
                $paginate = $request->show;
            }
            else {
                $paginate = 3;
            }


            $products = $products->where('status', 1)->paginate($paginate);
            $products_list = view('frontend.shop.listProduct', compact('products'))->render();
            $products_grid = view('frontend.shop.gridProduct', compact('products'))->render();
            $product_paginate = view('frontend.shop.paginateProduct', compact('products'))->render();
            return json_encode(['products_list'=>$products_list, 'products_grid'=>$products_grid, 'product_paginate'=>$product_paginate]);

        }
        return view('frontend.subCategory.view', compact('products', 'categories','brands', 'subcategory'));
    }
    public function showProductSubSubCategory(Request $request, $id) {
        $subsubCategory = SubSubCategory::findOrFail($id);
        $categories =  Category::orderBy('category_name_vn', 'ASC')->get();
        $brands = Brand::orderBy('brand_name_vn', 'ASC')->get();
        $products = Product::where('subsubCategory_id', $id)->where('status',1)->paginate(3);

            //load product ajax
            if($request->ajax()) {
                $products = Product::query();
                $products = $products->where('subsubCategory_id', $id);
                if ($request->sort_price) {
                    switch ($request->sort_price) {
                        case '<500000':
                            $products = $products->whereBetween('selling_price', [0, 500000]);
                            break;
                        case '500000-2000000':
                            $products = $products->whereBetween('selling_price', [500000,2000000]);
                            break;
                        case '2000000-5000000':
                            $products = $products->whereBetween('selling_price', [2000000,5000000]);
                            break;
                        case '>5000000':
                            $products = $products->whereBetween('selling_price', [5000000,100000000]);
                            break;
                    }
                }

                if ($request->sort_by) {
                    switch ($request->sort_by) {
                        case 'a-z':
                            $products = $products->orderBy('product_name_vn', 'ASC');
                            break;
                        case 'z-a':
                            $products = $products->orderBy('product_name_vn', 'DESC');
                            break;
                        case 'high-low':
                            $products = $products->orderBy('selling_price', 'DESC');
                            break;
                        case 'low-high':
                            $products = $products->orderBy('selling_price', 'ASC');
                            break;
                    }
                }
                if($request->show) {
                    $paginate = $request->show;
                }
                else {
                    $paginate = 3;
                }


                $products = $products->where('status', 1)->paginate($paginate);
                $products_list = view('frontend.shop.listProduct', compact('products'))->render();
                $products_grid = view('frontend.shop.gridProduct', compact('products'))->render();
                $product_paginate = view('frontend.shop.paginateProduct', compact('products'))->render();
                return json_encode(['products_list'=>$products_list, 'products_grid'=>$products_grid, 'product_paginate'=>$product_paginate]);

            }


        return view('frontend.subsubCategory.view', compact('products', 'categories','brands','subsubCategory'));
    }
    public function getProductModal($id) {
     $product = Product::with('category','brand')->findOrFail($id);
     $product_colors = explode(',', $product->product_color_vn);
     $product_sizes = explode(',', $product->product_size_vn);
     $data['product'] = $product;
     $data['product_colors'] = $product_colors;
     $data['product_sizes'] = $product_sizes;
    return json_encode($data);
    }


    public function searchProduct(Request $request) {
        $search =  $request->product_search;
        $categories =  Category::orderBy('category_name_vn', 'ASC')->get();
        $brands = Brand::orderBy('brand_name_vn', 'ASC')->get();
        $products = Product::where('status',1)->where('product_name_vn', 'LIKE', "%$search%")->paginate(3);
        return view('frontend.product.listProductSearch', compact('products', 'categories','brands'));
    }
    public function showProductSearch(Request $request) {
        $search =  $request->value;
        $products = Product::where('status',1)->where('product_name_vn', 'LIKE', "%$search%")->take(5)->get();
     return   json_encode($products);

    }
}
