<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Rating;
use App\Models\Review;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
    public function saveReview(Request $request) {
        $request->validate([
            'comment'=>'required',
        ]);

        Review::insert([
            'user_id'=>Auth::id(),
            'product_id'=>$request->product_id,
            'comment'=>$request->comment,
            'created_at'=>Carbon::now()
        ]);
        $notification = array(
            'message'=>'Thêm đánh giá sản phẩm thành công',
            'type'=>'success'
        );
        return redirect()->back()->with($notification);
    }

    public function loadComment(Request $request) {
        if($request->offset_get >= -3 && $request->offset_get <3) {
            $reviews = Review::where('status',1)->where('product_id', $request->product_id)->with('user')->limit($request->offset_get+3)->get();
        }

        else {
$reviews = Review::where('status',1)->where('product_id', $request->product_id)->with('user')->skip($request->offset_get)->limit(3)->get();
        }



        $reviews = $reviews->toArray();
        $reviews = array_reverse($reviews);

$html = '';
        foreach($reviews as $review) {
            $img = !empty($review['user']['profile_photo_path']) ? url('upload/user_profile/'.$review['user']['profile_photo_path']) : url('upload/user3-128x128.jpg');
            $name = $review['user']['name'];
            $comment = $review['comment'];
            $html .= "<div class='reviews'>
                        <div class='review'>
                            <div class='row' style='margin-bottom:5px;'>
                                <div class='col-sm-3'>
                                    <img width='30' height='30' src='$img' alt=''>
                                    <b>$name</b>
                                </div>
                                <div class='col-sm-9'>

                                </div>
                            </div>
                            <div class='text'>$comment</div>
                        </div>
                    </div>";
       }
       return $html;
    }
    public function loadRating(Request $request) {
        $count = count(Rating::where('product_id', $request->product_id)->get());
        if($count > 0) {
            $totalRating = Rating::where('product_id', $request->product_id)->sum('rating');
            $avgRating = round($totalRating / $count,2);
            $roundRating = round($avgRating);
            $count_rating_5 = count(Rating::where('product_id', $request->product_id)->where('rating',5)->get());
            $count_rating_4 = count(Rating::where('product_id', $request->product_id)->where('rating',4)->get());
            $count_rating_3 = count(Rating::where('product_id', $request->product_id)->where('rating',3)->get());
            $count_rating_2 = count(Rating::where('product_id', $request->product_id)->where('rating',2)->get());
            $count_rating_1 = count(Rating::where('product_id', $request->product_id)->where('rating',1)->get());
        }
        else {

            $avgRating = 0;
            $roundRating = 0;
            $count_rating_5 = 0;
            $count_rating_4 = 0;
            $count_rating_3 = 0;
            $count_rating_2 = 0;
            $count_rating_1 = 0;
        }

return view('frontend.rating.load_rating', compact('count','avgRating','roundRating','count_rating_5','count_rating_4','count_rating_3','count_rating_2','count_rating_1'));

    }
    public function ratingStar(Request $request) {
        Rating::insert([
            'product_id'=>$request->product_id,
            'user_id'=>Auth::id(),
            'rating'=>$request->index,
            'created_at'=>Carbon::now()
        ]);
        return true;
    }
}
