<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\PostCategory;
use Illuminate\Http\Request;

class HomePostController extends Controller
{
public function showListBlogs() {
    $blogs = Post::where('status',1)->latest()->paginate(5);
    $blogCategories = PostCategory::latest()->get();
    return view('frontend.blog.listBlog', compact('blogs','blogCategories'));
}
public function detailBlog($slug) {
    $blog = Post::where('status',1)->where('post_slug_vn',$slug)->latest()->first();
    $blogCategories = PostCategory::latest()->get();
    return view('frontend.blog.detailBlog', compact('blog','blogCategories'));
}
public function showListBlogsCategory ($slug_category) {

    $postCategory = PostCategory::where('post_category_slug_vn', $slug_category)->first();
    $blogs = Post::where('status',1)->where('post_category_id',$postCategory->id)->latest()->paginate(5);
    $blogCategories = PostCategory::latest()->get();
    return view('frontend.blog.listBlogCategory', compact('blogs','blogCategories','postCategory'));
}
public function showListBlogTag($tag) {
    $blogs = Post::where('status',1)->where('post_tag',$tag)->latest()->paginate(5);
    $blogCategories = PostCategory::latest()->get();
    return view('frontend.blog.listBlogTag', compact('blogs','blogCategories'));

}

public function loadMore(Request $request)
{
    $posts = Post::paginate(10);
    $data = '';
    if ($request->ajax()) {
        foreach ($posts as $post) {
            $data.='<li>'.'Name:'.' <strong>'.$post->post_title_vn.'</strong></li>';
        }
        return $data;
    }

    return view('frontend.test',compact('posts'));
}
}
