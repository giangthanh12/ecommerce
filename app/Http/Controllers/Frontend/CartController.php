<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Jobs\jobSendMail;
use App\Mail\OrderMail;
use App\Models\Coupon;
use App\Models\DetailOrder;
use App\Models\District;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Province;
use App\Models\User;
use App\Models\Wards;
use Carbon\Carbon;
use Exception;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{
public function addToCart($id, Request $request) {
    if(Session::has('coupon')) {
        Session::forget('coupon');
    }
    $product = Product::findOrFail($id);
    if ($product->discount_price == null) {
        $product_price = $product->selling_price;
    } else {
        $product_price = $product->discount_price;
    }
 Cart::add([ 'id' => $id,
                'name' => $request->product_name,
                'qty' => $request->product_qty,
                'price' => $product_price,
                'weight' => 1,
                'options' => ['image'=>$product->product_thumbnail,
                              'size' => $request->product_size,
                              'color'=>$request->product_color],
            ]);
            return response()->json(['success' => 'Thêm sản phẩm giỏ hàng thành công']);
}
public function getDataCart() {
    $data['dataCart'] = Cart::content();
    $data['total'] = Cart::total();
    $data['count'] =Cart::count();
    return json_encode($data);
}
public function deleteDataCart($rowId) {
    if(Session::has('coupon')) {
        Session::forget('coupon');
    }
    Cart::remove($rowId);
    return response()->json(['success' => 'Xóa sản phẩm giỏ hàng thành công']);
}


public function showCart() {

    return view('frontend.cart.show');
}


// start get list cart ajax
public function getListCart() {
    $data['listcart'] = Cart::content();
    return json_encode($data);
}
// end get list cart ajax

// Increase qty cart ajax
public function increaseQty($rowId) {
    $cart = Cart::get($rowId);
    Cart::update($rowId,$cart->qty + 1);
    if(Session::has('coupon')) {
        $coupon_name = Session::get('coupon')['coupon_name'];
        $coupon = Coupon::where('coupon_name_vn',$coupon_name)->first();
        Session::put('coupon', [
            'coupon_name'=>$coupon->coupon_name_vn,
            'coupon_discount'=>$coupon->coupon_discount,
            'ammount_discount'=> round(Cart::total() * $coupon->coupon_discount / 100),
            'grand_total'=> Cart::total() - round(Cart::total() * $coupon->coupon_discount / 100),
        ]);
    }
    return response()->json(['successIncreaseQty']);
}


public function decreaseQty($rowId) {
    $cart = Cart::get($rowId);
    $qty = $cart->qty;
    Cart::update($rowId,$cart->qty - 1);

    if(Session::has('coupon')) {
        if($qty == 1)
            {
                Session::forget('coupon');
            }
        else {
            $coupon_name = Session::get('coupon')['coupon_name'];
            $coupon = Coupon::where('coupon_name_vn',$coupon_name)->first();
            Session::put('coupon', [
                'coupon_name'=>$coupon->coupon_name_vn,
                'coupon_discount'=>$coupon->coupon_discount,
                'ammount_discount'=> round(Cart::total() * $coupon->coupon_discount / 100),
                'grand_total'=> Cart::total() - round(Cart::total() * $coupon->coupon_discount / 100),
            ]);
        }

    }
    return response()->json(['successDecreaseQty']);
}
public function deleteProductCart($rowId) {
    if(Session::has('coupon')) {
        Session::forget('coupon');
    }
    Cart::remove($rowId);
    return response()->json(['successDeleteProductCart']);
}


// ajax coupon
public function checkCoupon(Request $request) {

$coupon = Coupon::where('coupon_name_vn', $request->coupon_name)->where('coupon_validate', '>=', Carbon::now()->format('Y-m-d'))->first();
if($coupon) {
    Session::put('coupon', [
        'coupon_name'=>$coupon->coupon_name_vn,
        'coupon_discount'=>$coupon->coupon_discount,
        'ammount_discount'=> round(Cart::total() * $coupon->coupon_discount / 100),
        'grand_total'=> Cart::total() - round(Cart::total() * $coupon->coupon_discount / 100),
    ]);

    return response()->json(['success' => 'Your coupon valid']);
}
else {
    return response()->json(['error' => 'Your coupon invalid']);
}
}

public function caculateCart() {
if(Session::has('coupon')) {
    return response()->json([
        'subtotal' => Cart::total(),
        'coupon_name'=>Session::get('coupon')['coupon_name'],
        'coupon_discount'=>Session::get('coupon')['coupon_discount'],
        'ammount_discount'=> Session::get('coupon')['ammount_discount'],
        'grand_total'=> Session::get('coupon')['grand_total'],
    ]);
}
else {
    return response()->json(['total' => Cart::total()]);
}
}
public function removeCoupon() {
    if(Session::has('coupon')) {
     Session::forget('coupon');
     return response()->json(['success' => 'Remove Coupon successfully']);
    }
}



// check out
public function showCheckout() {
    if(Auth::check()) {
        if(Cart::total() > 0) {
            $dataCart = Cart::content();
            $total = Cart::total();
            $count =Cart::count();
            $provinces = Province::orderBy('name', 'ASC')->get();
            return view('frontend.cart.showCheckout', compact('dataCart', 'total', 'count','provinces'));
        }
        else {
            $notification = array(
                'type'=>'info',
                'message'=>'Giỏ hàng của bạn đang trống, tiến hành mua sắm ngay.'
            );
            return redirect('/')->with($notification);
        }
    }
    else {
        $notification = array(
            'type'=>'error',
            'message'=>'Bạn chưa đăng nhập, tiến hành đăng nhập ngay.'
        );
        return redirect()->route('login')->with($notification);
    }
}
public function getDistrict($matp) {
    $districts = District::where('matp', $matp)->orderBy('name','ASC')->get();
     return json_encode($districts);
}

public function getWards($maqh) {
    $wards = Wards::where('maqh', $maqh)->orderBy('name','ASC')->get();
     return json_encode($wards);
}
public function saveCheckout(Request $request) {
    $total = Cart::total();
    $payment_method = $request->payment_method;
    $data['matp'] = $request->matp;
    $data['maqh'] = $request->maqh;
    $data['maxa'] = $request->maxa;
    $data['notes'] = $request->notes;
    $data['shipping_name'] = $request->shipping_name;
    $data['shipping_email'] = $request->shipping_email;
    $data['shipping_phone'] = $request->shipping_phone;
    $data['post_code'] = $request->post_code;
    return view('frontend.cart.showPayment', compact('total', 'payment_method', 'data'));
    }
public function storeCheckout(Request $request) {
    if(Session::has('coupon')) {
        $amount = Session::get('coupon')['grand_total'];
    }
    else {
        $amount = Cart::total();
    }
    $request->payment_method === 'home' ? $payment_type = 'Tiền mặt' : $payment_type = 'Card';
    if($request->payment_method === 'home' || $request->payment_method === 'vnpay') {
        $currency = 'VND';
    }
    else {
        $currency = 'USD';
    }




if($request->payment_method == 'momo') {
    header('Content-type: text/html; charset=utf-8');
    function execPostRequest($url, $data)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data))
        );
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        //execute post
        $result = curl_exec($ch);
        //close connection
        curl_close($ch);
        return $result;
    }
    session([
            'user_id'=>Auth::id(),
            'matp'=>$request->matp,
            'maqh'=>$request->maqh,
            'maxa'=>$request->maxa,
            'name'=>$request->name,
            'email'=>$request->email,
            'phone'=>$request->phone,
            'post_code'=>$request->post_code,
            'notes'=>$request->notes,
            'payment_type'=>'online',
            'payment_method'=>'MOMO',
            'currency'=>'VND',
            'amount'=>$amount,
            'order_number'=>uniqid(),
            'order_date'=>Carbon::now()->format('d F Y'),
            'order_month'=>Carbon::now()->format('F'),
            'order_year'=>Carbon::now()->format('Y'),
            'status'=>'Pending',
            'created_at'=>Carbon::now()
]);


    $endpoint = "https://test-payment.momo.vn/v2/gateway/api/create";
    $partnerCode = 'MOMOBKUN20180529';
    $accessKey = 'klm05TvNBzhg7h7j';
    $secretKey = 'at67qH6mk8w5Y1nAyMoYKMWACiEi2bsa';
    $orderInfo = "Thanh toán qua MoMo";

    $orderId = uniqid();
    $redirectUrl = url('dashboard');
    $ipnUrl = url('dashboard');

    $extraData = "";

        $requestId = time() . "";
        $requestType = "payWithATM";
        $extraData = $extraData;
        //before sign HMAC SHA256 signature
        $rawHash = "accessKey=" . $accessKey . "&amount=" . $amount . "&extraData=" . $extraData . "&ipnUrl=" . $ipnUrl . "&orderId=" . $orderId . "&orderInfo=" . $orderInfo . "&partnerCode=" . $partnerCode . "&redirectUrl=" . $redirectUrl . "&requestId=" . $requestId . "&requestType=" . $requestType;
        $signature = hash_hmac("sha256", $rawHash, $secretKey);
        $data = array('partnerCode' => $partnerCode,
            'partnerName' => "Test",
            "storeId" => "MomoTestStore",
            'requestId' => $requestId,
            'amount' => $amount,
            'orderId' => $orderId,
            'orderInfo' => $orderInfo,
            'redirectUrl' => $redirectUrl,
            'ipnUrl' => $ipnUrl,
            'lang' => 'vi',
            'extraData' => $extraData,
            'requestType' => $requestType,
            'signature' => $signature);
        $result = execPostRequest($endpoint, json_encode($data));
        $jsonResult = json_decode($result, true);  // decode json
        //Just a example, please check more in there
        return redirect($jsonResult['payUrl']);


}
else {
    DB::beginTransaction();
    try {
        // Thêm order
        $order_id = Order::insertGetId([
            'user_id'=>Auth::id(),
            'matp'=>$request->matp,
            'maqh'=>$request->maqh,
            'maxa'=>$request->maxa,
            'name'=>$request->name,
            'email'=>$request->email,
            'phone'=>$request->phone,
            'post_code'=>$request->post_code,
            'notes'=>$request->notes,
            'payment_type'=>$payment_type,
            'payment_method'=>$payment_type,
            // 'transaction_id'=>$request->emai,
            'currency'=>$currency,
            'amount'=>$amount,
            'order_number'=>uniqid(),
            'invoice_no'=>'TYUQ'.mt_rand(10000000,99999999),
            'order_date'=>Carbon::now()->format('d F Y'),
            'order_month'=>Carbon::now()->format('F'),
            'order_year'=>Carbon::now()->format('Y'),
            'status'=>'Pending',
            'created_at'=>Carbon::now()
        ]);
        // thêm chi tiết sản phẩm trong order
        foreach( Cart::content() as $item) {
            DetailOrder::insert([
                'order_id'=>$order_id,
                'product_id'=>$item->id,
                'color'=>$item->options->color,
                'size'=>$item->options->size,
                'qty'=>$item->qty,
                'price'=>$item->price,
                'created_at'=>Carbon::now()
            ]);
        }
        //thực hiện gửi email
        $order = Order::findOrFail($order_id);
        $data['dataCart'] = Cart::content();
        $data['total'] = Cart::total();
        $data['order_number'] = $order->order_number;
        $data['order_date'] = $order->order_date;
        $data['invoice_no'] = $order->invoice_no;
        $data['notes'] = $order->notes;
        $data['email'] = $order->email;
        dispatch(new jobSendMail($data));
        // Mail::to($order->email)->send(new OrderMail($data));

        DB::commit();
    } catch (Exception $e) {
        DB::rollBack();
      throw new Exception($e->getMessage());
      $notification = array(
          'type' => 'success',
          'message'=>'Có lỗi xảy ra bạn vui lòng thanh toán lại. Cảm ơn.'
      );
        return redirect()->back()->with($notification);
    }

    if(Session::has('coupon')) {
        Session::forget('coupon');
    }
    Cart::destroy();
    $notification = array(
        'type'=>'success',
        'message'=>'Bạn đã đặt đơn hàng thành công.'
    );
    return redirect('dashboard')->with($notification);
}

}
}
