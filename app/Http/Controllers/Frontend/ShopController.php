<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ShopController extends Controller
{
public function showProduct(Request $request) {

    if($request->ajax()) {
        $products = Product::query();
        if ($request->sort_price) {
            switch ($request->sort_price) {
                case '<500000':
                    $products = $products->whereBetween('selling_price', [0, 500000]);
                    break;
                case '500000-2000000':
                    $products = $products->whereBetween('selling_price', [500000,2000000]);
                    break;
                case '2000000-5000000':
                    $products = $products->whereBetween('selling_price', [2000000,5000000]);
                    break;
                case '>5000000':
                    $products = $products->whereBetween('selling_price', [5000000,100000000]);
                    break;
            }
        }
        if ($request->category_product) {


            $products = $products->whereIn('category_id', $request->category_product);
        }

        if ($request->sort_by) {
            switch ($request->sort_by) {
                case 'a-z':
                    $products = $products->orderBy('product_name_vn', 'ASC');
                    break;
                case 'z-a':
                    $products = $products->orderBy('product_name_vn', 'DESC');
                    break;
                case 'high-low':
                    $products = $products->orderBy('selling_price', 'DESC');
                    break;
                case 'low-high':
                    $products = $products->orderBy('selling_price', 'ASC');
                    break;
            }
        }
        if($request->show) {
            $paginate = $request->show;
        }
        else {
            $paginate = 3;
        }


        $products = $products->where('status', 1)->paginate($paginate);
        $products_list = view('frontend.shop.listProduct', compact('products'))->render();
        $products_grid = view('frontend.shop.gridProduct', compact('products'))->render();
        $product_paginate = view('frontend.shop.paginateProduct', compact('products'))->render();
        return json_encode(['products_list'=>$products_list, 'products_grid'=>$products_grid, 'product_paginate'=>$product_paginate]);

    }






    $products = Product::where('status', 1)->paginate(3);
    $categories =  Category::orderBy('category_name_vn', 'ASC')->get();
    $brands = Brand::orderBy('brand_name_vn', 'ASC')->get();

return view('frontend.shop.showProduct', compact('products', 'categories','brands'));
}

    // $a = $request->fullUrlWithQuery(['page' => null]);


}
