<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Slider;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Image;
use Illuminate\Support\Str;
class SliderController extends Controller
{
    public function manageSlider() {
        $sliders = Slider::latest()->get();
        return view('backend.slider.manage', compact('sliders'));
    }
    public function storeSlider(Request $request) {
        $request->validate([
            'slider_img'=>'required|image'
        ]);

        $image = $request->file('slider_img');
        $image_gen = hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
        Image::make($image)->resize(870,370)->save('upload/slider/'.$image_gen);
        $slider_image = 'upload/slider/'.$image_gen;
        Slider::insert([
            'slider_title'=>$request->slider_title,
            'slider_description'=>$request->slider_description,
            'slider_img'=>$slider_image,
            'created_at'=>Carbon::now(),
        ]);
        $notification = array(
            'message'=>'Insert Slider successfully',
            'type'=>'success'
        );
        return redirect()->back()->with($notification);
    }

    public function activeSlider($id) {
        Slider::findOrFail($id)->update(['status'=>1]);
        $notification = array(
            'message'=>'Update status slider successfully',
            'type'=>'info'
        );
        return redirect()->back()->with($notification);
    }
    public function inactiveSlider($id) {
        Slider::findOrFail($id)->update(['status'=>0]);
        $notification = array(
            'message'=>'Update status slider successfully',
            'type'=>'info'
        );
        return redirect()->back()->with($notification);
    }
    public function editSlider($id) {
        $slider = Slider::findOrFail($id);
        return view('backend.slider.edit', compact('slider'));
    }
    public function updateSlider(Request $request, $id) {
        $image = $request->file('slider_img');
        if($image) {
            $img_old = Slider::findOrFail($id)->slider_img;
            unlink($img_old);
            $image_gen = hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
            Image::make($image)->resize(870,370)->save('upload/slider/'.$image_gen);
            $slider_image = 'upload/slider/'.$image_gen;
            Slider::findOrFail($id)->update([
                'slider_title'=>$request->slider_title,
                'slider_description'=>$request->slider_description,
                'slider_img'=>$slider_image,
                'updated_at'=>Carbon::now(),
            ]);
        }
        else {
            Slider::findOrFail($id)->update([
                'slider_title'=>$request->slider_title,
                'slider_description'=>$request->slider_description,
                'updated_at'=>Carbon::now(),
            ]);
        }
        $notification = array(
            'message'=>'Update  slider successfully',
            'type'=>'info'
        );
        return redirect()->route('Slider.manage')->with($notification);

    }
    public function deleteSlider($id) {
        $slider = Slider::findOrFail($id);
        unlink($slider->slider_img);
        $slider->delete();
        $notification = array(
            'message'=>'Deleting  slider successfully',
            'type'=>'info'
        );
        return redirect()->route('Slider.manage')->with($notification);
    }
}
