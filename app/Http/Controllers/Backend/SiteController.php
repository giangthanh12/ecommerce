<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\SiteSetting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Image;
class SiteController extends Controller
{
  public function manageSite() {
        $site = SiteSetting::find(1);
        return view('backend.setting.site', compact('site'));
  }
  public function updateSite(Request $request, $id) {

    $site = SiteSetting::findOrFail($id);
    $img_old = $site->logo;
    $logo = $site->logo;
    if($request->file('logo')) {
        if($logo != null) unlink(public_path($img_old));
    $image = $request->file('logo');
    $image_gen = hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
    Image::make($image)->resize(139,36)->save('upload/logo/'.$image_gen);
    $logo = 'upload/logo/'.$image_gen;
    }

    $site->update([
        'logo'=>$logo,
        'phone_one'=>$request->phone_one,
        'phone_two'=>$request->phone_two,
        'email'=>$request->email,
        'company_name'=>$request->company_name,
        'company_address'=>$request->company_address,
        'facebook'=>$request->facebook,
        'twitter'=>$request->twitter,
        'linkedin'=>$request->linkedin,
        'youtube'=>$request->youtube,
        'created_at'=>Carbon::now(),
        'updated_at'=>Carbon::now(),
    ]);
    $notification = array(
        'message'=>'Update Site successfully',
        'type'=>'success'
    );
    return redirect()->back()->with($notification);
  }
}
