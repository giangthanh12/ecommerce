<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\module;
use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Image;
class ModuleController extends Controller
{
    public function show() {

      $modules = module::all();

      return view('backend.module.show', compact('modules'));
    }
    public function store(Request $request) {
        module::insert([
            'name_module'=>$request->name_module,
            'created_at'=>Carbon::now()
        ]);
        $notification = array(
            'type'=>'success',
            'message'=>'Thêm module thành công',
        );
        return redirect()->back()->with($notification);
    }
    public function addAdmin() {
        $modules = module::all();
        return view('backend.module.addAdmin', compact('modules'));
    }
    public function storeAdmin(Request $request) {
        $modules = module::all();
        $image = $request->file('profile_photo_path');
        $image_gen = hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
        Image::make($image)->resize(225,225)->save('upload/admin_profile/'.$image_gen);
        $profile_photo_path = 'upload/admin_profile/'.$image_gen;
        $admin_id = Admin::insertGetId([
            'name'=>$request->name,
            'email'=>$request->email,
            'phone'=>$request->phone,
            'password'=>Hash::make($request->password),
            'profile_photo_path'=>$profile_photo_path,
            'type'=>2,
            'created_at'=>Carbon::now()

        ]);

        foreach($modules as $module) {
            $module_id = $module->id;
            Role::insert([
                'admin_id'=>$admin_id,
                'module_id'=>$module->id,
                'readRole'=>$request->input("read$module_id"),
                'createRole'=>$request->input("create$module_id"),
                'editRole'=>$request->input("edit$module_id"),
                'deleteRole'=>$request->input("delete$module_id"),
                'created_at'=>Carbon::now()
            ]);
        }
        $notification = array(
            'message'=>'Thêm thành viên quản trị thành công',
            'type'=>'success'
        );
        return redirect()->back()->with($notification);
    }
    public function manageAdmin() {
        $admins = Admin::all();
        return view('backend.module.manageAdmin', compact('admins'));
    }
    public function editAdmin($id) {
        $admin = Admin::findOrFail($id);
        $modules = module::all();
        return view('backend.module.editAdmin', compact('admin','modules'));
    }
    public function updateAdmin($id, Request $request) {

        $modules = module::all();
        $admin = Admin::findOrFail($id);

        $image = $request->file('profile_photo_path');
        if($image) {
            unlink(public_path($admin->profile_photo_path));
            $image_gen = hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
            Image::make($image)->resize(225,225)->save('upload/admin_profile/'.$image_gen);
            $profile_photo_path = 'upload/admin_profile/'.$image_gen;
        }
        else {
            $profile_photo_path = $admin->profile_photo_path;
        }

        if($request->password) {
            $password = $request->password;
        }
        else {
            $password = $admin->password;
        }

        $admin_id = Admin::findOrFail($id)->update([
            'name'=>$request->name,
            'email'=>$request->email,
            'phone'=>$request->phone,
            'password'=>Hash::make($request->password),
            'profile_photo_path'=>$profile_photo_path,
            'created_at'=>Carbon::now()

        ]);
        // Trước khi cấp quyền xóa tất cả các quyền của user đó đi
        if($admin->module) {
            $admin->module()->detach();
        }
        // cập nhật lại quyền
        foreach($modules as $module) {
            $module_id = $module->id;
            Role::insert([
                'admin_id'=>$id,
                'module_id'=>$module->id,
                'readRole'=>$request->input("read$module_id"),
                'createRole'=>$request->input("create$module_id"),
                'editRole'=>$request->input("edit$module_id"),
                'deleteRole'=>$request->input("delete$module_id"),
                'created_at'=>Carbon::now()
            ]);
        }
        $notification = array(
            'message'=>'Thêm thành viên quản trị thành công',
            'type'=>'success'
        );
        return redirect()->back()->with($notification);
    }
}
