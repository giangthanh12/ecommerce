<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\PostCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PostCategoryController extends Controller
{
public function managePostCateogry() {
    $categoryPosts = PostCategory::orderBy('id', 'DESC')->get();
    return view('backend.postCategory.manage', compact('categoryPosts'));
}
public function storeCategory(Request $request) {

    $request->validate([
        'post_category_name_vn'=>'required',
        'post_category_name_en'=>'required',
    ]);
    PostCategory::insert([
        'post_category_name_vn'=>$request->post_category_name_vn,
        'post_category_name_en'=>$request->post_category_name_en,
        'post_category_slug_vn'=>Str::slug($request->post_category_name_vn),
        'post_category_slug_en'=>Str::slug($request->post_category_name_en),
        'created_at' => Carbon::now()

    ]);
    $notification = array(
        'message'=>'Insert Category Post successfully',
        'type'=>'success'
    );
    return redirect()->back()->with($notification);
}
public function editCategory($id) {
    $PostCategory = PostCategory::findOrFail($id);
    return view('backend.postCategory.edit', compact('PostCategory'));
}
public function updateCategory(Request $request, $id) {

    $request->validate([
        'post_category_name_vn'=>'required',
        'post_category_name_en'=>'required',
    ]);
    PostCategory::findOrFail($id)->update([
        'post_category_name_vn'=>$request->post_category_name_vn,
        'post_category_name_en'=>$request->post_category_name_en,
        'post_category_slug_vn'=>Str::slug($request->post_category_name_vn),
        'post_category_slug_en'=>Str::slug($request->post_category_name_en),
        'updated_at' => Carbon::now()

    ]);
    $notification = array(
        'message'=>'Update Category Post successfully',
        'type'=>'success'
    );
    return redirect()->route('PostCategory.manage')->with($notification);
}
public function deleteCategory($id) {
    $PostCategory = PostCategory::findOrFail($id);
    $PostCategory->delete();
    $notification = array(
        'message'=>'Delete Category Post successfully',
        'type'=>'success'
    );
    return redirect()->back()->with($notification);
}
}
