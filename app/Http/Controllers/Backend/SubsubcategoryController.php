<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\SubSubCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SubsubcategoryController extends Controller
{
    public function view_subsubCategory() {
        $categories = Category::orderBy('category_name_en', 'ASC')->get();
        $Subsubcategories = SubSubCategory::latest()->get();
        return view('backend.category.show_Subsubcategory', compact('Subsubcategories', 'categories'));
    }
    public function getSubCategories(Request $request) {

        $Subcategories = SubCategory::where('category_id', $request->category_id)->get();
        // $output = "<option  selected='' disabled>Select subcategory</option>";
        // foreach ($Subcategories as $Subcategory) {
        //     $output .= "<option value='$Subcategory->id'>$Subcategory->subCategory_name_vn</option>";
        // }

        // return $Subcategories;

        return json_encode($Subcategories); // chuyển giá trị object thành giá trị chuỗi json

    }
    public function storeSubsubCategory(Request $request) {
        $request->validate([
            'Subsubcategory_name_vn'=>'required',
            'Subsubcategory_name_en'=>'required',
            'subCategory_id'=>'required',
            'category_id'=>'required'
        ]);
        SubSubCategory::insert([
            'Subsubcategory_name_vn'=>$request->Subsubcategory_name_vn,
            'Subsubcategory_name_en'=>$request->Subsubcategory_name_en,
            'Subsubcategory_slug_vn'=>Str::slug($request->Subsubcategory_name_vn),
            'Subsubcategory_slug_en'=>Str::slug($request->Subsubcategory_name_en),
            'subCategory_id'=>$request->subCategory_id,
            'category_id'=>$request->category_id,
            'created_at'=>Carbon::now()
        ]);
        $notification = array(
            'message'=>'Insert Sub-subcategory successfully',
            'type'=>'success'
        );
        return redirect()->back()->with($notification);
    }
    public function editSubsubCategory($id) {

        $categories = Category::orderBy('category_name_en', 'ASC')->get();
        $Subsubcategory = SubSubCategory::findOrFail($id);
        $Subcategories = SubCategory::where('category_id', $Subsubcategory->category_id)->orderBy('subCategory_name_vn','ASC')->get();
        return view('backend.category.edit_Subsubcategory', compact('categories', 'Subsubcategory', 'Subcategories'));
    }
    public function updateSubsubCategory(Request $request,$id) {
        $request->validate([
            'Subsubcategory_name_vn'=>'required',
            'Subsubcategory_name_en'=>'required',
            'subCategory_id'=>'required',
            'category_id'=>'required'
        ]);
        SubSubCategory::findOrFail($id)->update([
            'Subsubcategory_name_vn'=>$request->Subsubcategory_name_vn,
            'Subsubcategory_name_en'=>$request->Subsubcategory_name_en,
            'Subsubcategory_slug_vn'=>Str::slug($request->Subsubcategory_name_vn),
            'Subsubcategory_slug_en'=>Str::slug($request->Subsubcategory_name_en),
            'subCategory_id'=>$request->subCategory_id,
            'category_id'=>$request->category_id,
            'created_at'=>Carbon::now()
        ]);
        $notification = array(
            'message'=>'Update Sub-subcategory successfully',
            'type'=>'success'
        );
        return redirect()->route('SubsubCategory.view')->with($notification);
    }
    public function deleteSubsubCategory($id) {
        SubSubCategory::findOrFail($id)->delete();
        $notification = array(
            'message'=>'Delete Sub-Subcategory successfully',
            'type'=>'success'
        );
        return redirect()->back()->with($notification);
    }
}
