<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\SubCategory;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class SubCategoryContrller extends Controller
{
  public function view_subCategory(Request $request) {
      $subCategories = SubCategory::latest()->get();
      $categories = Category::latest()->get();
      return view('backend.category.show_SubCategory', compact('subCategories', 'categories'));
  }
  public function storeSubcategory(Request $request) {
    $request->validate([
        'subCategory_name_vn'=>'required',
        'subCategory_name_en'=>'required',
        'category_id'=>'required'
    ]);
    SubCategory::insert([
        'subCategory_name_vn'=>$request->subCategory_name_vn,
        'subCategory_name_en'=>$request->subCategory_name_en,
        'subCategory_slug_vn'=>Str::slug($request->subCategory_name_vn),
        'subCategory_slug_en'=>Str::slug($request->subCategory_name_en),
        'category_id'=>$request->category_id,
        'created_at'=>Carbon::now()

    ]);
    $notification = array(
        'message'=>'Insert Subcategory successfully',
        'type'=>'success'
    );
    return redirect()->back()->with($notification);
  }
  public function editSubcategory($id) {
      $subCategory = SubCategory::findOrFail($id);
      $categories = Category::latest()->get();
      return view('backend.category.edit_SubCategory', compact('subCategory','categories'));
  }
  public function updateSubcategory(Request $request, $id) {
    $request->validate([
        'subCategory_name_vn'=>'required',
        'subCategory_name_en'=>'required',
        'category_id'=>'required'
    ]);
    SubCategory::findOrFail($id)->update([
        'subCategory_name_vn'=>$request->subCategory_name_vn,
        'subCategory_name_en'=>$request->subCategory_name_en,
        'subCategory_slug_vn'=>Str::slug($request->subCategory_name_vn),
        'subCategory_slug_en'=>Str::slug($request->subCategory_name_en),
        'category_id'=>$request->category_id,
        'created_at'=>Carbon::now()

    ]);
    $notification = array(
        'message'=>'Update Subcategory successfully',
        'type'=>'success'
    );
    return redirect()->route('SubCategory.view')->with($notification);
  }
  public function deleteSubcategory($id) {
    SubCategory::findOrFail($id)->delete();
    $notification = array(
        'message'=>'Delete Subcategory successfully',
        'type'=>'success'
    );
    return redirect()->back()->with($notification);
  }
}
