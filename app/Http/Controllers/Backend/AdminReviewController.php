<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Review;
use Illuminate\Http\Request;

class AdminReviewController extends Controller
{
    public function manageReview() {
        $reviews = Review::latest()->get();
        return view('backend.review.manage', compact('reviews'));
    }
    public function active($id) {
        Review::findOrFail($id)->update(['status'=>1]);
        $notification = array(
            'message'=>'Update status Review successfully',
            'type'=>'info'
        );
        return redirect()->back()->with($notification);
    }
    public function inactive($id) {
        Review::findOrFail($id)->update(['status'=>0]);
        $notification = array(
            'message'=>'Update status Review successfully',
            'type'=>'info'
        );
        return redirect()->back()->with($notification);
    }
    public function delete($id) {
        Review::findOrFail($id)->delete();
        $notification = array(
            'message'=>'Deleting Review successfully',
            'type'=>'info'
        );
        return redirect()->back()->with($notification);
    }

}
