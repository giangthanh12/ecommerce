<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Coupon;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CouponController extends Controller
{
 public function manageCoupon() {
     $coupons = Coupon::orderBy('id', 'DESC')->get();
     return view('backend.coupon.view', compact('coupons'));
 }
public function storeCoupon(Request $request) {
    $request->validate([
        'coupon_name_vn'=>'required',
        'coupon_discount'=>'required',
        'coupon_validate'=>'required'
    ]);

    Coupon::insert([
        'coupon_name_vn'=>$request->coupon_name_vn,
        'coupon_discount'=>$request->coupon_discount,
        'coupon_validate'=>$request->coupon_validate,
        'created_at'=> Carbon::now()
    ]);
    $notification = array(
        'message'=>'Insert Coupon successfully',
        'type'=>'success'
    );
    return redirect()->back()->with($notification);
}

public function editCoupon($id) {
    $coupon = Coupon::findOrFail($id);
    return view('backend.coupon.edit', compact('coupon'));
}
public function updateCoupon(Request $request, $id) {

    $request->validate([
        'coupon_name_vn'=>'required',
        'coupon_discount'=>'required',
        'coupon_validate'=>'required'
    ]);

    Coupon::findOrFail($id)->update([
        'coupon_name_vn'=>$request->coupon_name_vn,
        'coupon_discount'=>$request->coupon_discount,
        'coupon_validate'=>$request->coupon_validate,
        'updated_at'=> Carbon::now()
    ]);
    $notification = array(
        'message'=>'Update Coupon successfully',
        'type'=>'success'
    );
    return redirect()->route('Coupon.manage')->with($notification);
}
public function deleteCoupon($id) {
    Coupon::findOrFail($id)->delete();
    $notification = array(
        'message'=>'Delete Coupon successfully',
        'type'=>'success'
    );
    return redirect()->back()->with($notification);
}
}
