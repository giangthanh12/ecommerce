<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use Illuminate\Http\Request;
use Image;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
class BrandController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
  public function viewBrand() {

      $brands = Brand::latest()->get();
      return view('backend.brand.viewBrand', compact('brands'));
  }
  public function storeBrand(Request $request) {
      $request->validate([
          'brand_name_vn'=>'required',
          'brand_name_en'=>'required',
          'brand_image'=>'required|image'
      ]);

      $image = $request->file('brand_image');
      $image_gen = hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
      Image::make($image)->resize(300,300)->save('upload/brand/'.$image_gen);
      $brand_image = 'upload/brand/'.$image_gen;
      Brand::insert([
          'brand_name_vn'=>$request->brand_name_vn,
          'brand_slug_vn'=>Str::slug($request->brand_name_vn),
          'brand_name_en'=>$request->brand_name_en,
          'brand_slug_en'=>Str::slug($request->brand_name_en),
          'brand_image'=>$brand_image
      ]);
      $notification = array(
          'message'=>'Insert Brand successfully',
          'type'=>'success'
      );
      return redirect()->back()->with($notification);

  }
  public function editBrand($id) {
      $brand = Brand::findOrFail($id); // nếu không tìm thấy id sẽ trả về trang 404, còn nếu dùng find thì không ném ra lỗi
      return view('backend.brand.editBrand', compact('brand'));
  }
  public function updateBrand(Request $request, $id) {
      $brand_image_old = $request->brand_image_old;
      $request->validate([
        'brand_name_vn'=>'required',
        'brand_name_en'=>'required',

    ]);
    if($request->file('brand_image')) {
        unlink($brand_image_old);
        $image = $request->file('brand_image');
        $image_gen = hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
        Image::make($image)->resize(300,300)->save('upload/brand/'.$image_gen);
        $brand_image = 'upload/brand/'.$image_gen;
        Brand::findOrFail($id)->update([
            'brand_name_vn'=>$request->brand_name_vn,
            'brand_slug_vn'=>Str::slug($request->brand_name_vn),
            'brand_name_en'=>$request->brand_name_en,
            'brand_slug_en'=>Str::slug($request->brand_name_en),
            'brand_image'=>$brand_image
        ]);
    }
    else {
        Brand::findOrFail($id)->update([
            'brand_name_vn'=>$request->brand_name_vn,
            'brand_slug_vn'=>Str::slug($request->brand_name_vn),
            'brand_name_en'=>$request->brand_name_en,
            'brand_slug_en'=>Str::slug($request->brand_name_en),
        ]);
    }
    $notification = array(
        'message'=>'Update Brand successfully',
        'type'=>'success'
    );
    return redirect()->route('Brand.view')->with($notification);
  }
  public function deleteBrand($id) {
 $brand = Brand::findOrFail($id);
 unlink($brand->brand_image);
 $brand->delete();

$notification = array(
    'message'=>'Delete Brand successfully',
    'type'=>'success'
);
return redirect()->back()->with($notification);
  }
}
