<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Image;
class AdminProfileController extends Controller
{
   public function AdminProfile() {

       $adminProfile = Admin::find(Auth::user()->id);
       return view('admin.admin_profile', compact('adminProfile'));
   }
   public function editAdminProfile() {

    $dataEdit = Admin::find(Auth::user()->id);
    return view('admin.admin_editProfile', compact('dataEdit'));
   }
   public function storeAdminProfile(Request $request) {
    $admin = Admin::findOrFail(Auth::id());
    $profile_photo_path = $admin->profile_photo_path;
    $image = $request->file('profile_photo_path');
    if($image) {
    if(!empty($profile_photo_path)) {
        unlink(public_path($profile_photo_path));
    }

    $image_gen = hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
    Image::make($image)->resize(225,225)->save('upload/admin_profile/'.$image_gen);
    $profile_photo_path = 'upload/admin_profile/'.$image_gen;
    }
    $admin->profile_photo_path = $profile_photo_path;
    $admin->phone = $request->phone;
    $admin->name = $request->name;
    $admin->email = $request->email;
    $admin->save();
    $message = array(
        'message'=>'Cập nhập profile thành công.',
        'type'=>'success'
    );
    return redirect()->route('admin.profile')->with($message);
   }
   public function changePassAdmin() {

       return view('admin.admin_changePassword');
   }
   public function SavePasswordAdmin(Request $request) {
       $request->validate([
           'current_password'=>'required',
           'password'=>'required|confirmed'
       ]);

      $data = Admin::find(Auth::user()->id);
      $hashedPassword = $data->password;
      if(Hash::check($request->current_password, $hashedPassword)) {
          $data->password = Hash::make($request->password);
          $data->save();
          Auth::logout();
          return redirect()->route('admin.logout');
      }
      else {
          $notification = array(
              'message'=>'Mật khẩu hiện tại không chính xác.',
              'type'=>'error',
          );

          return redirect()->back()->with($notification);
      }

   }
   public function viewAllUser() {
       $users = User::orderBy('id', 'DESC')->get();

       return view('backend.user.view', compact('users'));
   }

}
