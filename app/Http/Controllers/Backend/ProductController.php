<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use App\Models\MultiImage;
use App\Models\Product;
use App\Models\SubCategory;
use App\Models\SubSubCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Image;
class ProductController extends Controller
{
   public function addProduct() {

    $brands = Brand::orderBy('brand_name_vn', 'ASC')->get();
    $categories = Category::orderBy('category_name_vn', 'ASC')->get();
    return view('backend.product.add', 
    ['categories'=>$categories,
     'brands'=>$brands
    ]);
   }
   public function getSubSubCategories(Request $request) {
       $subsubCategories  = SubSubCategory::where('subCategory_id', $request->subCategory_id)->orderBy('Subsubcategory_name_vn', 'ASC')->get();
       return json_encode($subsubCategories);
   }

   public function saveProduct(Request $request) {
    $image = $request->file('product_thumbnail');
    if($image) {
        $image_gen = hexdec(uniqid()).$image->getClientOriginalName();
        Image::make($image)->resize(917,1000)->save('upload/product/main_thumbnai/'.$image_gen);
        $product_thumbnail =  'upload/product/main_thumbnai/'.$image_gen;
    }
   $product_id = Product::insertGetId([
        'brand_id'=>$request->brand_id,
        'category_id'=>$request->category_id,
        'subCategory_id'=>$request->subCategory_id,
        'subsubCategory_id'=>$request->subsubCategory_id,
        'product_name_vn'=>$request->product_name_vn,
        'product_name_en'=>$request->product_name_en,
        'product_slug_vn'=>Str::slug($request->product_name_vn),
        'product_slug_en'=>Str::slug($request->product_name_en),
        'product_code'=>$request->product_code,
        'product_qty'=>$request->product_qty,
        'product_tags_vn'=>$request->product_tags_vn,
        'product_tags_en'=>$request->product_tags_en,
        'product_size_vn'=>$request->product_size_vn,
        'product_size_en'=>$request->product_size_en,
        'product_color_vn'=>$request->product_color_vn,
        'product_color_en'=>$request->product_color_en,
        'selling_price'=>$request->selling_price,
        'product_thumbnail'=>$product_thumbnail,
        'discount_price'=>$request->discount_price,
        'short_descp_vn'=>$request->short_descp_vn,
        'short_descp_en'=>$request->short_descp_vn,
        'long_descp_vn'=>$request->long_descp_vn,
        'long_descp_en'=>$request->long_descp_en,
        'hot_deals'=>$request->hot_deals,
        'featured'=>$request->featured,
        'special_offer'=>$request->special_offer,
        'special_deals'=>$request->special_deals,
        'status'=>0,
    ]);

    $images = $request->multi_image;
    if($images) {
        foreach($images as $img) {
        $img_gen = hexdec(uniqid()).$img->getClientOriginalName();
        Image::make($img)->resize(917,1000)->save('upload/product/sub_thumbnais/'.$img_gen);
        $thumbnail =  'upload/product/sub_thumbnais/'.$img_gen;
        MultiImage::insert([
            'product_id'=>$product_id,
            'photo_name'=>$thumbnail,
        ]);
        }
    }
    $notification = array(
        'message'=>'Adding Product successfully',
        'type'=>'success'
    );
    return redirect()->back()->with($notification);
   }
   public function manageProduct() {
       $products = Product::latest()->get();
       return view('backend.product.view', compact('products'));
   }
   public function editProduct($id) {
       $multiImages = MultiImage::where('product_id', $id)->latest()->get();
       $brands = Brand::orderBy('brand_name_vn', 'ASC')->get();
       $categories = Category::orderBy('category_name_vn', 'ASC')->get();
       $subCategories = SubCategory::orderBy('subCategory_name_vn', 'ASC')->get();
       $subsubCategories = SubSubCategory::orderBy('Subsubcategory_name_vn', 'ASC')->get();
       $product = Product::findOrFail($id);
       return view('backend.product.edit', compact('brands', 'categories','subCategories','subsubCategories','product', 'multiImages'));
    }
    public function updateProduct(Request $request) {
        $id = $request->product_id;
        $image = $request->file('product_thumbnail');
        $img_old = Product::findOrFail($id)->product_thumbnail;
        if($image) {

            unlink($img_old);
            $image_gen = hexdec(uniqid()).$image->getClientOriginalName();
            Image::make($image)->resize(917,1000)->save('upload/product/main_thumbnai/'.$image_gen);
            $product_thumbnail =  'upload/product/main_thumbnai/'.$image_gen;
        }
        else {
            $product_thumbnail = $img_old;
        }
        Product::findOrFail($id)->update([
        'brand_id'=>$request->brand_id,
        'category_id'=>$request->category_id,
        'subCategory_id'=>$request->subCategory_id,
        'subsubCategory_id'=>$request->subsubCategory_id,
        'product_name_vn'=>$request->product_name_vn,
        'product_name_en'=>$request->product_name_en,
        'product_slug_vn'=>Str::slug($request->product_name_vn),
        'product_slug_en'=>Str::slug($request->product_name_en),
        'product_code'=>$request->product_code,
        'product_qty'=>$request->product_qty,
        'product_tags_vn'=>$request->product_tags_vn,
        'product_tags_en'=>$request->product_tags_en,
        'product_size_vn'=>$request->product_size_vn,
        'product_size_en'=>$request->product_size_en,
        'product_color_vn'=>$request->product_color_vn,
        'product_color_en'=>$request->product_color_en,
        'selling_price'=>$request->selling_price,
        'discount_price'=>$request->discount_price,
        'product_thumbnail'=>$product_thumbnail,
        'short_descp_vn'=>$request->short_descp_vn,
        'short_descp_en'=>$request->short_descp_vn,
        'long_descp_vn'=>$request->long_descp_vn,
        'long_descp_en'=>$request->long_descp_en,
        'hot_deals'=>$request->hot_deals,
        'featured'=>$request->featured,
        'special_offer'=>$request->special_offer,
        'special_deals'=>$request->special_deals,
        'updated_at'=>Carbon::now(),
        ]);
        $notification = array(
            'message'=>'Updating Product successfully',
            'type'=>'success'
        );
        return redirect()->route('Product.manage')->with($notification);
    }

    public function updateMultiImage(Request $request) {
        $images = $request->file('multiImage');
        if(!$images) return redirect()->back();
        foreach ( $images as $key=>$img) {
                $img_del = MultiImage::findOrFail($key)->photo_name;
                unlink($img_del);
                $image_gen = hexdec(uniqid()).$img->getClientOriginalName();
                Image::make($img)->resize(917,1000)->save('upload/product/sub_thumbnais/'.$image_gen);
                MultiImage::findOrFail($key)->update([
                    'photo_name'=>'upload/product/main_thumbnai/'.$image_gen,
                    'updated_at'=>Carbon::now(),
                ]);
        }
        $notification = array(
            'message'=>'Updating Multi images successfully',
            'type'=>'info'
        );
        return redirect()->route('Product.manage')->with($notification);
    }
    public function deleteMultiImage($id) {
       $img =  MultiImage::findOrFail($id);
        unlink($img->photo_name);
        $img->delete();
        $notification = array(
            'message'=>'Deleting Multi images successfully',
            'type'=>'info'
        );
        return redirect()->back()->with($notification);

    }
    public function activeProduct($id) {
        Product::findOrFail($id)->update(['status'=>1]);
        $notification = array(
            'message'=>'Update status product successfully',
            'type'=>'info'
        );
        return redirect()->back()->with($notification);
    }
    public function inactiveProduct($id) {
        Product::findOrFail($id)->update(['status'=>0]);
        $notification = array(
            'message'=>'Update status product successfully',
            'type'=>'info'
        );
        return redirect()->back()->with($notification);
    }
    public function deleteProduct($id) {
        $product = Product::findOrFail($id);
        $multiImages = MultiImage::latest()->get();
        unlink($product->product_thumbnail);
        foreach($multiImages as $img) {
            if($img->product_id == $id) {
                unlink(public_path($img->photo_name));
            }
        }
        Product::findOrFail($id)->delete();
        $notification = array(
            'message'=>'Deleting product successfully',
            'type'=>'info'
        );
        return redirect()->back()->with($notification);
    }
}
