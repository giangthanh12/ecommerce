<?php

namespace App\Http\Controllers\Backend;

use App\Events\OrderChanged;
use App\Http\Controllers\Controller;
use App\Models\DetailOrder;
use App\Models\Order;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;
class OrderController extends Controller
{
public function managePendingOrder() {
    $orders = Order::where('status', 'pending')->latest()->get();
    return view('backend.order.pendingOrder', compact('orders'));
}
public function updateConfirmedOrder($order_id) {

    Order::findOrFail($order_id)->update([
        'status'=>'confirmed',
        'confirmed_date'=>Carbon::now()
    ]);
    $order = Order::findOrFail($order_id);
    event(new OrderChanged($order));
    $notification = array(
        'type'=>'success',
        'message'=>'Cập nhật đơn hàng thành công',
    );
    return redirect()->route('Order.managePendingOrder')->with($notification);
}
public function detailOrder($order_id) {
    $order = Order::findOrFail($order_id);
    $order_products = DetailOrder::where('order_id', $order_id)->orderBy('id', 'DESC')->get();
    return view('backend.order.detailOrder', compact('order','order_products'));
}

public function manageConfirmedOrder() {
    $orders = Order::where('status', 'confirmed')->latest()->get();
    return view('backend.order.confirmedOrder', compact('orders'));
}
public function updateProcessingOrder($order_id) {
    Order::findOrFail($order_id)->update([
        'status'=>'processing',
        'confirmed_date'=>Carbon::now()
    ]);
    $order = Order::findOrFail($order_id);
    event(new OrderChanged($order));
    $notification = array(
        'type'=>'success',
        'message'=>'Cập nhật đơn hàng thành công',
    );
    return redirect()->route('Order.manageConfirmedOrder')->with($notification);
}
public function manageProcessingOrder() {
    $orders = Order::where('status', 'processing')->latest()->get();
    return view('backend.order.processingOrder', compact('orders'));
}
public function updatePickedOrder($order_id) {
    Order::findOrFail($order_id)->update([
        'status'=>'picked',
        'picked_date'=>Carbon::now()
    ]);
    $order = Order::findOrFail($order_id);
    event(new OrderChanged($order));
    $notification = array(
        'type'=>'success',
        'message'=>'Cập nhật đơn hàng thành công',
    );
    return redirect()->route('Order.manageProcessingOrder')->with($notification);
}

public function managePickedOrder() {
    $orders = Order::where('status', 'picked')->latest()->get();
    return view('backend.order.pickedOrder', compact('orders'));
}
public function updateShippedOrder($order_id) {
    Order::findOrFail($order_id)->update([
        'status'=>'shipped',
        'shipped_date'=>Carbon::now()
    ]);
    $order = Order::findOrFail($order_id);
    event(new OrderChanged($order));
    $notification = array(
        'type'=>'success',
        'message'=>'Cập nhật đơn hàng thành công',
    );
    return redirect()->route('Order.managePickedOrder')->with($notification);
}

public function manageShippedOrder() {
    $orders = Order::where('status', 'shipped')->latest()->get();
    return view('backend.order.shippedOrder', compact('orders'));
}
public function updateDeliveredOrder($order_id) {
    Order::findOrFail($order_id)->update([
        'status'=>'delivered',
        'delivered_date'=>Carbon::now()
    ]);
    $order = Order::findOrFail($order_id);
    event(new OrderChanged($order));
    $products_order = DetailOrder::where('order_id', $order_id)->get();
    foreach ($products_order as $product) {
        Product::findOrFail($product->product_id)->update([
            'product_qty'=>DB::raw('product_qty-'.$product->qty) // truyền một chuỗi truy vấn vấn và tự cập nhật field không cần thiết phải lấy ra
        ]);
    }
    $notification = array(
        'type'=>'success',
        'message'=>'Cập nhật đơn hàng thành công',
    );
    return redirect()->route('Order.manageShippedOrder')->with($notification);
}

public function manageDeliveredOrder() {
    $orders = Order::where('status', 'delivered')->latest()->get();
    return view('backend.order.deliveredOrder', compact('orders'));
}
public function updateCancelOrder($order_id) {
    Order::findOrFail($order_id)->update([
        'status'=>'cancel',
        'cancel_date'=>Carbon::now()
    ]);
    $notification = array(
        'type'=>'success',
        'message'=>'Cập nhật đơn hàng thành công',
    );
    return redirect()->route('Order.manageDeliveredOrder')->with($notification);
}
public function managecancelOrder() {
    $orders = Order::where('status', 'cancel')->latest()->get();
    return view('backend.order.cancelOrder', compact('orders'));
}
public function invoiceOrder($order_id) {
    $order = Order::findOrFail($order_id);
    $order_products = DetailOrder::with('product')->where('order_id', $order_id)->orderBy('id', 'DESC')->get();

    $pdf = PDF::loadView('backend.order.invoiceOrder', compact('order','order_products'));

    return $pdf->download('invoice'.$order_id.'.pdf');
}


public function searchAllOrder() {
    return view('backend.order.searchAllOrder');
}
public function searchDaySave(Request $request) {
    $date = Carbon::parse($request->order_date)->format('d F Y') ;

$orders = Order::where('order_date', $date)->orderBy('id', 'DESC')->get();
return view('backend.order.allOrder', compact('orders'));

}
public function searchMonthSave(Request $request) {


$orders = Order::where('order_month', $request->order_month)->where('order_year', $request->order_year)->orderBy('id', 'DESC')->get();
return view('backend.order.allOrder', compact('orders'));
}
public function searchYearSave(Request $request) {
    $orders = Order::where('order_year', $request->order_year)->orderBy('id', 'DESC')->get();
return view('backend.order.allOrder', compact('orders'));
}
public function manageReturnOrder() {
    $orders = Order::where('return_reason','!=', Null)->orderBy('order_date', 'DESC')->get();
    return view('backend.order.returnOrder', compact('orders'));
}
public function returnSuccess($id) {
    $order = Order::findOrFail($id);
    $order->update([
        'return_order'=>2,
        'status'=>'cancel'
    ]);
    $notification = array(
        'type'=>'success',
        'message'=>'Cập nhật đơn hàng thành công',
    );
    return redirect()->back()->with($notification);
}
}
