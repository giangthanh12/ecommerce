<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\PostCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Image;
use Illuminate\Support\Str;
class PostController extends Controller
{
    public function manage() {
        $posts= Post::latest()->get();
        return view('backend.post.manage', compact('posts'));
    }
    public function add() {
        $categories = PostCategory::latest()->get();
        return view('backend.post.add', compact('categories'));
    }
    public function store(Request $request) {
        $request->validate([
            'post_title_vn'=>'required',
            'post_title_en'=>'required',
            'post_image'=>'required|image',
            'post_detail_vn'=>'required',
            'post_detail_en'=>'required',
            'post_category_id'=>'required',
            'post_tag'=>'required'
        ]);

        $image = $request->file('post_image');
        $image_gen = hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
        Image::make($image)->resize(780,433)->save('upload/post/'.$image_gen);
        $post_image = 'upload/post/'.$image_gen;
        Post::insert([
            'post_title_vn'=>$request->post_title_vn,
            'post_slug_vn'=>Str::slug($request->post_title_vn),
            'post_title_en'=>$request->post_title_en,
            'post_slug_en'=>Str::slug($request->post_title_en),
            'post_tag'=>$request->post_tag,
            'post_image'=>$post_image,
            'post_category_id'=>$request->post_category_id,
            'post_detail_vn'=>$request->post_detail_vn,
            'post_detail_en'=>$request->post_detail_en,

            'created_at'=>Carbon::now()
        ]);
        $notification = array(
            'message'=>'Insert Post successfully',
            'type'=>'success'
        );
        return redirect()->route('Post.manage')->with($notification);
    }
    public function edit($id) {
        $categories = PostCategory::latest()->get();
            $post = Post::findOrFail($id);
            return view('backend.post.edit', compact('categories', 'post'));
    }
    public function update(Request $request) {
        $id = $request->post_id;
        $image = $request->file('post_image');
        $img_old = Post::findOrFail($id)->post_image;
        if($image) {
            unlink(public_path($img_old));
            $image_gen = hexdec(uniqid()).$image->getClientOriginalName();
            Image::make($image)->resize(780,433)->save('upload/post/'.$image_gen);
            $post_image =  'upload/post/'.$image_gen;
        }
        else {
            $post_image = $img_old;
        }
        Post::findOrFail($id)->update([
            'post_title_vn'=>$request->post_title_vn,
            'post_slug_vn'=>Str::slug($request->post_title_vn),
            'post_title_en'=>$request->post_title_en,
            'post_slug_en'=>Str::slug($request->post_title_en),
            'post_tag'=>$request->post_tag,
            'post_image'=>$post_image,
            'post_category_id'=>$request->post_category_id,
            'post_detail_vn'=>$request->post_detail_vn,
            'post_detail_en'=>$request->post_detail_en,
            'updated_at'=>Carbon::now(),
            ]);

        $notification = array(
            'message'=>'Update Post successfully',
            'type'=>'success'
        );
        return redirect()->route('Post.manage')->with($notification);


    }
    public function active($id) {
        Post::findOrFail($id)->update(['status'=>1]);
        $notification = array(
            'message'=>'Update status post successfully',
            'type'=>'info'
        );
        return redirect()->back()->with($notification);
    }
    public function inactive($id) {
        Post::findOrFail($id)->update(['status'=>0]);
        $notification = array(
            'message'=>'Update status post successfully',
            'type'=>'info'
        );
        return redirect()->back()->with($notification);
    }
    public function delete($id) {
        $post = Post::findOrFail($id);
        // unlink(public_path($post->post_image));
        Post::findOrFail($id)->delete();
        $notification = array(
            'message'=>'Deleting Post successfully',
            'type'=>'info'
        );
        return redirect()->back()->with($notification);
    }
}
