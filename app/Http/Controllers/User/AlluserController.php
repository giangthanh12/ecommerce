<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\DetailOrder;
use App\Models\Order;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PDF;
class AlluserController extends Controller
{
    public function viewOrder() {

        $orders = Order::where('user_id', Auth::id())->orderBy('id','DESC')->get();

        return view('frontend.user.viewOrder', compact('orders'));

    }
    public function detailOrder($order_id) {
        $order = Order::findOrFail($order_id);
        $order_products = DetailOrder::where('order_id', $order_id)->orderBy('id', 'DESC')->get();

        return view('frontend.user.detailOrder', compact('order','order_products'));
    }
    public function invoiceOrder($order_id) {

        $order = Order::findOrFail($order_id);
        $order_products = DetailOrder::with('product')->where('order_id', $order_id)->orderBy('id', 'DESC')->get();

        $pdf = PDF::loadView('frontend.user.invoiceOrder', compact('order','order_products'));

        return $pdf->download('invoice'.$order_id.'.pdf');

    }
    public function saveReasonOrder(Request $request, $order_id) {

        $order = Order::where('user_id', Auth::id())->where('id', $order_id)->first();

        $order->update([
            'return_reason'=>$request->return_reason,
            'return_order'=>1,
            'return_date'=>Carbon::now()->format('d F Y')
        ]);
        $order->save();
        $notification = array(
            'type'=>'success',
            'message'=>'Bạn đã gửi thông tin lên hệ thống thành công.'
        );
        return redirect()->back()->with($notification);
    }
    public function viewOrderReturn() {

        $orders = Order::where('user_id', Auth::id())->where('return_reason', '!=', NULL)->orderBy('id','DESC')->get();

        return view('frontend.user.viewReturnOrder', compact('orders'));

    }
    public function searchOrder(Request $request) {
        $order = Order::where('invoice_no', $request->invoice_no)->first();
        if(!$order) {
            $notification = array(
                'type'=>'error',
                'message'=>'Mã đơn hàng không hợp lệ'
            );
            return redirect()->back()->with($notification);
        }
        else {
            return view('frontend.user.searchOrder', compact('order'));
        }
    }

}
