<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Wishlist;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WishlistContrller extends Controller
{


    function showWishlist() {
        return view('frontend.wishlist.view');
    }
  function addWishlist($id) {
    if(Auth::check()) {
        $exist = Wishlist::where('product_id', $id)->where('user_id', Auth::id())->first();
        if($exist) {
            return response()->json(['error' => 'Sản phẩm đã được thêm vào sản phẩm yêu thích.']);
        }
        Wishlist::insert([
            'user_id'=>Auth::id(),
            'product_id'=>$id,
            'created_at'=>Carbon::now()
        ]);
        return response()->json(['success' => 'Thêm sản phẩm yêu thích thành công']);
    }
    else {
        return response()->json(['error' => 'Bạn chưa đăng nhập để tiến hành thêm sản phẩm yêu thích.']);
    }
  }


  public function getWishlist() {
     $withlist = Wishlist::where('user_id', Auth::id())->with('product')->latest()->get();
    $data['withlist'] = $withlist;
    return json_encode($data);
  }
  public function deleteWishlist($id) {
      $product = Wishlist::where('user_id', Auth::id())->where('product_id', $id)->first();
      $product->delete();
      return response()->json(['success' => 'Bạn đã xóa sản phẩm thành công']);
  }
}
