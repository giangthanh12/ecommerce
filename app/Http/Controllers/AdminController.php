<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Routing\Pipeline;
use App\Actions\Fortify\AttemptToAuthenticate;
use Laravel\Fortify\Actions\EnsureLoginIsNotThrottled;
use Laravel\Fortify\Actions\PrepareAuthenticatedSession;
use App\Actions\Fortify\RedirectIfTwoFactorAuthenticatable;
use App\Http\Responses\LoginResponse;
use App\Models\Admin;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Laravel\Fortify\Contracts\LoginViewResponse;
use Laravel\Fortify\Contracts\LogoutResponse;
use Laravel\Fortify\Features;
use Laravel\Fortify\Fortify;
use Laravel\Fortify\Http\Requests\LoginRequest;
use Image;
class AdminController extends Controller
{
    /**
     * The guard implementation.
     *
     * @var \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected $guard;

    /**
     * Create a new controller instance.
     *
     * @param  \Illuminate\Contracts\Auth\StatefulGuard  $guard
     * @return void
     */

    public function __construct(StatefulGuard $guard)
    {
        $this->guard = $guard;

    }
 public function manageAdmin() {
       $admins = Admin::latest()->get();

       return view('backend.admin.view', compact('admins'));
   }
   public function addAdmin() {
       return view('backend.admin.add');
   }
    public function loginForm(){

    	return view('auth.admin_login', ['guard' => 'admin']);
    }
    public function storeAdmin(Request $request) {


        $image = $request->file('profile_photo_path');
        $image_gen = hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
        Image::make($image)->resize(225,225)->save('upload/admin_profile/'.$image_gen);
        $profile_photo_path = 'upload/admin_profile/'.$image_gen;

        Admin::insert([
            'name'=>$request->name,
            'email'=>$request->email,
            'phone'=>$request->phone,
            'password'=>Hash::make($request->password),
            'profile_photo_path'=>$profile_photo_path,
            'phone'=>$request->phone,
            'brand'=>$request->brand,
            'category'=>$request->category,
            'product'=>$request->product,
            'slider'=>$request->slider,
            'coupon'=>$request->coupon,
            'postCategory'=>$request->postCategory,
            'post'=>$request->post,
            'setting'=>$request->setting,
            'order'=>$request->order,
            'Alluser'=>$request->Alluser,
            'review'=>$request->review,
            'AllOrder'=>$request->AllOrder,
            'type'=>2,
            'created_at'=>Carbon::now()

        ]);
        $notification = array(
            'message'=>'Insert User successfully',
            'type'=>'success'
        );
        return redirect()->route('adminRole.manage')->with($notification);
    }
    public function editAdmin($id) {
        $admin = Admin::findOrFail($id);
        return view('backend.admin.edit', compact('admin'));
    }
    public function updateAdmin(Request $request, $id) {
        $admin = Admin::findOrFail($id);
        $profile_photo_path = $admin->profile_photo_path;
        $image = $request->file('profile_photo_path');
        if($image) {
        if(!empty($profile_photo_path)) {
            unlink(public_path($profile_photo_path));
        }

        $image_gen = hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
        Image::make($image)->resize(225,225)->save('upload/admin_profile/'.$image_gen);
        $profile_photo_path = 'upload/admin_profile/'.$image_gen;
        }


        Admin::findOrfail($id)->update([
            'name'=>$request->name,
            'email'=>$request->email,
            'phone'=>$request->phone,
            'password'=>Hash::make($request->password),
            'profile_photo_path'=>$profile_photo_path,
            'phone'=>$request->phone,
            'brand'=>$request->brand,
            'category'=>$request->category,
            'product'=>$request->product,
            'slider'=>$request->slider,
            'coupon'=>$request->coupon,
            'postCategory'=>$request->postCategory,
            'post'=>$request->post,
            'setting'=>$request->setting,
            'order'=>$request->order,
            'Alluser'=>$request->Alluser,
            'review'=>$request->review,
            'AllOrder'=>$request->AllOrder,
            'type'=>2,
            'created_at'=>Carbon::now()

        ]);
        $notification = array(
            'message'=>'Update User successfully',
            'type'=>'success'
        );
        return redirect()->route('adminRole.manage')->with($notification);
    }
    public function deleteAdmin($id) {
$admin = Admin::findOrFail($id);

unlink(public_path($admin->profile_photo_path));

$admin->delete();
$notification = array(
    'message'=>'Delete User successfully',
    'type'=>'success'
);
return redirect()->route('adminRole.manage')->with($notification);
    }

    /**
     * Show the login view.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Laravel\Fortify\Contracts\LoginViewResponse
     */
    public function create(Request $request): LoginViewResponse
    {
        return app(LoginViewResponse::class);
    }

    /**
     * Attempt to authenticate a new session.
     *
     * @param  \Laravel\Fortify\Http\Requests\LoginRequest  $request
     * @return mixed
     */
    public function store(LoginRequest $request)
    {
        return $this->loginPipeline($request)->then(function ($request) {
            return app(LoginResponse::class);
        });
    }

    /**
     * Get the authentication pipeline instance.
     *
     * @param  \Laravel\Fortify\Http\Requests\LoginRequest  $request
     * @return \Illuminate\Pipeline\Pipeline
     */
    protected function loginPipeline(LoginRequest $request)
    {
        if (Fortify::$authenticateThroughCallback) {
            return (new Pipeline(app()))->send($request)->through(array_filter(
                call_user_func(Fortify::$authenticateThroughCallback, $request)
            ));
        }

        if (is_array(config('fortify.pipelines.login'))) {
            return (new Pipeline(app()))->send($request)->through(array_filter(
                config('fortify.pipelines.login')
            ));
        }

        return (new Pipeline(app()))->send($request)->through(array_filter([
            config('fortify.limiters.login') ? null : EnsureLoginIsNotThrottled::class,
            Features::enabled(Features::twoFactorAuthentication()) ? RedirectIfTwoFactorAuthenticatable::class : null,
            AttemptToAuthenticate::class,
            PrepareAuthenticatedSession::class,
        ]));
    }

    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Laravel\Fortify\Contracts\LogoutResponse
     */
    public function destroy(Request $request): LogoutResponse
    {
        $this->guard->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return app(LogoutResponse::class);
    }
}

