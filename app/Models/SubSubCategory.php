<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubSubCategory extends Model
{
    use HasFactory;
protected $fillable = [
        'category_id',
        'subCategory_id',
        'Subsubcategory_name_vn',
        'Subsubcategory_name_en',
        'Subsubcategory_slug_vn',
        'Subsubcategory_slug_en'
    ];

    public function category() {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
    public function subCategory() {
        return $this->belongsTo(SubCategory::class, 'subCategory_id', 'id');
    }
}
