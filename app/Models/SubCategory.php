<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    use HasFactory;
    protected $fillable = [
        'category_id',
        'subCategory_name_vn',
        'subCategory_name_en',
        'subCategory_slug_vn',
        'subCategory_slug_en',
    ];

    public function category() {
        return $this->belongsTo(Category::class, 'category_id');
    }
    public function subsubcategory() {
        return $this->hasMany(SubSubcategory::class, 'subcategory_id');
    }
}
