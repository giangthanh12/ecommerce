<?php

namespace App\Policies;

use App\Models\Admin;
use App\Models\module;
use App\Models\Role;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class BrandPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {

    }
    public function read() {
     $admin = Auth::guard('admin')->user();
     $module_brand = module::where('name_module','brand')->first();
     $role = Role::where('admin_id', $admin->id)->where('module_id',$module_brand->id)->first();
     return $role->readRole == 1;
    }
    public function create() {
        $admin = Auth::guard('admin')->user();
        $module_brand = module::where('name_module','brand')->first();
        $role = Role::where('admin_id', $admin->id)->where('module_id',$module_brand->id)->first();
        return $role->createRole == 1;
    }
    public function update() {
    $admin = Auth::guard('admin')->user();
    $module_brand = module::where('name_module','brand')->first();
    $role = Role::where('admin_id', $admin->id)->where('module_id',$module_brand->id)->first();
    return $role->editRole == 1;
    }
    public function delete() {
    $admin = Auth::guard('admin')->user();
    $module_brand = module::where('name_module','brand')->first();
    $role = Role::where('admin_id', $admin->id)->where('module_id',$module_brand->id)->first();
    return $role->deleteRole == 1;
    }

}
