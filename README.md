Bước 1: Cài laragon (https://www.youtube.com/watch?v=A3MSmO-gQ3A), có thể dùng xampp, hoặc wampp, nhưng nên dùng laragon nhé
Bước 2. Sau khi cài laragon thì cài đặt phiên bản php. Con này đang dùng phiên bản php 7.4, laravel 8x., composer 2.0.12
Bước 3: Chạy composer install
Bước 4: Tạo file .env cấu hình setup database, chạy lệnh php artisan key:generate để tiến hành tạo key cho dự án, sau đó chạy lệnh php artisan config:cache để clear cache,
        Thay đổi gì trong file config, và .env thì đều chạy lệnh php artisan config:cache
Bước 4 Chạy dự án với domain.test ( domain là tên folder thư mục: mặc định là ecommerce.test)

