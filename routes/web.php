<?php

use App\Demo\Demo;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Backend\AdminProfileController;
use App\Http\Controllers\Backend\AdminReviewController;
use App\Http\Controllers\Backend\BrandController;
use App\Http\Controllers\Backend\CategoryController;
use App\Http\Controllers\Backend\CouponController;
use App\Http\Controllers\Backend\ModuleController;
use App\Http\Controllers\Backend\OrderController;
use App\Http\Controllers\Backend\PostCategoryController;
use App\Http\Controllers\Backend\PostController;
use App\Http\Controllers\Backend\ProductController;
use App\Http\Controllers\Backend\SiteController;
use App\Http\Controllers\Backend\SliderController;
use App\Http\Controllers\backend\SubCategoryContrller;
use App\Http\Controllers\Frontend\IndexController;
use App\Http\Controllers\Backend\SubsubcategoryController;
use App\Http\Controllers\frontend\CartController;
use App\Http\Controllers\Frontend\HomePostController;
use App\Http\Controllers\Frontend\ShopController;
use App\Http\Controllers\Frontend\ReviewController;
use App\Http\Controllers\User\AlluserController;
use App\Http\Controllers\User\WishlistContrller;
use App\Models\User;
use App\Models\Order;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use App\Mail\OrderMail;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\DB;
use App\Models\DetailOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'laravel-filemanager'], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});
Route::group(['prefix' => 'admin', 'middleware'=>['admin:admin']], function() {
    Route::get('login', [AdminController::class, 'loginForm']);
    Route::POST('login', [AdminController::class, 'store'])->name('admin.login');
});
Route::middleware(['auth:admin'])->group(function () {
    Route::get('admin/logout', [AdminController::class, 'destroy'])->name('admin.logout');
    Route::get('admin/profile', [AdminProfileController::class, 'AdminProfile'])->name('admin.profile');
    Route::get('admin/profile/edit', [AdminProfileController::class, 'editAdminProfile'])->name('adminProfile.edit');
    Route::post('admin/profile/store', [AdminProfileController::class, 'storeAdminProfile'])->name('adminProfile.store');
    Route::get('admin/profile/changePassword', [AdminProfileController::class, 'changePassAdmin'])->name('admin.changePass');
    Route::post('admin/profile/changePassword/save', [AdminProfileController::class, 'SavePasswordAdmin'])->name('adminProfile.updatePassword');
});
//all admin route




Route::middleware(['auth:sanctum,admin', 'verified'])->get('admin/dashboard', function () {
    return view('admin.index');
})->name('dashboard')->middleware('auth:admin');

Route::middleware(['auth:sanctum,web', 'verified','user','checkOnline'])->any('/dashboard', function (Request $request) {
    $id = Auth::id();
    $user = User::find($id);
    if($request->isMethod("post")) {
        dd("ok");
        DB::beginTransaction();
        try {
        $order_id = Order::insertGetId(
            [
                'user_id'=>session('user_id'),
                'matp'=>session('matp'),
                'maqh'=>session('maqh'),
                'maxa'=>session('maxa'),
                'name'=>session('name'),
                'email'=>session('email'),
                'phone'=>session('phone'),
                'post_code'=>session('post_code'),
                'notes'=>session('notes'),
                'payment_type'=>session('payment_type'),
                'payment_method'=>session('payment_method'),
                'currency'=>session('currency'),
                'amount'=>session('amount'),
                'order_number'=>$_GET['orderId'],
                'invoice_no'=>'TYUQ'.mt_rand(10000000,99999999),
                'order_date'=>Carbon::now()->format('d F Y'),
                'order_month'=>Carbon::now()->format('F'),
                'order_year'=>Carbon::now()->format('Y'),
                'status'=>'Pending',
                'created_at'=>Carbon::now()
            ]
        );
        foreach( Cart::content() as $item) {
            DetailOrder::insert([
                'order_id'=>$order_id,
                'product_id'=>$item->id,
                'color'=>$item->options->color,
                'size'=>$item->options->size,
                'qty'=>$item->qty,
                'price'=>$item->price,
                'created_at'=>Carbon::now()
            ]);
        }
        //thực hiện gửi email
        $order = Order::findOrFail($order_id);
        $data['dataCart'] = Cart::content();
        $data['total'] = Cart::total();
        $data['order_number'] = $order->order_number;
        $data['order_date'] = $order->order_date;
        $data['invoice_no'] = $order->invoice_no;
        $data['notes'] = $order->notes;
        // Mail::to($order->email)->send(new OrderMail($data));
        DB::commit();
        if(Session::has('coupon')) {
            Session::forget('coupon');
        }
        Cart::destroy();
        $notification = array(
            'type' => 'success',
            'message'=>'Bạn đã đặt hàng thành công.'
        );
          return redirect("/dashboard")->with($notification);
    } catch (Exception $e) {
        DB::rollBack();
        throw new Exception($e->getMessage());
    }
    }
    return view('dashboard', compact('user'));
})->name('dashboard');
// user profile
Route::get('/', [IndexController::class, 'index']);
Route::get('product/tag/{tag}', [IndexController::class, 'viewProductTag']);
Route::get('product/search', [IndexController::class, 'searchProduct'])->name('product.search');
Route::get('show-product-search', [IndexController::class, 'showProductSearch']);
Route::get('product/detail/{id}/{slug}', [IndexController::class, 'detailProduct']);
Route::get('/user/profile', [IndexController::class, 'profileUser'])->name('user.profile');
Route::post('/user/profile/save', [IndexController::class, 'storeProfile'])->name('user.storeProfile');
Route::get('/user/changePassword', [IndexController::class, 'changePassword'])->name('user.changPassword');
Route::post('/user/saveChangePassword', [IndexController::class, 'saveChangePassword'])->name('user.saveChangePassword');
Route::get('user/logout', [IndexController::class, 'logoutUser'])->name('user.logout');


//Route Brand Admin
Route::group(['prefix' => 'brand', 'middleware'=>['auth:admin', 'CheckRole']], function() {
        Route::get('view', [BrandController::class, 'viewBrand'])->name('Brand.view')->can('read-brand');
        Route::POST('store', [BrandController::class, 'storeBrand'])->name('Brand.store')->can('create-brand');
        Route::GET('edit/{id}', [BrandController::class, 'editBrand'])->name('Brand.edit')->can('update-brand');
        Route::POST('update/{id}', [BrandController::class, 'updateBrand'])->name('Brand.update')->can('update-brand');
        Route::GET('delete/{id}', [BrandController::class, 'deleteBrand'])->name('Brand.delete')->can('delete-brand');
});

// route Category Admin
Route::group(['prefix' => 'category', 'middleware'=>['auth:admin','CheckRole']], function() {
    Route::get('view-category', [CategoryController::class, 'viewCategory'])->name('Category.view');
    Route::POST('store', [CategoryController::class, 'storeCategory'])->name('Category.store');
    Route::GET('edit-category/{id}', [CategoryController::class, 'editCategory'])->name('Category.edit');
    Route::POST('update/{id}', [CategoryController::class, 'updateCategory'])->name('Category.update');
    Route::GET('delete/{id}', [CategoryController::class, 'deleteCategory'])->name('Category.delete');
// route subCategory
    Route::get('view-subCategory', [SubCategoryContrller::class, 'view_subCategory'])->name('SubCategory.view');
    Route::POST('store-subCategory', [SubCategoryContrller::class, 'storeSubcategory'])->name('Subcategory.store');
    Route::GET('edit-Subcategory/{id}', [SubCategoryContrller::class, 'editSubcategory'])->name('Subcategory.edit');
    Route::POST('update-Subcategory/{id}', [SubCategoryContrller::class, 'updateSubcategory'])->name('Subcategory.update');
    Route::GET('delete-Subcategory/{id}', [SubCategoryContrller::class, 'deleteSubcategory'])->name('Subcategory.delete');
// route subsubCategory
    Route::get('view-subsubCategory', [SubsubcategoryController::class, 'view_subsubCategory'])->name('SubsubCategory.view');
    Route::POST('store-subsubCategory', [SubsubcategoryController::class, 'storeSubsubCategory'])->name('Subsubcategory.store');
    Route::GET('edit-SubsubCategory/{id}', [SubsubcategoryController::class, 'editSubsubCategory'])->name('SubsubCategory.edit');
    Route::POST('update-subsubCategory/{id}', [SubsubcategoryController::class, 'updateSubsubCategory'])->name('Subsubcategory.update');
    Route::GET('delete-subsubCategory/{id}', [SubsubcategoryController::class, 'deleteSubsubCategory'])->name('SubsubCategory.delete');
    Route::POST('getSubCategories', [SubsubcategoryController::class, 'getSubCategories'])->name('ajax.request.getSubCategories');

});

// route product
Route::group(['prefix' => 'product', 'middleware'=>['auth:admin','CheckRole']], function() {
    Route::get('add', [ProductController::class, 'addProduct'])->name('Product.add');
    Route::POST('save', [ProductController::class, 'saveProduct'])->name('product.save');
    Route::POST('getSubSubCategories', [ProductController::class, 'getSubSubCategories'])->name('ajax.request.getSubSubCategories');
    Route::get('manage', [ProductController::class, 'manageProduct'])->name('Product.manage');
    Route::get('edit/{id}', [ProductController::class, 'editProduct'])->name('Product.edit');
    Route::POST('update', [ProductController::class, 'updateProduct'])->name('product.update');
    Route::POST('update/multiImage', [ProductController::class, 'updateMultiImage'])->name('product.updateMultiImage');
    Route::get('edit/{id}', [ProductController::class, 'editProduct'])->name('Product.edit');
    Route::get('deleteMultiImage/{id}', [ProductController::class, 'deleteMultiImage'])->name('product.deleteMultiImage');
    Route::get('active/{id}', [ProductController::class, 'activeProduct'])->name('product.active');
    Route::get('inactive/{id}', [ProductController::class, 'inactiveProduct'])->name('product.inactive');
    Route::get('delete/{id}', [ProductController::class, 'deleteProduct'])->name('Product.delete');
});


//route slider
Route::group(['prefix' => 'slider', 'middleware'=>['auth:admin','CheckRole']], function() {
    Route::get('manage', [SliderController::class, 'manageSlider'])->name('Slider.manage');
    Route::POST('store', [SliderController::class, 'storeSlider'])->name('Slider.store');
    Route::GET('edit/{id}', [SliderController::class, 'editSlider'])->name('Slider.edit');
    Route::POST('update/{id}', [SliderController::class, 'updateSlider'])->name('Slider.update');
    Route::GET('delete/{id}', [SliderController::class, 'deleteSlider'])->name('Slider.delete');
    Route::get('inactive/{id}', [SliderController::class, 'inactiveSlider'])->name('slider.inactive');
    Route::get('active/{id}', [SliderController::class, 'activeSlider'])->name('slider.active');
});
// route coupon
Route::group(['prefix' => 'coupon', 'middleware'=>['auth:admin','CheckRole']], function() {
    Route::get('manage', [CouponController::class, 'manageCoupon'])->name('Coupon.manage');
    Route::POST('store', [CouponController::class, 'storeCoupon'])->name('Coupon.store');
    Route::GET('edit/{id}', [CouponController::class, 'editCoupon'])->name('Coupon.edit');
    Route::POST('update/{id}', [CouponController::class, 'updateCoupon'])->name('Coupon.update');
    Route::GET('delete/{id}', [CouponController::class, 'deleteCoupon'])->name('Coupon.delete');
});
// route order admin
Route::group(['prefix' => 'order', 'middleware'=>['auth:admin','CheckRole']], function() {
    Route::get('pending', [OrderController::class, 'managePendingOrder'])->name('Order.managePendingOrder');
    Route::get('pending/confirmed/{order_id}', [OrderController::class, 'updateConfirmedOrder']);
    Route::get('detail/{order_id}', [OrderController::class, 'detailOrder'])->name('Order.detail');
    Route::get('confirmed', [OrderController::class, 'manageConfirmedOrder'])->name('Order.manageConfirmedOrder');
    Route::get('confirmed/processing/{order_id}', [OrderController::class, 'updateProcessingOrder']);
    Route::get('processing', [OrderController::class, 'manageProcessingOrder'])->name('Order.manageProcessingOrder');
    Route::get('processing/picked/{order_id}', [OrderController::class, 'updatePickedOrder']);
    Route::get('picked', [OrderController::class, 'managePickedOrder'])->name('Order.managePickedOrder');
    Route::get('picked/shipped/{order_id}', [OrderController::class, 'updateShippedOrder']);
    Route::get('shipped', [OrderController::class, 'manageShippedOrder'])->name('Order.manageShippedOrder');
    Route::get('shipped/delivered/{order_id}', [OrderController::class, 'updateDeliveredOrder']);
    Route::get('delivered', [OrderController::class, 'manageDeliveredOrder'])->name('Order.manageDeliveredOrder');
    Route::get('delivered/cancel/{order_id}', [OrderController::class, 'updateCancelOrder']);
    Route::get('cancel', [OrderController::class, 'manageCancelOrder'])->name('Order.manageCancelOrder');
    Route::get('invoice/{order_id}', [OrderController::class, 'invoiceOrder'])->name('Order.invoice');
    Route::get('return', [OrderController::class, 'manageReturnOrder'])->name('Order.manageReturnOrder');
    Route::get('return/success/{id}', [OrderController::class, 'returnSuccess'])->name('Order.returnSuccess');
});

// module routes
Route::group(['prefix' => 'module'], function() {
    Route::get('show', [ModuleController::class, 'show'])->name('Module.manage');
    Route::POST('store', [ModuleController::class, 'store'])->name('Module.store');
    Route::GET('edit/{id}', [ModuleController::class, 'edit'])->name('Module.edit');
    Route::POST('update/{id}', [ModuleController::class, 'update'])->name('Module.update');
    Route::GET('delete/{id}', [ModuleController::class, 'delete'])->name('Module.delete');
});

// end module routes

//add role admin routes
Route::group(['prefix' => 'admin'], function() {
    Route::get('manageAdmin', [ModuleController::class, 'manageAdmin'])->name('Admin.manage');
    Route::get('editAdmin/{id}', [ModuleController::class, 'editAdmin'])->name('Admin.edit');
    Route::post('updateAdmin/{id}', [ModuleController::class, 'updateAdmin'])->name('Admin.update');
    Route::get('deleteAdmin', [ModuleController::class, 'deleteAdmin'])->name('Admin.delete');
    Route::post('storeAdmin', [ModuleController::class, 'storeAdmin'])->name('Admin.storeRoleAdmin');
});
//end module routes


// route product subcategory
Route::middleware('checkOnline')->group(function () {
Route::get('subcategory/{id}/{slug}', [IndexController::class, 'showProductSubCategory'])->name('ProductSubCategory.show');
Route::get('subsubCategory/{id}/{slug}', [IndexController::class, 'showProductSubSubCategory'])->name('ProductSubSubCategory.show');
Route::get('product/modal/{id}', [IndexController::class, 'getProductModal']);
});

// route cart
Route::middleware('checkOnline')->group(function () {
Route::post('cart/add/{id}', [CartController::class, 'addToCart']);
Route::POST('get/data/cart', [CartController::class, 'getDataCart'])->name('cartMini.data');
Route::POST('cart/delete/data/{rowId}', [CartController::class, 'deleteDataCart']);
Route::get('my-cart', [CartController::class, 'showCart'])->name('cart.show');
Route::get('get-listCart' , [CartController::class, 'getListCart']);
Route::get('increase-qty/{rowId}' , [CartController::class, 'increaseQty']);
Route::get('decrease-qty/{rowId}' , [CartController::class, 'decreaseQty']);
Route::get('delete-product-cart/{rowId}' , [CartController::class, 'deleteProductCart']);
});


// route wishlist
Route::middleware('checkOnline')->group(function () {
Route::get('wishlist' , [WishlistContrller::class, 'showWishlist'])->name('wishlist.all')->middleware('auth');
Route::POST('addWishlist/{id}' , [WishlistContrller::class, 'addWishlist']);
Route::get('get-withlist' , [WishlistContrller::class, 'getWishlist'])->middleware('auth');
Route::POST('delete-withlist/{id}' , [WishlistContrller::class, 'deleteWishlist']);
});


// route coupon ajax
Route::middleware('checkOnline')->group(function () {
    Route::post('apply-coupon', [CartController::class, 'checkCoupon']);
Route::post('caculation-cart', [CartController::class, 'caculateCart']);
Route::post('remove-coupon', [CartController::class, 'removeCoupon']);
});

// route checkout

Route::middleware('checkOnline')->group(function () {
   Route::get('checkout', [CartController::class, 'showCheckout'])->name('checkout');
Route::get('get-district/{matp}', [CartController::class, 'getDistrict']);
Route::get('get-wards/{maqh}', [CartController::class, 'getWards']);
Route::get('checkout/save', [CartController::class, 'saveCheckout'])->name('checkout.save');
Route::POST('checkout/store', [CartController::class, 'storeCheckout'])->name('checkout.store');
});


// order
Route::middleware('checkOnline')->group(function () {
    Route::get('my-orders', [AlluserController::class, 'viewOrder'])->name('order.view');
    Route::get('my-order/detail/{order_id}', [AlluserController::class, 'detailOrder'])->name('order.detail');
    Route::get('my-order/invoice/{order_id}', [AlluserController::class, 'invoiceOrder'])->name('order.invoice');
    Route::post('my-order/reason/{order_id}', [AlluserController::class, 'saveReasonOrder'])->name('order.reason');
    Route::get('order/search', [AlluserController::class, 'searchOrder'])->name('order.search');
    });






Route::group(['prefix' => 'AllOrder', 'middleware'=>['auth:admin','CheckRole']], function() {
    Route::get('search', [OrderController::class, 'searchAllOrder'])->name('Allorder.search');
    Route::get('search/day/save', [OrderController::class, 'searchDaySave'])->name('Allorder.searchDaySave');
    Route::get('search/month/save', [OrderController::class, 'searchMonthSave'])->name('Allorder.searchMonthSave');
    Route::get('search/year/save', [OrderController::class, 'searchYearSave'])->name('Allorder.searchYearSave');
});
Route::group(['prefix' => 'Alluser', 'middleware'=>['auth:admin','CheckRole']], function() {
    Route::get('view', [AdminProfileController::class, 'viewAllUser'])->name('Alluser.view');
});
//route post_category backend

Route::group(['prefix' => 'postCategory', 'middleware'=>['auth:admin','CheckRole']], function() {
    Route::get('manage', [PostCategoryController::class, 'managePostCateogry'])->name('PostCategory.manage');
    Route::POST('store', [PostCategoryController::class, 'storeCategory'])->name('PostCategory.store');
    Route::GET('edit/{id}', [PostCategoryController::class, 'editCategory'])->name('PostCategory.edit');
    Route::POST('update/{id}', [PostCategoryController::class, 'updateCategory'])->name('PostCategory.update');
    Route::GET('delete/{id}', [PostCategoryController::class, 'deleteCategory'])->name('PostCategory.delete');
});
Route::group(['prefix' => 'post', 'middleware'=>['auth:admin','CheckRole']], function() {
    Route::get('manage', [PostController::class, 'manage'])->name('Post.manage');
    Route::get('add', [PostController::class, 'add'])->name('Post.add');
    Route::POST('store', [PostController::class, 'store'])->name('Post.save');
    Route::GET('edit/{id}', [PostController::class, 'edit'])->name('Post.edit');
    Route::POST('update', [PostController::class, 'update'])->name('Post.update');
    Route::get('active/{id}', [PostController::class, 'active'])->name('Post.active');
    Route::get('inactive/{id}', [PostController::class, 'inactive'])->name('Post.inactive');
    Route::get('delete/{id}', [PostController::class, 'delete'])->name('Post.delete');
});

//route post_category backend
Route::group(['prefix' => 'setting', 'middleware'=>['auth:admin','CheckRole']], function() {
    Route::get('site', [SiteController::class, 'manageSite'])->name('Site.manage');
    Route::POST('update/{id}', [SiteController::class, 'updateSite'])->name('Site.update');
});


// route post home
Route::get('blogs', [HomePostController::class, 'showListBlogs'])->name('Blog.list');
Route::get('blog/category/{slug_category}', [HomePostController::class, 'showListBlogsCategory'])->name('Blog.listCategory');
Route::get('blog/{slug}', [HomePostController::class, 'detailBlog'])->name('Blog.detail');
Route::get('blog/tag/{tag}', [HomePostController::class, 'showListBlogTag'])->name('Blog.listTag');
Route::get('/blogss', [HomePostController::class, 'loadMore']);
// Route review client
Route::POST('review/save', [ReviewController::class, 'saveReview'])->name('Review.save');
Route::GET('load-comment', [ReviewController::class, 'loadComment']);

Route::group(['prefix' => 'review', 'middleware'=>['auth:admin','CheckRole']], function() {
    Route::get('manage', [AdminReviewController::class, 'manageReview'])->name('Review.manage');
    Route::get('active/{id}', [AdminReviewController::class, 'active'])->name('Review.active');
    Route::get('inactive/{id}', [AdminReviewController::class, 'inactive'])->name('Review.inactive');
    Route::get('delete/{id}', [AdminReviewController::class, 'delete'])->name('Review.delete');
});
Route::group(['prefix' => 'adminRole', 'middleware'=>['auth:admin','CheckRole']], function() {
    Route::get('manage', [AdminController::class, 'manageAdmin'])->name('adminRole.manage');
    Route::get('add', [AdminController::class, 'addAdmin'])->name('adminRole.add');
    Route::post('store', [AdminController::class, 'storeAdmin'])->name('adminRole.store');
    Route::get('edit/{id}', [AdminController::class, 'editAdmin'])->name('adminRole.edit');
    Route::get('delete/{id}', [AdminController::class, 'deleteAdmin'])->name('adminRole.delete');
    Route::post('update/{id}', [AdminController::class, 'updateAdmin'])->name('adminRole.update');
    Route::post('delete/{id}', [AdminController::class, 'deleteAdmin'])->name('adminRole.delete');
});

Route::get('load-rating',[ReviewController::class, 'loadRating']);
Route::post('rating-star',[ReviewController::class, 'ratingStar']);
Route::get('shop', [ShopController::class, 'showProduct'])->name('product.shop');
Route::post('shop/by-category', [ShopController::class, 'sortCategory']);



// Route::get('{any}', function() {
//     return 1;
//         return redirect('https://targetdomain.com');
//      })->where('any', '.*');
Route::get('demo', function () {
    echo Demo::helloWorld();
   });
